void run2-trilep-CutZdefi_bestWithWWWnode_bveto-lepPt_antiIDFFbins-lin()
{
//=========Macro generated from canvas: CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins/CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins
//=========  (Fri Mar  3 21:39:36 2023) by ROOT version 6.18/04
   TCanvas *CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins = new TCanvas("CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins", "CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetHighLightColor(2);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->Range(0,0,1,1);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetFillColor(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetBorderMode(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetBorderSize(2);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetLeftMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetRightMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetTopMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetBottomMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-121.519,159.4937,637.9747);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis19[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",4, xAxis19);
   Graph_master->SetMinimum(0);
   Graph_master->SetMaximum(600);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis20[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *stack_stack_7 = new TH1F("stack_stack_7","Background Stack",4, xAxis20);
   stack_stack_7->SetMinimum(-0);
   stack_stack_7->SetMaximum(6.317771);
   stack_stack_7->SetDirectory(0);
   stack_stack_7->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_7->SetLineColor(ci);
   stack_stack_7->GetXaxis()->SetLabelFont(42);
   stack_stack_7->GetXaxis()->SetLabelSize(0.035);
   stack_stack_7->GetXaxis()->SetTitleSize(0.035);
   stack_stack_7->GetXaxis()->SetTitleOffset(1);
   stack_stack_7->GetXaxis()->SetTitleFont(42);
   stack_stack_7->GetYaxis()->SetLabelFont(42);
   stack_stack_7->GetYaxis()->SetLabelSize(0.035);
   stack_stack_7->GetYaxis()->SetTitleSize(0.035);
   stack_stack_7->GetYaxis()->SetTitleFont(42);
   stack_stack_7->GetZaxis()->SetLabelFont(42);
   stack_stack_7->GetZaxis()->SetLabelSize(0.035);
   stack_stack_7->GetZaxis()->SetTitleSize(0.035);
   stack_stack_7->GetZaxis()->SetTitleOffset(1);
   stack_stack_7->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_7);
   
   Double_t xAxis21[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_WZ_stack_1 = new TH1F("hist_WZ_stack_1","$WZ$",4, xAxis21);
   hist_WZ_stack_1->SetBinContent(2,2.679123);
   hist_WZ_stack_1->SetBinContent(3,2.893568);
   hist_WZ_stack_1->SetBinContent(4,3.929338);
   hist_WZ_stack_1->SetBinError(2,0.3069703);
   hist_WZ_stack_1->SetBinError(3,0.3263394);
   hist_WZ_stack_1->SetBinError(4,0.367025);
   hist_WZ_stack_1->SetEntries(1944);
   hist_WZ_stack_1->SetStats(0);
   hist_WZ_stack_1->SetFillColor(222);
   hist_WZ_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_WZ_stack_1->GetXaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_WZ_stack_1->GetXaxis()->SetTitleFont(42);
   hist_WZ_stack_1->GetYaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetYaxis()->SetTitleFont(42);
   hist_WZ_stack_1->GetZaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_WZ_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WZ_stack_1,"");
   Double_t xAxis22[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_ZZ_stack_2 = new TH1F("hist_ZZ_stack_2","$ZZ$",4, xAxis22);
   hist_ZZ_stack_2->SetBinContent(2,0.342388);
   hist_ZZ_stack_2->SetBinContent(3,0.4478564);
   hist_ZZ_stack_2->SetBinContent(4,0.4497783);
   hist_ZZ_stack_2->SetBinError(2,0.04695682);
   hist_ZZ_stack_2->SetBinError(3,0.05105908);
   hist_ZZ_stack_2->SetBinError(4,0.07286329);
   hist_ZZ_stack_2->SetEntries(820);
   hist_ZZ_stack_2->SetStats(0);
   hist_ZZ_stack_2->SetFillColor(226);
   hist_ZZ_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_ZZ_stack_2->GetXaxis()->SetLabelFont(42);
   hist_ZZ_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_ZZ_stack_2->GetXaxis()->SetTitleFont(42);
   hist_ZZ_stack_2->GetYaxis()->SetLabelFont(42);
   hist_ZZ_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_2->GetYaxis()->SetTitleFont(42);
   hist_ZZ_stack_2->GetZaxis()->SetLabelFont(42);
   hist_ZZ_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_ZZ_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ZZ_stack_2,"");
   Double_t xAxis23[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_VVV_stack_3 = new TH1F("hist_VVV_stack_3","$VVV$",4, xAxis23);
   hist_VVV_stack_3->SetBinContent(2,0.4394206);
   hist_VVV_stack_3->SetBinContent(3,0.6179417);
   hist_VVV_stack_3->SetBinContent(4,0.8569328);
   hist_VVV_stack_3->SetBinError(2,0.009357862);
   hist_VVV_stack_3->SetBinError(3,0.01107323);
   hist_VVV_stack_3->SetBinError(4,0.01473718);
   hist_VVV_stack_3->SetEntries(18932);
   hist_VVV_stack_3->SetStats(0);

   ci = TColor::GetColor("#ff00ff");
   hist_VVV_stack_3->SetFillColor(ci);
   hist_VVV_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_VVV_stack_3->GetXaxis()->SetLabelFont(42);
   hist_VVV_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_VVV_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_VVV_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_VVV_stack_3->GetXaxis()->SetTitleFont(42);
   hist_VVV_stack_3->GetYaxis()->SetLabelFont(42);
   hist_VVV_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_VVV_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_VVV_stack_3->GetYaxis()->SetTitleFont(42);
   hist_VVV_stack_3->GetZaxis()->SetLabelFont(42);
   hist_VVV_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_VVV_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_VVV_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_VVV_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_VVV_stack_3,"");
   Double_t xAxis24[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_ttV_stack_4 = new TH1F("hist_ttV_stack_4","$ttV$",4, xAxis24);
   hist_ttV_stack_4->SetBinContent(2,0.1872506);
   hist_ttV_stack_4->SetBinContent(3,0.3580432);
   hist_ttV_stack_4->SetBinContent(4,0.7631143);
   hist_ttV_stack_4->SetBinError(2,0.0388555);
   hist_ttV_stack_4->SetBinError(3,0.04847776);
   hist_ttV_stack_4->SetBinError(4,0.07482207);
   hist_ttV_stack_4->SetEntries(546);
   hist_ttV_stack_4->SetStats(0);
   hist_ttV_stack_4->SetFillColor(220);
   hist_ttV_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_ttV_stack_4->GetXaxis()->SetLabelFont(42);
   hist_ttV_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_ttV_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_ttV_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_ttV_stack_4->GetXaxis()->SetTitleFont(42);
   hist_ttV_stack_4->GetYaxis()->SetLabelFont(42);
   hist_ttV_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_ttV_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_ttV_stack_4->GetYaxis()->SetTitleFont(42);
   hist_ttV_stack_4->GetZaxis()->SetLabelFont(42);
   hist_ttV_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_ttV_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_ttV_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_ttV_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ttV_stack_4,"");
   Double_t xAxis25[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_tZ_stack_5 = new TH1F("hist_tZ_stack_5","$tZ$",4, xAxis25);
   hist_tZ_stack_5->SetBinContent(2,0.0246203);
   hist_tZ_stack_5->SetBinContent(3,0.02128303);
   hist_tZ_stack_5->SetBinContent(4,0.01776135);
   hist_tZ_stack_5->SetBinError(2,0.004493025);
   hist_tZ_stack_5->SetBinError(3,0.004154991);
   hist_tZ_stack_5->SetBinError(4,0.003866372);
   hist_tZ_stack_5->SetEntries(83);
   hist_tZ_stack_5->SetStats(0);
   hist_tZ_stack_5->SetFillColor(218);
   hist_tZ_stack_5->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_tZ_stack_5->GetXaxis()->SetLabelFont(42);
   hist_tZ_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_tZ_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_tZ_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_tZ_stack_5->GetXaxis()->SetTitleFont(42);
   hist_tZ_stack_5->GetYaxis()->SetLabelFont(42);
   hist_tZ_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_tZ_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_tZ_stack_5->GetYaxis()->SetTitleFont(42);
   hist_tZ_stack_5->GetZaxis()->SetLabelFont(42);
   hist_tZ_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_tZ_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_tZ_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_tZ_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tZ_stack_5,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3007[7] = {
   0,
   7.5,
   17.5,
   25,
   90,
   168.75,
   206.25};
   Double_t totalStackErr_fy3007[7] = {
   0,
   0,
   3.672802,
   4.338692,
   6.016924,
   0,
   0};
   Double_t totalStackErr_felx3007[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fely3007[7] = {
   0,
   0,
   0.3131345,
   0.3340575,
   0.381899,
   0,
   0};
   Double_t totalStackErr_fehx3007[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fehy3007[7] = {
   0,
   0,
   0.3131345,
   0.3340575,
   0.381899,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(7,totalStackErr_fx3007,totalStackErr_fy3007,totalStackErr_felx3007,totalStackErr_fehx3007,totalStackErr_fely3007,totalStackErr_fehy3007);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3007 = new TH1F("Graph_totalStackErr3007","SM",100,0,292.875);
   Graph_totalStackErr3007->SetMinimum(0);
   Graph_totalStackErr3007->SetMaximum(7.038706);
   Graph_totalStackErr3007->SetDirectory(0);
   Graph_totalStackErr3007->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3007->SetLineColor(ci);
   Graph_totalStackErr3007->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3007->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3007->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3007->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3007->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3007->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3007->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3007->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3007->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3007->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3007->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3007->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3007->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3007->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3007);
   
   grae->Draw("2");
   Double_t xAxis26[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_data__7 = new TH1F("hist_data__7","Data",4, xAxis26);
   hist_data__7->SetBinContent(2,151);
   hist_data__7->SetBinContent(3,248);
   hist_data__7->SetBinContent(4,310);
   hist_data__7->SetBinError(2,12.28821);
   hist_data__7->SetBinError(3,15.74802);
   hist_data__7->SetBinError(4,17.60682);
   hist_data__7->SetEntries(709);
   hist_data__7->SetStats(0);
   hist_data__7->SetFillColor(15);
   hist_data__7->SetLineWidth(2);
   hist_data__7->SetMarkerStyle(20);
   hist_data__7->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_data__7->GetXaxis()->SetLabelFont(42);
   hist_data__7->GetXaxis()->SetLabelSize(0.035);
   hist_data__7->GetXaxis()->SetTitleSize(0.035);
   hist_data__7->GetXaxis()->SetTitleOffset(1);
   hist_data__7->GetXaxis()->SetTitleFont(42);
   hist_data__7->GetYaxis()->SetLabelFont(42);
   hist_data__7->GetYaxis()->SetLabelSize(0.035);
   hist_data__7->GetYaxis()->SetTitleSize(0.035);
   hist_data__7->GetYaxis()->SetTitleFont(42);
   hist_data__7->GetZaxis()->SetLabelFont(42);
   hist_data__7->GetZaxis()->SetLabelSize(0.035);
   hist_data__7->GetZaxis()->SetTitleSize(0.035);
   hist_data__7->GetZaxis()->SetTitleOffset(1);
   hist_data__7->GetZaxis()->SetTitleFont(42);
   hist_data__7->Draw("ep same");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("hist_data"," Data","lep");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WZ_stack_1"," #it{WZ}","f");
   entry->SetFillColor(222);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ZZ_stack_2"," #it{ZZ}","f");
   entry->SetFillColor(226);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_VVV_stack_3"," #it{VVV}","f");

   ci = TColor::GetColor("#ff00ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ttV_stack_4"," #it{ttV}","f");
   entry->SetFillColor(220);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tZ_stack_5"," #it{tZ}","f");
   entry->SetFillColor(218);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutZdefi_bestWithWWWnode_bveto/lepPt_antiIDFFbins\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"3l channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis27[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",4, xAxis27);
   Graph_master_copy->SetMinimum(0);
   Graph_master_copy->SetMaximum(600);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins->SetSelected(CutZdefi_bestWithWWWnode_bveto_lepPt_antiIDFFbins);
}
