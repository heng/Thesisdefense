void run2-trilep-CutZdefi_bestWithWWWnode_bveto-mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged-log()
{
//=========Macro generated from canvas: CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged/CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged
//=========  (Thu Mar  2 22:44:13 2023) by ROOT version 6.18/04
   TCanvas *CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged = new TCanvas("CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged", "CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetHighLightColor(2);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->Range(0,0,1,1);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetFillColor(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetBorderMode(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetBorderSize(2);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetLeftMargin(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetRightMargin(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetTopMargin(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetBottomMargin(0);
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-1.405063,-1.749158,1.126582,2.933082);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis31[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",6, xAxis31);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(500);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("Multiclassifier WH-WZ-WWW node");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis32[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *stack_stack_10 = new TH1F("stack_stack_10","Background Stack",6, xAxis32);
   stack_stack_10->SetMinimum(0.008698338);
   stack_stack_10->SetMaximum(34.79335);
   stack_stack_10->SetDirectory(0);
   stack_stack_10->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_10->SetLineColor(ci);
   stack_stack_10->GetXaxis()->SetLabelFont(42);
   stack_stack_10->GetXaxis()->SetLabelSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleOffset(1);
   stack_stack_10->GetXaxis()->SetTitleFont(42);
   stack_stack_10->GetYaxis()->SetLabelFont(42);
   stack_stack_10->GetYaxis()->SetLabelSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleFont(42);
   stack_stack_10->GetZaxis()->SetLabelFont(42);
   stack_stack_10->GetZaxis()->SetLabelSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleOffset(1);
   stack_stack_10->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_10);
   
   Double_t xAxis33[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *hist_light_flavor_stack_1 = new TH1F("hist_light_flavor_stack_1","light flavor",6, xAxis33);
   hist_light_flavor_stack_1->SetBinContent(1,3.200152);
   hist_light_flavor_stack_1->SetBinContent(2,5.404833);
   hist_light_flavor_stack_1->SetBinContent(3,0.3835376);
   hist_light_flavor_stack_1->SetBinContent(4,2.336215);
   hist_light_flavor_stack_1->SetBinContent(5,-0.1143191);
   hist_light_flavor_stack_1->SetBinContent(6,0.0517606);
   hist_light_flavor_stack_1->SetBinError(1,0.4496723);
   hist_light_flavor_stack_1->SetBinError(2,2.615956);
   hist_light_flavor_stack_1->SetBinError(3,0.1265496);
   hist_light_flavor_stack_1->SetBinError(4,1.729628);
   hist_light_flavor_stack_1->SetBinError(5,0.2874171);
   hist_light_flavor_stack_1->SetBinError(6,0.04610529);
   hist_light_flavor_stack_1->SetEntries(317);
   hist_light_flavor_stack_1->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_1->SetFillColor(ci);
   hist_light_flavor_stack_1->GetXaxis()->SetTitle("Multiclassifier WH-WZ-WWW node");
   hist_light_flavor_stack_1->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_1->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_1->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_1,"");
   Double_t xAxis34[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *hist_heavy_flavor_stack_2 = new TH1F("hist_heavy_flavor_stack_2","heavy flavor",6, xAxis34);
   hist_heavy_flavor_stack_2->SetBinContent(1,5.87044);
   hist_heavy_flavor_stack_2->SetBinContent(2,8.423578);
   hist_heavy_flavor_stack_2->SetBinContent(3,3.113394);
   hist_heavy_flavor_stack_2->SetBinContent(4,2.195584);
   hist_heavy_flavor_stack_2->SetBinContent(5,1.307357);
   hist_heavy_flavor_stack_2->SetBinContent(6,0.2044148);
   hist_heavy_flavor_stack_2->SetBinError(1,0.5561943);
   hist_heavy_flavor_stack_2->SetBinError(2,0.7248619);
   hist_heavy_flavor_stack_2->SetBinError(3,0.4027891);
   hist_heavy_flavor_stack_2->SetBinError(4,0.3508122);
   hist_heavy_flavor_stack_2->SetBinError(5,0.2653586);
   hist_heavy_flavor_stack_2->SetBinError(6,0.1542839);
   hist_heavy_flavor_stack_2->SetEntries(481);
   hist_heavy_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_2->SetFillColor(ci);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitle("Multiclassifier WH-WZ-WWW node");
   hist_heavy_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_2,"");
   Double_t xAxis35[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *hist_gamma_conversion_stack_3 = new TH1F("hist_gamma_conversion_stack_3","$\\gamma$ conv.",6, xAxis35);
   hist_gamma_conversion_stack_3->SetBinContent(1,3.042709);
   hist_gamma_conversion_stack_3->SetBinContent(2,7.917433);
   hist_gamma_conversion_stack_3->SetBinContent(3,10.46691);
   hist_gamma_conversion_stack_3->SetBinContent(4,2.233247);
   hist_gamma_conversion_stack_3->SetBinContent(5,1.067542);
   hist_gamma_conversion_stack_3->SetBinContent(6,0.4558582);
   hist_gamma_conversion_stack_3->SetBinError(1,0.4986923);
   hist_gamma_conversion_stack_3->SetBinError(2,2.004305);
   hist_gamma_conversion_stack_3->SetBinError(3,5.251077);
   hist_gamma_conversion_stack_3->SetBinError(4,0.3738553);
   hist_gamma_conversion_stack_3->SetBinError(5,0.2429306);
   hist_gamma_conversion_stack_3->SetBinError(6,0.2309574);
   hist_gamma_conversion_stack_3->SetEntries(560);
   hist_gamma_conversion_stack_3->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_3->SetFillColor(ci);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitle("Multiclassifier WH-WZ-WWW node");
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3010[9] = {
   0,
   -0.875,
   -0.525,
   -0.1125,
   0.25,
   0.55,
   0.8375,
   1.166667,
   1.5};
   Double_t totalStackErr_fy3010[9] = {
   0,
   12.1133,
   21.74584,
   13.96385,
   6.765044,
   2.260581,
   0.7120335,
   0,
   0};
   Double_t totalStackErr_felx3010[9] = {
   0,
   0.125,
   0.225,
   0.1875,
   0.175,
   0.125,
   0.1625,
   0.1625,
   0.1625};
   Double_t totalStackErr_fely3010[9] = {
   0,
   0.8719239,
   3.374298,
   5.268023,
   1.804009,
   0.460477,
   0.2815502,
   0,
   0};
   Double_t totalStackErr_fehx3010[9] = {
   0,
   0.125,
   0.225,
   0.1875,
   0.175,
   0.125,
   0.1625,
   0.1625,
   0.1625};
   Double_t totalStackErr_fehy3010[9] = {
   0,
   0.8719239,
   3.374298,
   5.268023,
   1.804009,
   0.460477,
   0.2815502,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(9,totalStackErr_fx3010,totalStackErr_fy3010,totalStackErr_felx3010,totalStackErr_fehx3010,totalStackErr_fely3010,totalStackErr_fehy3010);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3010 = new TH1F("Graph_totalStackErr3010","SM",100,-1.26625,1.92875);
   Graph_totalStackErr3010->SetMinimum(0.02763216);
   Graph_totalStackErr3010->SetMaximum(27.63216);
   Graph_totalStackErr3010->SetDirectory(0);
   Graph_totalStackErr3010->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3010->SetLineColor(ci);
   Graph_totalStackErr3010->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3010);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_3"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_1"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_2"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutZdefi_bestWithWWWnode_bveto/mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"3l channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis36[7] = {-1, -0.75, -0.3, 0.075, 0.425, 0.675, 1}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",6, xAxis36);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(500);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("Multiclassifier WH-WZ-WWW node");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->cd();
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->Modified();
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->cd();
   CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged->SetSelected(CutZdefi_bestWithWWWnode_bveto_mclf_bestWithWWWnode_deltawhwzwwwNodeRebinnedMerged);
}
