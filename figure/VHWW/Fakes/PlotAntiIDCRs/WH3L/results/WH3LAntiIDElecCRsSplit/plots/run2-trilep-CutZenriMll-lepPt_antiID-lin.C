void run2-trilep-CutZenriMll-lepPt_antiID-lin()
{
//=========Macro generated from canvas: CutZenriMll_lepPt_antiID/CutZenriMll_lepPt_antiID
//=========  (Thu Mar  2 22:44:13 2023) by ROOT version 6.18/04
   TCanvas *CutZenriMll_lepPt_antiID = new TCanvas("CutZenriMll_lepPt_antiID", "CutZenriMll_lepPt_antiID",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutZenriMll_lepPt_antiID->SetHighLightColor(2);
   CutZenriMll_lepPt_antiID->Range(0,0,1,1);
   CutZenriMll_lepPt_antiID->SetFillColor(0);
   CutZenriMll_lepPt_antiID->SetBorderMode(0);
   CutZenriMll_lepPt_antiID->SetBorderSize(2);
   CutZenriMll_lepPt_antiID->SetLeftMargin(0);
   CutZenriMll_lepPt_antiID->SetRightMargin(0);
   CutZenriMll_lepPt_antiID->SetTopMargin(0);
   CutZenriMll_lepPt_antiID->SetBottomMargin(0);
   CutZenriMll_lepPt_antiID->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-85.15913,159.4937,425.5347);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",30,0,150);
   Graph_master->SetMinimum(-3.448121);
   Graph_master->SetMaximum(400);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   
   TH1F *stack_stack_17 = new TH1F("stack_stack_17","Background Stack",30,0,150);
   stack_stack_17->SetMinimum(-3.448121);
   stack_stack_17->SetMaximum(227.377);
   stack_stack_17->SetDirectory(0);
   stack_stack_17->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_17->SetLineColor(ci);
   stack_stack_17->GetXaxis()->SetLabelFont(42);
   stack_stack_17->GetXaxis()->SetLabelSize(0.035);
   stack_stack_17->GetXaxis()->SetTitleSize(0.035);
   stack_stack_17->GetXaxis()->SetTitleOffset(1);
   stack_stack_17->GetXaxis()->SetTitleFont(42);
   stack_stack_17->GetYaxis()->SetLabelFont(42);
   stack_stack_17->GetYaxis()->SetLabelSize(0.035);
   stack_stack_17->GetYaxis()->SetTitleSize(0.035);
   stack_stack_17->GetYaxis()->SetTitleFont(42);
   stack_stack_17->GetZaxis()->SetLabelFont(42);
   stack_stack_17->GetZaxis()->SetLabelSize(0.035);
   stack_stack_17->GetZaxis()->SetTitleSize(0.035);
   stack_stack_17->GetZaxis()->SetTitleOffset(1);
   stack_stack_17->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_17);
   
   
   TH1F *hist_gamma_conversion_stack_1 = new TH1F("hist_gamma_conversion_stack_1","$\\gamma$ conv.",30,0,150);
   hist_gamma_conversion_stack_1->SetBinContent(4,167.8476);
   hist_gamma_conversion_stack_1->SetBinContent(5,143.8897);
   hist_gamma_conversion_stack_1->SetBinContent(6,63.59817);
   hist_gamma_conversion_stack_1->SetBinContent(7,38.73368);
   hist_gamma_conversion_stack_1->SetBinContent(8,41.93939);
   hist_gamma_conversion_stack_1->SetBinContent(9,14.31468);
   hist_gamma_conversion_stack_1->SetBinContent(10,18.47932);
   hist_gamma_conversion_stack_1->SetBinContent(11,10.83849);
   hist_gamma_conversion_stack_1->SetBinContent(12,9.887231);
   hist_gamma_conversion_stack_1->SetBinContent(13,6.474629);
   hist_gamma_conversion_stack_1->SetBinContent(14,5.291805);
   hist_gamma_conversion_stack_1->SetBinContent(15,3.124089);
   hist_gamma_conversion_stack_1->SetBinContent(16,2.040477);
   hist_gamma_conversion_stack_1->SetBinContent(17,2.771676);
   hist_gamma_conversion_stack_1->SetBinContent(18,2.125549);
   hist_gamma_conversion_stack_1->SetBinContent(19,-0.295783);
   hist_gamma_conversion_stack_1->SetBinContent(20,5.245502);
   hist_gamma_conversion_stack_1->SetBinContent(21,1.944242);
   hist_gamma_conversion_stack_1->SetBinContent(22,0.4266528);
   hist_gamma_conversion_stack_1->SetBinContent(23,3.301776);
   hist_gamma_conversion_stack_1->SetBinContent(24,0.581685);
   hist_gamma_conversion_stack_1->SetBinContent(25,0.451439);
   hist_gamma_conversion_stack_1->SetBinContent(26,-1.522929);
   hist_gamma_conversion_stack_1->SetBinContent(27,1.193663);
   hist_gamma_conversion_stack_1->SetBinContent(28,0.3483288);
   hist_gamma_conversion_stack_1->SetBinContent(29,0.4569927);
   hist_gamma_conversion_stack_1->SetBinContent(30,7.156782);
   hist_gamma_conversion_stack_1->SetBinError(4,15.3991);
   hist_gamma_conversion_stack_1->SetBinError(5,14.35767);
   hist_gamma_conversion_stack_1->SetBinError(6,9.398678);
   hist_gamma_conversion_stack_1->SetBinError(7,15.93948);
   hist_gamma_conversion_stack_1->SetBinError(8,10.28155);
   hist_gamma_conversion_stack_1->SetBinError(9,4.521993);
   hist_gamma_conversion_stack_1->SetBinError(10,4.297083);
   hist_gamma_conversion_stack_1->SetBinError(11,3.206829);
   hist_gamma_conversion_stack_1->SetBinError(12,2.339099);
   hist_gamma_conversion_stack_1->SetBinError(13,2.146768);
   hist_gamma_conversion_stack_1->SetBinError(14,4.506977);
   hist_gamma_conversion_stack_1->SetBinError(15,0.9299922);
   hist_gamma_conversion_stack_1->SetBinError(16,1.100182);
   hist_gamma_conversion_stack_1->SetBinError(17,0.9234371);
   hist_gamma_conversion_stack_1->SetBinError(18,1.189514);
   hist_gamma_conversion_stack_1->SetBinError(19,1.235696);
   hist_gamma_conversion_stack_1->SetBinError(20,2.826162);
   hist_gamma_conversion_stack_1->SetBinError(21,0.9575425);
   hist_gamma_conversion_stack_1->SetBinError(22,0.1351078);
   hist_gamma_conversion_stack_1->SetBinError(23,1.487639);
   hist_gamma_conversion_stack_1->SetBinError(24,0.3702384);
   hist_gamma_conversion_stack_1->SetBinError(25,0.3123773);
   hist_gamma_conversion_stack_1->SetBinError(26,1.925192);
   hist_gamma_conversion_stack_1->SetBinError(27,0.4773571);
   hist_gamma_conversion_stack_1->SetBinError(28,0.1658547);
   hist_gamma_conversion_stack_1->SetBinError(29,0.1666582);
   hist_gamma_conversion_stack_1->SetBinError(30,1.597028);
   hist_gamma_conversion_stack_1->SetEntries(7263);
   hist_gamma_conversion_stack_1->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_1->SetFillColor(ci);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_gamma_conversion_stack_1->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_1->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_1->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_1,"");
   
   TH1F *hist_light_flavor_stack_2 = new TH1F("hist_light_flavor_stack_2","light flavor",30,0,150);
   hist_light_flavor_stack_2->SetBinContent(4,27.87517);
   hist_light_flavor_stack_2->SetBinContent(5,20.62526);
   hist_light_flavor_stack_2->SetBinContent(6,5.058494);
   hist_light_flavor_stack_2->SetBinContent(7,7.397597);
   hist_light_flavor_stack_2->SetBinContent(8,6.878558);
   hist_light_flavor_stack_2->SetBinContent(9,6.386817);
   hist_light_flavor_stack_2->SetBinContent(10,2.394427);
   hist_light_flavor_stack_2->SetBinContent(11,2.864335);
   hist_light_flavor_stack_2->SetBinContent(12,0.8017682);
   hist_light_flavor_stack_2->SetBinContent(13,1.850592);
   hist_light_flavor_stack_2->SetBinContent(14,0.4716597);
   hist_light_flavor_stack_2->SetBinContent(15,1.186841);
   hist_light_flavor_stack_2->SetBinContent(16,1.697732);
   hist_light_flavor_stack_2->SetBinContent(17,0.8546113);
   hist_light_flavor_stack_2->SetBinContent(18,0.4482355);
   hist_light_flavor_stack_2->SetBinContent(19,0.0968592);
   hist_light_flavor_stack_2->SetBinContent(20,2.875516);
   hist_light_flavor_stack_2->SetBinContent(21,0.8134751);
   hist_light_flavor_stack_2->SetBinContent(22,0.19462);
   hist_light_flavor_stack_2->SetBinContent(23,-0.001309935);
   hist_light_flavor_stack_2->SetBinContent(24,-0.3584703);
   hist_light_flavor_stack_2->SetBinContent(25,0.6160743);
   hist_light_flavor_stack_2->SetBinContent(26,0.2878745);
   hist_light_flavor_stack_2->SetBinContent(27,0.7361475);
   hist_light_flavor_stack_2->SetBinContent(28,1.179073);
   hist_light_flavor_stack_2->SetBinContent(29,0.9456218);
   hist_light_flavor_stack_2->SetBinContent(30,9.917096);
   hist_light_flavor_stack_2->SetBinError(4,8.99442);
   hist_light_flavor_stack_2->SetBinError(5,7.778236);
   hist_light_flavor_stack_2->SetBinError(6,1.993289);
   hist_light_flavor_stack_2->SetBinError(7,2.866136);
   hist_light_flavor_stack_2->SetBinError(8,3.442808);
   hist_light_flavor_stack_2->SetBinError(9,4.123037);
   hist_light_flavor_stack_2->SetBinError(10,1.670046);
   hist_light_flavor_stack_2->SetBinError(11,1.08009);
   hist_light_flavor_stack_2->SetBinError(12,0.5316525);
   hist_light_flavor_stack_2->SetBinError(13,0.7672042);
   hist_light_flavor_stack_2->SetBinError(14,0.3320017);
   hist_light_flavor_stack_2->SetBinError(15,0.5147385);
   hist_light_flavor_stack_2->SetBinError(16,0.5594261);
   hist_light_flavor_stack_2->SetBinError(17,0.4635174);
   hist_light_flavor_stack_2->SetBinError(18,0.1993634);
   hist_light_flavor_stack_2->SetBinError(19,0.08689713);
   hist_light_flavor_stack_2->SetBinError(20,1.966138);
   hist_light_flavor_stack_2->SetBinError(21,0.3564455);
   hist_light_flavor_stack_2->SetBinError(22,0.06117038);
   hist_light_flavor_stack_2->SetBinError(23,0.09323675);
   hist_light_flavor_stack_2->SetBinError(24,1.103289);
   hist_light_flavor_stack_2->SetBinError(25,0.2540853);
   hist_light_flavor_stack_2->SetBinError(26,0.165378);
   hist_light_flavor_stack_2->SetBinError(27,0.5821398);
   hist_light_flavor_stack_2->SetBinError(28,0.8443818);
   hist_light_flavor_stack_2->SetBinError(29,0.6012523);
   hist_light_flavor_stack_2->SetBinError(30,2.478059);
   hist_light_flavor_stack_2->SetEntries(1480);
   hist_light_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_2->SetFillColor(ci);
   hist_light_flavor_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_light_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_2,"");
   
   TH1F *hist_heavy_flavor_stack_3 = new TH1F("hist_heavy_flavor_stack_3","heavy flavor",30,0,150);
   hist_heavy_flavor_stack_3->SetBinContent(4,20.82672);
   hist_heavy_flavor_stack_3->SetBinContent(5,11.33355);
   hist_heavy_flavor_stack_3->SetBinContent(6,6.702657);
   hist_heavy_flavor_stack_3->SetBinContent(7,2.877467);
   hist_heavy_flavor_stack_3->SetBinContent(8,2.961903);
   hist_heavy_flavor_stack_3->SetBinContent(9,1.189664);
   hist_heavy_flavor_stack_3->SetBinContent(10,0.6986575);
   hist_heavy_flavor_stack_3->SetBinContent(11,0.6274651);
   hist_heavy_flavor_stack_3->SetBinContent(12,0.7008331);
   hist_heavy_flavor_stack_3->SetBinContent(13,0.7614549);
   hist_heavy_flavor_stack_3->SetBinContent(14,0.4191225);
   hist_heavy_flavor_stack_3->SetBinContent(15,0.3120678);
   hist_heavy_flavor_stack_3->SetBinContent(16,0.1266685);
   hist_heavy_flavor_stack_3->SetBinContent(17,0.255286);
   hist_heavy_flavor_stack_3->SetBinContent(18,0.229975);
   hist_heavy_flavor_stack_3->SetBinContent(19,0.07306326);
   hist_heavy_flavor_stack_3->SetBinContent(20,0.2052126);
   hist_heavy_flavor_stack_3->SetBinContent(21,0.07749178);
   hist_heavy_flavor_stack_3->SetBinContent(22,0.2859155);
   hist_heavy_flavor_stack_3->SetBinContent(23,0.0769255);
   hist_heavy_flavor_stack_3->SetBinContent(24,0.1900759);
   hist_heavy_flavor_stack_3->SetBinContent(25,0.01951847);
   hist_heavy_flavor_stack_3->SetBinContent(26,0.04468149);
   hist_heavy_flavor_stack_3->SetBinContent(27,0.03959479);
   hist_heavy_flavor_stack_3->SetBinContent(28,0.03214445);
   hist_heavy_flavor_stack_3->SetBinContent(29,0.01946677);
   hist_heavy_flavor_stack_3->SetBinContent(30,0.8192369);
   hist_heavy_flavor_stack_3->SetBinError(4,1.474539);
   hist_heavy_flavor_stack_3->SetBinError(5,1.05312);
   hist_heavy_flavor_stack_3->SetBinError(6,1.347182);
   hist_heavy_flavor_stack_3->SetBinError(7,0.3810236);
   hist_heavy_flavor_stack_3->SetBinError(8,0.5979223);
   hist_heavy_flavor_stack_3->SetBinError(9,0.2451393);
   hist_heavy_flavor_stack_3->SetBinError(10,0.1678636);
   hist_heavy_flavor_stack_3->SetBinError(11,0.1676759);
   hist_heavy_flavor_stack_3->SetBinError(12,0.1711558);
   hist_heavy_flavor_stack_3->SetBinError(13,0.1734354);
   hist_heavy_flavor_stack_3->SetBinError(14,0.2712938);
   hist_heavy_flavor_stack_3->SetBinError(15,0.1094459);
   hist_heavy_flavor_stack_3->SetBinError(16,0.07328532);
   hist_heavy_flavor_stack_3->SetBinError(17,0.1435646);
   hist_heavy_flavor_stack_3->SetBinError(18,0.158994);
   hist_heavy_flavor_stack_3->SetBinError(19,0.05124512);
   hist_heavy_flavor_stack_3->SetBinError(20,0.09197302);
   hist_heavy_flavor_stack_3->SetBinError(21,0.054795);
   hist_heavy_flavor_stack_3->SetBinError(22,0.2068949);
   hist_heavy_flavor_stack_3->SetBinError(23,0.05442148);
   hist_heavy_flavor_stack_3->SetBinError(24,0.09755078);
   hist_heavy_flavor_stack_3->SetBinError(25,0.01372181);
   hist_heavy_flavor_stack_3->SetBinError(26,0.04173081);
   hist_heavy_flavor_stack_3->SetBinError(27,0.03959479);
   hist_heavy_flavor_stack_3->SetBinError(28,0.03214445);
   hist_heavy_flavor_stack_3->SetBinError(29,0.01946678);
   hist_heavy_flavor_stack_3->SetBinError(30,0.3238054);
   hist_heavy_flavor_stack_3->SetEntries(1149);
   hist_heavy_flavor_stack_3->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_3->SetFillColor(ci);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_heavy_flavor_stack_3->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_3->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_3->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3017[33] = {
   0,
   2.5,
   7.5,
   12.5,
   17.5,
   22.5,
   27.5,
   32.5,
   37.5,
   42.5,
   47.5,
   52.5,
   57.5,
   62.5,
   67.5,
   72.5,
   77.5,
   82.5,
   87.5,
   92.5,
   97.5,
   102.5,
   107.5,
   112.5,
   117.5,
   122.5,
   127.5,
   132.5,
   137.5,
   142.5,
   147.5,
   152.5,
   157.5};
   Double_t totalStackErr_fy3017[33] = {
   0,
   0,
   0,
   0,
   216.5495,
   175.8485,
   75.35932,
   49.00875,
   51.77985,
   21.89117,
   21.5724,
   14.33029,
   11.38983,
   9.086676,
   6.182587,
   4.622999,
   3.864878,
   3.881573,
   2.803759,
   -0.1258605,
   8.326231,
   2.835209,
   0.9071883,
   3.377392,
   0.4132906,
   1.087032,
   -1.190373,
   1.969405,
   1.559546,
   1.422081,
   17.89311,
   0,
   0};
   Double_t totalStackErr_felx3017[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fely3017[33] = {
   0,
   0,
   0,
   0,
   17.8943,
   16.36315,
   9.701713,
   16.1996,
   10.85913,
   6.124373,
   4.613259,
   3.387988,
   2.404856,
   2.286328,
   4.527325,
   1.06856,
   1.236418,
   1.043166,
   1.216539,
   1.239807,
   3.444031,
   1.023203,
   0.2545613,
   1.491551,
   1.167835,
   0.4028985,
   1.932733,
   0.7538729,
   0.8611165,
   0.6242261,
   2.965826,
   0,
   0};
   Double_t totalStackErr_fehx3017[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fehy3017[33] = {
   0,
   0,
   0,
   0,
   17.8943,
   16.36315,
   9.701713,
   16.1996,
   10.85913,
   6.124373,
   4.613259,
   3.387988,
   2.404856,
   2.286328,
   4.527325,
   1.06856,
   1.236418,
   1.043166,
   1.216539,
   1.239807,
   3.444031,
   1.023203,
   0.2545613,
   1.491551,
   1.167835,
   0.4028985,
   1.932733,
   0.7538729,
   0.8611165,
   0.6242261,
   2.965826,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(33,totalStackErr_fx3017,totalStackErr_fy3017,totalStackErr_felx3017,totalStackErr_fehx3017,totalStackErr_fely3017,totalStackErr_fehy3017);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3017 = new TH1F("Graph_totalStackErr3017","SM",100,0,176);
   Graph_totalStackErr3017->SetMinimum(-26.8798);
   Graph_totalStackErr3017->SetMaximum(258.2005);
   Graph_totalStackErr3017->SetDirectory(0);
   Graph_totalStackErr3017->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3017->SetLineColor(ci);
   Graph_totalStackErr3017->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3017->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3017->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3017->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3017->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3017);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_1"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_2"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_3"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutZenriMll/lepPt_antiID\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"3l channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",30,0,150);
   Graph_master_copy->SetMinimum(-3.448121);
   Graph_master_copy->SetMaximum(400);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutZenriMll_lepPt_antiID->cd();
   CutZenriMll_lepPt_antiID->Modified();
   CutZenriMll_lepPt_antiID->cd();
   CutZenriMll_lepPt_antiID->SetSelected(CutZenriMll_lepPt_antiID);
}
