void run2-trilep-CutZdefi_bestWithWWWnode_bveto-lepPtCorr_antiID-lin()
{
//=========Macro generated from canvas: CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID/CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID
//=========  (Fri Mar  3 15:29:19 2023) by ROOT version 6.18/04
   TCanvas *CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID = new TCanvas("CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID", "CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetHighLightColor(2);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->Range(0,0,1,1);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetFillColor(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetBorderMode(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetBorderSize(2);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetLeftMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetRightMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetTopMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetBottomMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-8.321072,159.4937,42.54321);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",30,0,150);
   Graph_master->SetMinimum(-0.1827864);
   Graph_master->SetMaximum(40);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   
   TH1F *stack_stack_1 = new TH1F("stack_stack_1","Background Stack",30,0,150);
   stack_stack_1->SetMinimum(-0.1827864);
   stack_stack_1->SetMaximum(21.31759);
   stack_stack_1->SetDirectory(0);
   stack_stack_1->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_1->SetLineColor(ci);
   stack_stack_1->GetXaxis()->SetLabelFont(42);
   stack_stack_1->GetXaxis()->SetLabelSize(0.035);
   stack_stack_1->GetXaxis()->SetTitleSize(0.035);
   stack_stack_1->GetXaxis()->SetTitleOffset(1);
   stack_stack_1->GetXaxis()->SetTitleFont(42);
   stack_stack_1->GetYaxis()->SetLabelFont(42);
   stack_stack_1->GetYaxis()->SetLabelSize(0.035);
   stack_stack_1->GetYaxis()->SetTitleSize(0.035);
   stack_stack_1->GetYaxis()->SetTitleFont(42);
   stack_stack_1->GetZaxis()->SetLabelFont(42);
   stack_stack_1->GetZaxis()->SetLabelSize(0.035);
   stack_stack_1->GetZaxis()->SetTitleSize(0.035);
   stack_stack_1->GetZaxis()->SetTitleOffset(1);
   stack_stack_1->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_1);
   
   
   TH1F *hist_Zjets_stack_1 = new TH1F("hist_Zjets_stack_1","$Z+$jets",30,0,150);
   hist_Zjets_stack_1->SetBinContent(4,0.2689601);
   hist_Zjets_stack_1->SetBinContent(6,1.499398);
   hist_Zjets_stack_1->SetBinContent(30,3.412557);
   hist_Zjets_stack_1->SetBinError(4,0.1901835);
   hist_Zjets_stack_1->SetBinError(6,1.499398);
   hist_Zjets_stack_1->SetBinError(30,2.413119);
   hist_Zjets_stack_1->SetEntries(5);
   hist_Zjets_stack_1->SetStats(0);
   hist_Zjets_stack_1->SetFillColor(210);
   hist_Zjets_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Zjets_stack_1->GetXaxis()->SetLabelFont(42);
   hist_Zjets_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_Zjets_stack_1->GetXaxis()->SetTitleFont(42);
   hist_Zjets_stack_1->GetYaxis()->SetLabelFont(42);
   hist_Zjets_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_1->GetYaxis()->SetTitleFont(42);
   hist_Zjets_stack_1->GetZaxis()->SetLabelFont(42);
   hist_Zjets_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_Zjets_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zjets_stack_1,"");
   
   TH1F *hist_Zgamma_stack_2 = new TH1F("hist_Zgamma_stack_2","$Z+\\gamma$",30,0,150);
   hist_Zgamma_stack_2->SetBinContent(4,9.712031);
   hist_Zgamma_stack_2->SetBinContent(5,1.778048);
   hist_Zgamma_stack_2->SetBinContent(6,2.898007e-05);
   hist_Zgamma_stack_2->SetBinContent(8,0.1340368);
   hist_Zgamma_stack_2->SetBinContent(9,0.2317355);
   hist_Zgamma_stack_2->SetBinContent(10,0.0748555);
   hist_Zgamma_stack_2->SetBinContent(11,0.08299237);
   hist_Zgamma_stack_2->SetBinContent(12,0.06493864);
   hist_Zgamma_stack_2->SetBinContent(13,0.04798354);
   hist_Zgamma_stack_2->SetBinContent(14,0.03051291);
   hist_Zgamma_stack_2->SetBinContent(15,0.4445291);
   hist_Zgamma_stack_2->SetBinContent(16,0.00810281);
   hist_Zgamma_stack_2->SetBinContent(17,0.02449794);
   hist_Zgamma_stack_2->SetBinContent(18,0.01257973);
   hist_Zgamma_stack_2->SetBinContent(19,0.01926365);
   hist_Zgamma_stack_2->SetBinContent(20,0.1609358);
   hist_Zgamma_stack_2->SetBinContent(21,0.6404564);
   hist_Zgamma_stack_2->SetBinContent(22,0.2976187);
   hist_Zgamma_stack_2->SetBinContent(23,0.01145369);
   hist_Zgamma_stack_2->SetBinContent(24,0.006185425);
   hist_Zgamma_stack_2->SetBinContent(25,1.541283e-05);
   hist_Zgamma_stack_2->SetBinContent(26,0.004894448);
   hist_Zgamma_stack_2->SetBinContent(27,0.007264245);
   hist_Zgamma_stack_2->SetBinContent(28,0.01826312);
   hist_Zgamma_stack_2->SetBinContent(29,0.1370237);
   hist_Zgamma_stack_2->SetBinContent(30,0.1702749);
   hist_Zgamma_stack_2->SetBinError(4,5.567388);
   hist_Zgamma_stack_2->SetBinError(5,1.089082);
   hist_Zgamma_stack_2->SetBinError(6,2.898007e-05);
   hist_Zgamma_stack_2->SetBinError(8,0.1340368);
   hist_Zgamma_stack_2->SetBinError(9,0.1949103);
   hist_Zgamma_stack_2->SetBinError(10,0.05912333);
   hist_Zgamma_stack_2->SetBinError(11,0.04648227);
   hist_Zgamma_stack_2->SetBinError(12,0.03751553);
   hist_Zgamma_stack_2->SetBinError(13,0.02498673);
   hist_Zgamma_stack_2->SetBinError(14,0.02059247);
   hist_Zgamma_stack_2->SetBinError(15,0.4072733);
   hist_Zgamma_stack_2->SetBinError(16,0.007784137);
   hist_Zgamma_stack_2->SetBinError(17,0.01427077);
   hist_Zgamma_stack_2->SetBinError(18,0.008946871);
   hist_Zgamma_stack_2->SetBinError(19,0.01115719);
   hist_Zgamma_stack_2->SetBinError(20,0.157692);
   hist_Zgamma_stack_2->SetBinError(21,0.6262684);
   hist_Zgamma_stack_2->SetBinError(22,0.2794029);
   hist_Zgamma_stack_2->SetBinError(23,0.006510698);
   hist_Zgamma_stack_2->SetBinError(24,0.004397927);
   hist_Zgamma_stack_2->SetBinError(25,1.541283e-05);
   hist_Zgamma_stack_2->SetBinError(26,0.003462359);
   hist_Zgamma_stack_2->SetBinError(27,0.004206046);
   hist_Zgamma_stack_2->SetBinError(28,0.01642594);
   hist_Zgamma_stack_2->SetBinError(29,0.1292772);
   hist_Zgamma_stack_2->SetBinError(30,0.06043738);
   hist_Zgamma_stack_2->SetEntries(308);
   hist_Zgamma_stack_2->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_Zgamma_stack_2->SetFillColor(ci);
   hist_Zgamma_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Zgamma_stack_2->GetXaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleFont(42);
   hist_Zgamma_stack_2->GetYaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetYaxis()->SetTitleFont(42);
   hist_Zgamma_stack_2->GetZaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zgamma_stack_2,"");
   
   TH1F *hist_WW_stack_3 = new TH1F("hist_WW_stack_3","$WW$",30,0,150);
   hist_WW_stack_3->SetBinContent(4,1.724885);
   hist_WW_stack_3->SetBinContent(5,1.316497);
   hist_WW_stack_3->SetBinContent(6,0.5038491);
   hist_WW_stack_3->SetBinContent(7,0.3681799);
   hist_WW_stack_3->SetBinContent(8,0.1125316);
   hist_WW_stack_3->SetBinContent(9,0.1674142);
   hist_WW_stack_3->SetBinContent(10,0.210795);
   hist_WW_stack_3->SetBinContent(11,-0.04031954);
   hist_WW_stack_3->SetBinContent(12,0.06925543);
   hist_WW_stack_3->SetBinContent(13,0.07580207);
   hist_WW_stack_3->SetBinContent(14,0.02706876);
   hist_WW_stack_3->SetBinContent(15,0.1103619);
   hist_WW_stack_3->SetBinContent(16,-0.06096904);
   hist_WW_stack_3->SetBinContent(17,0.02949973);
   hist_WW_stack_3->SetBinContent(18,0.01821624);
   hist_WW_stack_3->SetBinContent(20,0.05002715);
   hist_WW_stack_3->SetBinContent(21,0.01360748);
   hist_WW_stack_3->SetBinContent(22,0.0406206);
   hist_WW_stack_3->SetBinContent(23,0.02663141);
   hist_WW_stack_3->SetBinContent(24,0.01469084);
   hist_WW_stack_3->SetBinContent(25,0.04303699);
   hist_WW_stack_3->SetBinContent(26,0.01558048);
   hist_WW_stack_3->SetBinContent(27,0.01750951);
   hist_WW_stack_3->SetBinContent(28,0.03178091);
   hist_WW_stack_3->SetBinContent(30,0.847756);
   hist_WW_stack_3->SetBinError(4,0.410136);
   hist_WW_stack_3->SetBinError(5,0.435291);
   hist_WW_stack_3->SetBinError(6,0.1851453);
   hist_WW_stack_3->SetBinError(7,0.1555955);
   hist_WW_stack_3->SetBinError(8,0.1412932);
   hist_WW_stack_3->SetBinError(9,0.1857316);
   hist_WW_stack_3->SetBinError(10,0.1480796);
   hist_WW_stack_3->SetBinError(11,0.1424669);
   hist_WW_stack_3->SetBinError(12,0.0317689);
   hist_WW_stack_3->SetBinError(13,0.03081161);
   hist_WW_stack_3->SetBinError(14,0.01694855);
   hist_WW_stack_3->SetBinError(15,0.06849014);
   hist_WW_stack_3->SetBinError(16,0.0712617);
   hist_WW_stack_3->SetBinError(17,0.01706501);
   hist_WW_stack_3->SetBinError(18,0.01088113);
   hist_WW_stack_3->SetBinError(20,0.04024886);
   hist_WW_stack_3->SetBinError(21,0.01360748);
   hist_WW_stack_3->SetBinError(22,0.02483846);
   hist_WW_stack_3->SetBinError(23,0.01609578);
   hist_WW_stack_3->SetBinError(24,0.0105089);
   hist_WW_stack_3->SetBinError(25,0.03459908);
   hist_WW_stack_3->SetBinError(26,0.0113249);
   hist_WW_stack_3->SetBinError(27,0.0113559);
   hist_WW_stack_3->SetBinError(28,0.01853604);
   hist_WW_stack_3->SetBinError(30,0.135041);
   hist_WW_stack_3->SetEntries(298);
   hist_WW_stack_3->SetStats(0);

   ci = TColor::GetColor("#6666cc");
   hist_WW_stack_3->SetFillColor(ci);
   hist_WW_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_WW_stack_3->GetXaxis()->SetLabelFont(42);
   hist_WW_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_WW_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_WW_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_WW_stack_3->GetXaxis()->SetTitleFont(42);
   hist_WW_stack_3->GetYaxis()->SetLabelFont(42);
   hist_WW_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_WW_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_WW_stack_3->GetYaxis()->SetTitleFont(42);
   hist_WW_stack_3->GetZaxis()->SetLabelFont(42);
   hist_WW_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_WW_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_WW_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_WW_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WW_stack_3,"");
   
   TH1F *hist_tt_stack_4 = new TH1F("hist_tt_stack_4","$t\\bar{t}$",30,0,150);
   hist_tt_stack_4->SetBinContent(4,6.946964);
   hist_tt_stack_4->SetBinContent(5,4.35205);
   hist_tt_stack_4->SetBinContent(6,2.97869);
   hist_tt_stack_4->SetBinContent(7,2.206653);
   hist_tt_stack_4->SetBinContent(8,1.971528);
   hist_tt_stack_4->SetBinContent(9,1.89036);
   hist_tt_stack_4->SetBinContent(10,1.072002);
   hist_tt_stack_4->SetBinContent(11,0.795693);
   hist_tt_stack_4->SetBinContent(12,0.7087613);
   hist_tt_stack_4->SetBinContent(13,0.6146212);
   hist_tt_stack_4->SetBinContent(14,0.514728);
   hist_tt_stack_4->SetBinContent(15,0.3348725);
   hist_tt_stack_4->SetBinContent(16,0.3152391);
   hist_tt_stack_4->SetBinContent(17,0.285979);
   hist_tt_stack_4->SetBinContent(18,0.1848843);
   hist_tt_stack_4->SetBinContent(19,0.1394345);
   hist_tt_stack_4->SetBinContent(20,0.1246316);
   hist_tt_stack_4->SetBinContent(21,0.04098525);
   hist_tt_stack_4->SetBinContent(22,0.2053525);
   hist_tt_stack_4->SetBinContent(23,0.07762554);
   hist_tt_stack_4->SetBinContent(24,0.05153564);
   hist_tt_stack_4->SetBinContent(26,0.03790804);
   hist_tt_stack_4->SetBinContent(29,0.09421661);
   hist_tt_stack_4->SetBinContent(30,0.5876014);
   hist_tt_stack_4->SetBinError(4,0.5363014);
   hist_tt_stack_4->SetBinError(5,0.4240749);
   hist_tt_stack_4->SetBinError(6,0.3497179);
   hist_tt_stack_4->SetBinError(7,0.3063616);
   hist_tt_stack_4->SetBinError(8,0.2802201);
   hist_tt_stack_4->SetBinError(9,0.2913145);
   hist_tt_stack_4->SetBinError(10,0.2116754);
   hist_tt_stack_4->SetBinError(11,0.1830121);
   hist_tt_stack_4->SetBinError(12,0.1723348);
   hist_tt_stack_4->SetBinError(13,0.1537656);
   hist_tt_stack_4->SetBinError(14,0.155229);
   hist_tt_stack_4->SetBinError(15,0.1154986);
   hist_tt_stack_4->SetBinError(16,0.1125269);
   hist_tt_stack_4->SetBinError(17,0.1083548);
   hist_tt_stack_4->SetBinError(18,0.09267973);
   hist_tt_stack_4->SetBinError(19,0.08075932);
   hist_tt_stack_4->SetBinError(20,0.07198823);
   hist_tt_stack_4->SetBinError(21,0.04098525);
   hist_tt_stack_4->SetBinError(22,0.08926678);
   hist_tt_stack_4->SetBinError(23,0.05492352);
   hist_tt_stack_4->SetBinError(24,0.0380124);
   hist_tt_stack_4->SetBinError(26,0.03790804);
   hist_tt_stack_4->SetBinError(29,0.0667185);
   hist_tt_stack_4->SetBinError(30,0.1508121);
   hist_tt_stack_4->SetEntries(691);
   hist_tt_stack_4->SetStats(0);
   hist_tt_stack_4->SetFillColor(219);
   hist_tt_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_tt_stack_4->GetXaxis()->SetLabelFont(42);
   hist_tt_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_tt_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_tt_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_tt_stack_4->GetXaxis()->SetTitleFont(42);
   hist_tt_stack_4->GetYaxis()->SetLabelFont(42);
   hist_tt_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_tt_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_tt_stack_4->GetYaxis()->SetTitleFont(42);
   hist_tt_stack_4->GetZaxis()->SetLabelFont(42);
   hist_tt_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_tt_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_tt_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_tt_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tt_stack_4,"");
   
   TH1F *hist_Wt_stack_5 = new TH1F("hist_Wt_stack_5","$tW$",30,0,150);
   hist_Wt_stack_5->SetBinContent(4,1.486287);
   hist_Wt_stack_5->SetBinContent(5,0.9701038);
   hist_Wt_stack_5->SetBinContent(6,0.283856);
   hist_Wt_stack_5->SetBinContent(7,0.5679677);
   hist_Wt_stack_5->SetBinContent(8,0.2709474);
   hist_Wt_stack_5->SetBinContent(9,0.2465709);
   hist_Wt_stack_5->SetBinContent(10,0.2533197);
   hist_Wt_stack_5->SetBinContent(11,0.4367309);
   hist_Wt_stack_5->SetBinContent(13,0.1216579);
   hist_Wt_stack_5->SetBinContent(17,0.1186514);
   hist_Wt_stack_5->SetBinContent(18,0.1198428);
   hist_Wt_stack_5->SetBinContent(22,0.1476738);
   hist_Wt_stack_5->SetBinContent(30,0.1442687);
   hist_Wt_stack_5->SetBinError(4,0.4510655);
   hist_Wt_stack_5->SetBinError(5,0.3712981);
   hist_Wt_stack_5->SetBinError(6,0.2010381);
   hist_Wt_stack_5->SetBinError(7,0.2855161);
   hist_Wt_stack_5->SetBinError(8,0.1756664);
   hist_Wt_stack_5->SetBinError(9,0.1752853);
   hist_Wt_stack_5->SetBinError(10,0.1796023);
   hist_Wt_stack_5->SetBinError(11,0.2522823);
   hist_Wt_stack_5->SetBinError(13,0.1216579);
   hist_Wt_stack_5->SetBinError(17,0.1186514);
   hist_Wt_stack_5->SetBinError(18,0.1198428);
   hist_Wt_stack_5->SetBinError(22,0.1476738);
   hist_Wt_stack_5->SetBinError(30,0.1442687);
   hist_Wt_stack_5->SetEntries(39);
   hist_Wt_stack_5->SetStats(0);
   hist_Wt_stack_5->SetFillColor(218);
   hist_Wt_stack_5->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Wt_stack_5->GetXaxis()->SetLabelFont(42);
   hist_Wt_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_Wt_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_Wt_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_Wt_stack_5->GetXaxis()->SetTitleFont(42);
   hist_Wt_stack_5->GetYaxis()->SetLabelFont(42);
   hist_Wt_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_Wt_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_Wt_stack_5->GetYaxis()->SetTitleFont(42);
   hist_Wt_stack_5->GetZaxis()->SetLabelFont(42);
   hist_Wt_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_Wt_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_Wt_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_Wt_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wt_stack_5,"");
   
   TH1F *hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6 = new TH1F("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6","Other fakes",30,0,150);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(4,0.1633371);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(5,0.067284);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(6,0.02694664);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(7,0.1666027);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(8,0.06716951);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(12,0.05814145);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(17,0.06467249);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(19,0.06322329);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinContent(30,0.1473328);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(4,0.0965045);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(5,0.06728399);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(6,0.02694664);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(7,0.1034633);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(8,0.06716951);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(12,0.05814145);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(17,0.06467249);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(19,0.0632233);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetBinError(30,0.09315185);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetEntries(17);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetStats(0);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->SetFillColor(15);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetXaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetYaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetYaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetZaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetZaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3001[33] = {
   0,
   2.5,
   7.5,
   12.5,
   17.5,
   22.5,
   27.5,
   32.5,
   37.5,
   42.5,
   47.5,
   52.5,
   57.5,
   62.5,
   67.5,
   72.5,
   77.5,
   82.5,
   87.5,
   92.5,
   97.5,
   102.5,
   107.5,
   112.5,
   117.5,
   122.5,
   127.5,
   132.5,
   137.5,
   142.5,
   147.5,
   152.5,
   157.5};
   Double_t totalStackErr_fy3001[33] = {
   0,
   0,
   0,
   0,
   20.30246,
   8.483983,
   5.292768,
   3.309403,
   2.556214,
   2.53608,
   1.610972,
   1.275097,
   0.9010968,
   0.8600646,
   0.5723097,
   0.8897634,
   0.2623729,
   0.5233005,
   0.3355231,
   0.2219214,
   0.3355946,
   0.6950492,
   0.6912656,
   0.1157106,
   0.0724119,
   0.0430524,
   0.05838297,
   0.02477376,
   0.05004403,
   0.2312403,
   5.30979,
   0,
   0};
   Double_t totalStackErr_felx3001[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fely3001[33] = {
   0,
   0,
   0,
   0,
   5.630327,
   1.302999,
   1.563944,
   0.4585756,
   0.3896452,
   0.4336765,
   0.3201352,
   0.3458283,
   0.1884048,
   0.2000455,
   0.1575034,
   0.4288384,
   0.1334209,
   0.1746319,
   0.1521522,
   0.1031685,
   0.177958,
   0.6277556,
   0.3293312,
   0.05760258,
   0.03968276,
   0.03459908,
   0.03971474,
   0.0121098,
   0.02476683,
   0.1454784,
   2.428429,
   0,
   0};
   Double_t totalStackErr_fehx3001[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fehy3001[33] = {
   0,
   0,
   0,
   0,
   5.630327,
   1.302999,
   1.563944,
   0.4585756,
   0.3896452,
   0.4336765,
   0.3201352,
   0.3458283,
   0.1884048,
   0.2000455,
   0.1575034,
   0.4288384,
   0.1334209,
   0.1746319,
   0.1521522,
   0.1031685,
   0.177958,
   0.6277556,
   0.3293312,
   0.05760258,
   0.03968276,
   0.03459908,
   0.03971474,
   0.0121098,
   0.02476683,
   0.1454784,
   2.428429,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(33,totalStackErr_fx3001,totalStackErr_fy3001,totalStackErr_felx3001,totalStackErr_fehx3001,totalStackErr_fely3001,totalStackErr_fehy3001);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3001 = new TH1F("Graph_totalStackErr3001","SM",100,0,176);
   Graph_totalStackErr3001->SetMinimum(0);
   Graph_totalStackErr3001->SetMaximum(28.52607);
   Graph_totalStackErr3001->SetDirectory(0);
   Graph_totalStackErr3001->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3001->SetLineColor(ci);
   Graph_totalStackErr3001->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3001->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3001->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3001->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3001->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3001->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3001->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3001->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3001->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3001->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3001->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3001->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3001->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3001->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3001);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zjets_stack_1"," #it{Z+}jets","f");
   entry->SetFillColor(210);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zgamma_stack_2"," #it{Z+#gamma}","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WW_stack_3"," #it{WW}","f");

   ci = TColor::GetColor("#6666cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tt_stack_4"," #it{t#bar{t}}","f");
   entry->SetFillColor(219);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wt_stack_5"," #it{tW}","f");
   entry->SetFillColor(218);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_6"," Other fakes","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutZdefi_bestWithWWWnode_bveto/lepPtCorr_antiID\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"3l channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",30,0,150);
   Graph_master_copy->SetMinimum(-0.1827864);
   Graph_master_copy->SetMaximum(40);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID->SetSelected(CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiID);
}
