void run2-trilep-CutZdefi_bestWithWWWnode_bveto-lepPtCorr_antiIDFFbins-log()
{
//=========Macro generated from canvas: CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins/CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins
//=========  (Fri Mar  3 15:29:20 2023) by ROOT version 6.18/04
   TCanvas *CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins = new TCanvas("CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins", "CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetHighLightColor(2);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->Range(0,0,1,1);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetFillColor(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetBorderMode(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetBorderSize(2);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetLeftMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetRightMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetTopMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetBottomMargin(0);
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-1.800859,159.4937,3.204511);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis10[4] = {0, 15, 30, 150}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",3, xAxis10);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(900);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis11[4] = {0, 15, 30, 150}; 
   
   TH1F *stack_stack_4 = new TH1F("stack_stack_4","Background Stack",3, xAxis11);
   stack_stack_4->SetMinimum(0.01363169);
   stack_stack_4->SetMaximum(54.52675);
   stack_stack_4->SetDirectory(0);
   stack_stack_4->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_4->SetLineColor(ci);
   stack_stack_4->GetXaxis()->SetLabelFont(42);
   stack_stack_4->GetXaxis()->SetLabelSize(0.035);
   stack_stack_4->GetXaxis()->SetTitleSize(0.035);
   stack_stack_4->GetXaxis()->SetTitleOffset(1);
   stack_stack_4->GetXaxis()->SetTitleFont(42);
   stack_stack_4->GetYaxis()->SetLabelFont(42);
   stack_stack_4->GetYaxis()->SetLabelSize(0.035);
   stack_stack_4->GetYaxis()->SetTitleSize(0.035);
   stack_stack_4->GetYaxis()->SetTitleFont(42);
   stack_stack_4->GetZaxis()->SetLabelFont(42);
   stack_stack_4->GetZaxis()->SetLabelSize(0.035);
   stack_stack_4->GetZaxis()->SetTitleSize(0.035);
   stack_stack_4->GetZaxis()->SetTitleOffset(1);
   stack_stack_4->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_4);
   
   Double_t xAxis12[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1 = new TH1F("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1","Other fakes",3, xAxis12);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetBinContent(2,0.2575678);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetBinContent(3,0.5671422);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetBinError(2,0.1206912);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetBinError(3,0.1882918);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetEntries(17);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetStats(0);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->SetFillColor(15);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetXaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetYaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetYaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetZaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetZaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1,"");
   Double_t xAxis13[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_Wt_stack_2 = new TH1F("hist_Wt_stack_2","$tW$",3, xAxis13);
   hist_Wt_stack_2->SetBinContent(2,2.740247);
   hist_Wt_stack_2->SetBinContent(3,2.427631);
   hist_Wt_stack_2->SetBinError(2,0.61785);
   hist_Wt_stack_2->SetBinError(3,0.5699724);
   hist_Wt_stack_2->SetEntries(39);
   hist_Wt_stack_2->SetStats(0);
   hist_Wt_stack_2->SetFillColor(218);
   hist_Wt_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Wt_stack_2->GetXaxis()->SetLabelFont(42);
   hist_Wt_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_Wt_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_Wt_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_Wt_stack_2->GetXaxis()->SetTitleFont(42);
   hist_Wt_stack_2->GetYaxis()->SetLabelFont(42);
   hist_Wt_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_Wt_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_Wt_stack_2->GetYaxis()->SetTitleFont(42);
   hist_Wt_stack_2->GetZaxis()->SetLabelFont(42);
   hist_Wt_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_Wt_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_Wt_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_Wt_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wt_stack_2,"");
   Double_t xAxis14[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_Zjets_stack_3 = new TH1F("hist_Zjets_stack_3","$Z+$jets",3, xAxis14);
   hist_Zjets_stack_3->SetBinContent(2,1.768359);
   hist_Zjets_stack_3->SetBinContent(3,3.412557);
   hist_Zjets_stack_3->SetBinError(2,1.511412);
   hist_Zjets_stack_3->SetBinError(3,2.413119);
   hist_Zjets_stack_3->SetEntries(5);
   hist_Zjets_stack_3->SetStats(0);
   hist_Zjets_stack_3->SetFillColor(210);
   hist_Zjets_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Zjets_stack_3->GetXaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_Zjets_stack_3->GetXaxis()->SetTitleFont(42);
   hist_Zjets_stack_3->GetYaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetYaxis()->SetTitleFont(42);
   hist_Zjets_stack_3->GetZaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_Zjets_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zjets_stack_3,"");
   Double_t xAxis15[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_WW_stack_4 = new TH1F("hist_WW_stack_4","$WW$",3, xAxis15);
   hist_WW_stack_4->SetBinContent(2,3.54523);
   hist_WW_stack_4->SetBinContent(3,2.189077);
   hist_WW_stack_4->SetBinError(2,0.6260739);
   hist_WW_stack_4->SetBinError(3,0.3951512);
   hist_WW_stack_4->SetEntries(298);
   hist_WW_stack_4->SetStats(0);

   ci = TColor::GetColor("#6666cc");
   hist_WW_stack_4->SetFillColor(ci);
   hist_WW_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_WW_stack_4->GetXaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_WW_stack_4->GetXaxis()->SetTitleFont(42);
   hist_WW_stack_4->GetYaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetYaxis()->SetTitleFont(42);
   hist_WW_stack_4->GetZaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_WW_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WW_stack_4,"");
   Double_t xAxis16[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_Zgamma_stack_5 = new TH1F("hist_Zgamma_stack_5","$Z+\\gamma$",3, xAxis16);
   hist_Zgamma_stack_5->SetBinContent(2,11.49011);
   hist_Zgamma_stack_5->SetBinContent(3,2.630414);
   hist_Zgamma_stack_5->SetBinError(2,5.67291);
   hist_Zgamma_stack_5->SetBinError(3,0.8638747);
   hist_Zgamma_stack_5->SetEntries(308);
   hist_Zgamma_stack_5->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_Zgamma_stack_5->SetFillColor(ci);
   hist_Zgamma_stack_5->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Zgamma_stack_5->GetXaxis()->SetLabelFont(42);
   hist_Zgamma_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_5->GetXaxis()->SetTitleFont(42);
   hist_Zgamma_stack_5->GetYaxis()->SetLabelFont(42);
   hist_Zgamma_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_5->GetYaxis()->SetTitleFont(42);
   hist_Zgamma_stack_5->GetZaxis()->SetLabelFont(42);
   hist_Zgamma_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zgamma_stack_5,"");
   Double_t xAxis17[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_tt_stack_6 = new TH1F("hist_tt_stack_6","$t\\bar{t}$",3, xAxis17);
   hist_tt_stack_6->SetBinContent(2,14.27771);
   hist_tt_stack_6->SetBinContent(3,12.25461);
   hist_tt_stack_6->SetBinError(2,0.7679592);
   hist_tt_stack_6->SetBinError(3,0.7167729);
   hist_tt_stack_6->SetEntries(691);
   hist_tt_stack_6->SetStats(0);
   hist_tt_stack_6->SetFillColor(219);
   hist_tt_stack_6->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_tt_stack_6->GetXaxis()->SetLabelFont(42);
   hist_tt_stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_tt_stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_tt_stack_6->GetXaxis()->SetTitleOffset(1);
   hist_tt_stack_6->GetXaxis()->SetTitleFont(42);
   hist_tt_stack_6->GetYaxis()->SetLabelFont(42);
   hist_tt_stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_tt_stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_tt_stack_6->GetYaxis()->SetTitleFont(42);
   hist_tt_stack_6->GetZaxis()->SetLabelFont(42);
   hist_tt_stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_tt_stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_tt_stack_6->GetZaxis()->SetTitleOffset(1);
   hist_tt_stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tt_stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3004[6] = {
   0,
   7.5,
   22.5,
   90,
   175,
   225};
   Double_t totalStackErr_fy3004[6] = {
   0,
   0,
   34.07922,
   23.48143,
   0,
   0};
   Double_t totalStackErr_felx3004[6] = {
   0,
   7.5,
   7.5,
   60,
   60,
   60};
   Double_t totalStackErr_fely3004[6] = {
   0,
   0,
   5.987012,
   2.756747,
   0,
   0};
   Double_t totalStackErr_fehx3004[6] = {
   0,
   7.5,
   7.5,
   60,
   60,
   60};
   Double_t totalStackErr_fehy3004[6] = {
   0,
   0,
   5.987012,
   2.756747,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(6,totalStackErr_fx3004,totalStackErr_fy3004,totalStackErr_felx3004,totalStackErr_fehx3004,totalStackErr_fely3004,totalStackErr_fehy3004);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3004 = new TH1F("Graph_totalStackErr3004","SM",100,0,313.5);
   Graph_totalStackErr3004->SetMinimum(0.04407285);
   Graph_totalStackErr3004->SetMaximum(44.07285);
   Graph_totalStackErr3004->SetDirectory(0);
   Graph_totalStackErr3004->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3004->SetLineColor(ci);
   Graph_totalStackErr3004->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3004->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3004->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3004->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3004->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3004->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3004->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3004->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3004->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3004->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3004->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3004->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3004->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3004->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3004);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zjets_stack_3"," #it{Z+}jets","f");
   entry->SetFillColor(210);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zgamma_stack_5"," #it{Z+#gamma}","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WW_stack_4"," #it{WW}","f");

   ci = TColor::GetColor("#6666cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tt_stack_6"," #it{t#bar{t}}","f");
   entry->SetFillColor(219);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wt_stack_2"," #it{tW}","f");
   entry->SetFillColor(218);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_NonWW_triboson__stack_1"," Other fakes","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutZdefi_bestWithWWWnode_bveto/lepPtCorr_antiIDFFbins\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"3l channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis18[4] = {0, 15, 30, 150}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",3, xAxis18);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(900);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->Modified();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->cd();
   CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins->SetSelected(CutZdefi_bestWithWWWnode_bveto_lepPtCorr_antiIDFFbins);
}
