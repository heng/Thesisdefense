void run2-emme-CutMultiANN_SigANN_SR-multiANNPFlow_VH_VHbb-log()
{
//=========Macro generated from canvas: CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb/CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb
//=========  (Fri Mar  3 15:40:38 2023) by ROOT version 6.18/04
   TCanvas *CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb = new TCanvas("CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb", "CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetHighLightColor(2);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->Range(0,0,1,1);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetFillColor(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetBorderMode(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetBorderSize(2);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetLeftMargin(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetRightMargin(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetTopMargin(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetBottomMargin(0);
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(0.03797469,-1.765195,1.050633,3.017275);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis31[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",9, xAxis31);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(600);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("ANN_{multi,VH}");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis32[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *stack_stack_10 = new TH1F("stack_stack_10","Background Stack",9, xAxis32);
   stack_stack_10->SetMinimum(0.009588628);
   stack_stack_10->SetMaximum(38.35451);
   stack_stack_10->SetDirectory(0);
   stack_stack_10->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_10->SetLineColor(ci);
   stack_stack_10->GetXaxis()->SetLabelFont(42);
   stack_stack_10->GetXaxis()->SetLabelSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleOffset(1);
   stack_stack_10->GetXaxis()->SetTitleFont(42);
   stack_stack_10->GetYaxis()->SetLabelFont(42);
   stack_stack_10->GetYaxis()->SetLabelSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleFont(42);
   stack_stack_10->GetZaxis()->SetLabelFont(42);
   stack_stack_10->GetZaxis()->SetLabelSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleOffset(1);
   stack_stack_10->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_10);
   
   Double_t xAxis33[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *hist_light_flavor_stack_1 = new TH1F("hist_light_flavor_stack_1","light flavor",9, xAxis33);
   hist_light_flavor_stack_1->SetBinContent(1,3.074492);
   hist_light_flavor_stack_1->SetBinContent(2,-0.3641819);
   hist_light_flavor_stack_1->SetBinContent(3,0.2667826);
   hist_light_flavor_stack_1->SetBinContent(4,0.4704151);
   hist_light_flavor_stack_1->SetBinContent(5,0.001313378);
   hist_light_flavor_stack_1->SetBinContent(6,0.7952858);
   hist_light_flavor_stack_1->SetBinContent(7,0.371082);
   hist_light_flavor_stack_1->SetBinContent(8,0.3045012);
   hist_light_flavor_stack_1->SetBinContent(9,0.002090275);
   hist_light_flavor_stack_1->SetBinError(1,1.296164);
   hist_light_flavor_stack_1->SetBinError(2,1.274822);
   hist_light_flavor_stack_1->SetBinError(3,0.1819176);
   hist_light_flavor_stack_1->SetBinError(4,0.2813619);
   hist_light_flavor_stack_1->SetBinError(5,0.001313378);
   hist_light_flavor_stack_1->SetBinError(6,0.3550651);
   hist_light_flavor_stack_1->SetBinError(7,0.3511913);
   hist_light_flavor_stack_1->SetBinError(8,0.2028306);
   hist_light_flavor_stack_1->SetBinError(9,0.001727137);
   hist_light_flavor_stack_1->SetEntries(43);
   hist_light_flavor_stack_1->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_1->SetFillColor(ci);
   hist_light_flavor_stack_1->GetXaxis()->SetTitle("ANN_{multi,VH}");
   hist_light_flavor_stack_1->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_1->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_1->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_1->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_1,"");
   Double_t xAxis34[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *hist_heavy_flavor_stack_2 = new TH1F("hist_heavy_flavor_stack_2","heavy flavor",9, xAxis34);
   hist_heavy_flavor_stack_2->SetBinContent(1,4.763117);
   hist_heavy_flavor_stack_2->SetBinContent(2,3.98424);
   hist_heavy_flavor_stack_2->SetBinContent(3,1.475618);
   hist_heavy_flavor_stack_2->SetBinContent(4,1.336288);
   hist_heavy_flavor_stack_2->SetBinContent(5,1.679473);
   hist_heavy_flavor_stack_2->SetBinContent(6,1.124381);
   hist_heavy_flavor_stack_2->SetBinContent(7,0.8741581);
   hist_heavy_flavor_stack_2->SetBinContent(8,0.3648933);
   hist_heavy_flavor_stack_2->SetBinContent(9,0.1492494);
   hist_heavy_flavor_stack_2->SetBinError(1,0.7411079);
   hist_heavy_flavor_stack_2->SetBinError(2,0.7231436);
   hist_heavy_flavor_stack_2->SetBinError(3,0.4376835);
   hist_heavy_flavor_stack_2->SetBinError(4,0.3954286);
   hist_heavy_flavor_stack_2->SetBinError(5,0.4559625);
   hist_heavy_flavor_stack_2->SetBinError(6,0.3620797);
   hist_heavy_flavor_stack_2->SetBinError(7,0.3333726);
   hist_heavy_flavor_stack_2->SetBinError(8,0.2124024);
   hist_heavy_flavor_stack_2->SetBinError(9,0.1130592);
   hist_heavy_flavor_stack_2->SetEntries(138);
   hist_heavy_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_2->SetFillColor(ci);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitle("ANN_{multi,VH}");
   hist_heavy_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_2,"");
   Double_t xAxis35[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *hist_gamma_conversion_stack_3 = new TH1F("hist_gamma_conversion_stack_3","$\\gamma$ conv.",9, xAxis35);
   hist_gamma_conversion_stack_3->SetBinContent(1,10.75502);
   hist_gamma_conversion_stack_3->SetBinContent(2,17.85);
   hist_gamma_conversion_stack_3->SetBinContent(3,7.735362);
   hist_gamma_conversion_stack_3->SetBinContent(4,10.6561);
   hist_gamma_conversion_stack_3->SetBinContent(5,6.484022);
   hist_gamma_conversion_stack_3->SetBinContent(6,12.71574);
   hist_gamma_conversion_stack_3->SetBinContent(7,8.370427);
   hist_gamma_conversion_stack_3->SetBinContent(8,3.638084);
   hist_gamma_conversion_stack_3->SetBinContent(9,1.824853);
   hist_gamma_conversion_stack_3->SetBinError(1,5.444059);
   hist_gamma_conversion_stack_3->SetBinError(2,6.121571);
   hist_gamma_conversion_stack_3->SetBinError(3,6.409386);
   hist_gamma_conversion_stack_3->SetBinError(4,6.724636);
   hist_gamma_conversion_stack_3->SetBinError(5,3.327967);
   hist_gamma_conversion_stack_3->SetBinError(6,5.793852);
   hist_gamma_conversion_stack_3->SetBinError(7,4.312761);
   hist_gamma_conversion_stack_3->SetBinError(8,1.641134);
   hist_gamma_conversion_stack_3->SetBinError(9,0.8454703);
   hist_gamma_conversion_stack_3->SetEntries(244);
   hist_gamma_conversion_stack_3->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_3->SetFillColor(ci);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitle("ANN_{multi,VH}");
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3010[12] = {
   0,
   0.265,
   0.385,
   0.485,
   0.565,
   0.625,
   0.675,
   0.725,
   0.775,
   0.9,
   1.044444,
   1.133333};
   Double_t totalStackErr_fy3010[12] = {
   0,
   18.59263,
   21.47006,
   9.477762,
   12.4628,
   8.164807,
   14.63541,
   9.615667,
   4.307478,
   1.976193,
   0,
   0};
   Double_t totalStackErr_felx3010[12] = {
   0,
   0.065,
   0.055,
   0.045,
   0.035,
   0.025,
   0.025,
   0.025,
   0.025,
   0.1,
   0.1,
   0.1};
   Double_t totalStackErr_fely3010[12] = {
   0,
   5.645092,
   6.29458,
   6.426888,
   6.742126,
   3.359058,
   5.816003,
   4.339859,
   1.667206,
   0.8529979,
   0,
   0};
   Double_t totalStackErr_fehx3010[12] = {
   0,
   0.065,
   0.055,
   0.045,
   0.035,
   0.025,
   0.025,
   0.025,
   0.025,
   0.1,
   0.1,
   0.1};
   Double_t totalStackErr_fehy3010[12] = {
   0,
   5.645092,
   6.29458,
   6.426888,
   6.742126,
   3.359058,
   5.816003,
   4.339859,
   1.667206,
   0.8529979,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(12,totalStackErr_fx3010,totalStackErr_fy3010,totalStackErr_felx3010,totalStackErr_fehx3010,totalStackErr_fely3010,totalStackErr_fehy3010);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3010 = new TH1F("Graph_totalStackErr3010","SM",100,0,1.356667);
   Graph_totalStackErr3010->SetMinimum(0.0305411);
   Graph_totalStackErr3010->SetMaximum(30.5411);
   Graph_totalStackErr3010->SetDirectory(0);
   Graph_totalStackErr3010->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3010->SetLineColor(ci);
   Graph_totalStackErr3010->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3010);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_3"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_1"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_2"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutMultiANN_SigANN_SR/multiANNPFlow_VH_VHbb\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis36[10] = {0.2, 0.33, 0.44, 0.53, 0.6, 0.65, 0.7, 0.75, 0.8, 1}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",9, xAxis36);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(600);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("ANN_{multi,VH}");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->cd();
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->Modified();
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->cd();
   CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb->SetSelected(CutMultiANN_SigANN_SR_multiANNPFlow_VH_VHbb);
}
