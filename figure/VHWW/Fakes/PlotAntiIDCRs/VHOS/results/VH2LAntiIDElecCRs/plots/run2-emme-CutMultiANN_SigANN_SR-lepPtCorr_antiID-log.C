void run2-emme-CutMultiANN_SigANN_SR-lepPtCorr_antiID-log()
{
//=========Macro generated from canvas: CutMultiANN_SigANN_SR_lepPtCorr_antiID/CutMultiANN_SigANN_SR_lepPtCorr_antiID
//=========  (Fri Mar  3 21:43:45 2023) by ROOT version 6.18/04
   TCanvas *CutMultiANN_SigANN_SR_lepPtCorr_antiID = new TCanvas("CutMultiANN_SigANN_SR_lepPtCorr_antiID", "CutMultiANN_SigANN_SR_lepPtCorr_antiID",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetHighLightColor(2);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->Range(0,0,1,1);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetFillColor(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetBorderMode(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetBorderSize(2);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetLeftMargin(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetRightMargin(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetTopMargin(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetBottomMargin(0);
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-1.81851,159.4937,3.297177);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",30,0,150);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(1100);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   
   TH1F *stack_stack_2 = new TH1F("stack_stack_2","Background Stack",30,0,150);
   stack_stack_2->SetMinimum(0.01427859);
   stack_stack_2->SetMaximum(57.11436);
   stack_stack_2->SetDirectory(0);
   stack_stack_2->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_2->SetLineColor(ci);
   stack_stack_2->GetXaxis()->SetLabelFont(42);
   stack_stack_2->GetXaxis()->SetLabelSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleOffset(1);
   stack_stack_2->GetXaxis()->SetTitleFont(42);
   stack_stack_2->GetYaxis()->SetLabelFont(42);
   stack_stack_2->GetYaxis()->SetLabelSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleFont(42);
   stack_stack_2->GetZaxis()->SetLabelFont(42);
   stack_stack_2->GetZaxis()->SetLabelSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleOffset(1);
   stack_stack_2->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_2);
   
   
   TH1F *hist_Wjets_stack_1 = new TH1F("hist_Wjets_stack_1","$W+$jets",30,0,150);
   hist_Wjets_stack_1->SetStats(0);

   ci = TColor::GetColor("#006666");
   hist_Wjets_stack_1->SetFillColor(ci);
   hist_Wjets_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Wjets_stack_1->GetXaxis()->SetLabelFont(42);
   hist_Wjets_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_Wjets_stack_1->GetXaxis()->SetTitleFont(42);
   hist_Wjets_stack_1->GetYaxis()->SetLabelFont(42);
   hist_Wjets_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_1->GetYaxis()->SetTitleFont(42);
   hist_Wjets_stack_1->GetZaxis()->SetLabelFont(42);
   hist_Wjets_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_Wjets_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wjets_stack_1,"");
   
   TH1F *hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2 = new TH1F("hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2","Other fakes",30,0,150);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinContent(4,0.005198332);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinContent(5,0.006199816);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinContent(30,0.004593093);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinError(4,0.005198332);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinError(5,0.006199816);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetBinError(30,0.004593093);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetEntries(3);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetStats(0);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->SetFillColor(222);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetXaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetYaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetYaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetZaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2,"");
   
   TH1F *hist_tt_stack_3 = new TH1F("hist_tt_stack_3","$t\\bar{t}$",30,0,150);
   hist_tt_stack_3->SetBinContent(4,8.922871);
   hist_tt_stack_3->SetBinContent(5,4.670042);
   hist_tt_stack_3->SetBinContent(6,3.156227);
   hist_tt_stack_3->SetBinContent(7,2.577203);
   hist_tt_stack_3->SetBinContent(8,1.316955);
   hist_tt_stack_3->SetBinContent(9,1.317372);
   hist_tt_stack_3->SetBinContent(10,0.436465);
   hist_tt_stack_3->SetBinContent(11,0.9213725);
   hist_tt_stack_3->SetBinContent(12,0.2908221);
   hist_tt_stack_3->SetBinContent(13,0.3196537);
   hist_tt_stack_3->SetBinContent(14,0.01912215);
   hist_tt_stack_3->SetBinContent(15,0.2558694);
   hist_tt_stack_3->SetBinContent(16,0.1426056);
   hist_tt_stack_3->SetBinContent(17,0.2779301);
   hist_tt_stack_3->SetBinContent(19,0.1429842);
   hist_tt_stack_3->SetBinContent(25,0.1033011);
   hist_tt_stack_3->SetBinContent(26,0.09739631);
   hist_tt_stack_3->SetBinContent(30,0.285164);
   hist_tt_stack_3->SetBinError(4,1.032093);
   hist_tt_stack_3->SetBinError(5,0.7587844);
   hist_tt_stack_3->SetBinError(6,0.6505939);
   hist_tt_stack_3->SetBinError(7,0.5854931);
   hist_tt_stack_3->SetBinError(8,0.4143146);
   hist_tt_stack_3->SetBinError(9,0.4204032);
   hist_tt_stack_3->SetBinError(10,0.2519943);
   hist_tt_stack_3->SetBinError(11,0.3500488);
   hist_tt_stack_3->SetBinError(12,0.2056422);
   hist_tt_stack_3->SetBinError(13,0.1936163);
   hist_tt_stack_3->SetBinError(14,0.01912215);
   hist_tt_stack_3->SetBinError(15,0.1818425);
   hist_tt_stack_3->SetBinError(16,0.1426056);
   hist_tt_stack_3->SetBinError(17,0.1965494);
   hist_tt_stack_3->SetBinError(19,0.1429842);
   hist_tt_stack_3->SetBinError(25,0.1033011);
   hist_tt_stack_3->SetBinError(26,0.09739631);
   hist_tt_stack_3->SetBinError(30,0.2018651);
   hist_tt_stack_3->SetEntries(215);
   hist_tt_stack_3->SetStats(0);
   hist_tt_stack_3->SetFillColor(219);
   hist_tt_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_tt_stack_3->GetXaxis()->SetLabelFont(42);
   hist_tt_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_tt_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_tt_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_tt_stack_3->GetXaxis()->SetTitleFont(42);
   hist_tt_stack_3->GetYaxis()->SetLabelFont(42);
   hist_tt_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_tt_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_tt_stack_3->GetYaxis()->SetTitleFont(42);
   hist_tt_stack_3->GetZaxis()->SetLabelFont(42);
   hist_tt_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_tt_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_tt_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_tt_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tt_stack_3,"");
   
   TH1F *hist_Wgamma_stack_4 = new TH1F("hist_Wgamma_stack_4","$W+\\gamma$",30,0,150);
   hist_Wgamma_stack_4->SetBinContent(4,26.76841);
   hist_Wgamma_stack_4->SetBinContent(5,14.91226);
   hist_Wgamma_stack_4->SetBinContent(6,8.583316);
   hist_Wgamma_stack_4->SetBinContent(7,6.4337);
   hist_Wgamma_stack_4->SetBinContent(8,6.603512);
   hist_Wgamma_stack_4->SetBinContent(9,1.14465);
   hist_Wgamma_stack_4->SetBinContent(10,7.883889);
   hist_Wgamma_stack_4->SetBinContent(11,1.073745);
   hist_Wgamma_stack_4->SetBinContent(12,0.8767315);
   hist_Wgamma_stack_4->SetBinContent(13,1.456689);
   hist_Wgamma_stack_4->SetBinContent(14,0.3451036);
   hist_Wgamma_stack_4->SetBinContent(15,0.7306926);
   hist_Wgamma_stack_4->SetBinContent(16,1.726585);
   hist_Wgamma_stack_4->SetBinContent(17,0.00211909);
   hist_Wgamma_stack_4->SetBinContent(23,0.06678569);
   hist_Wgamma_stack_4->SetBinContent(25,-3.483247);
   hist_Wgamma_stack_4->SetBinContent(28,0.00693043);
   hist_Wgamma_stack_4->SetBinContent(30,0.3015935);
   hist_Wgamma_stack_4->SetBinError(4,8.372677);
   hist_Wgamma_stack_4->SetBinError(5,6.86569);
   hist_Wgamma_stack_4->SetBinError(6,5.252934);
   hist_Wgamma_stack_4->SetBinError(7,3.953053);
   hist_Wgamma_stack_4->SetBinError(8,4.040467);
   hist_Wgamma_stack_4->SetBinError(9,0.8477328);
   hist_Wgamma_stack_4->SetBinError(10,4.956344);
   hist_Wgamma_stack_4->SetBinError(11,1.043396);
   hist_Wgamma_stack_4->SetBinError(12,0.8767314);
   hist_Wgamma_stack_4->SetBinError(13,2.235695);
   hist_Wgamma_stack_4->SetBinError(14,0.2754466);
   hist_Wgamma_stack_4->SetBinError(15,0.7306926);
   hist_Wgamma_stack_4->SetBinError(16,1.220882);
   hist_Wgamma_stack_4->SetBinError(17,0.00211909);
   hist_Wgamma_stack_4->SetBinError(23,0.06678569);
   hist_Wgamma_stack_4->SetBinError(25,3.380224);
   hist_Wgamma_stack_4->SetBinError(28,0.005514636);
   hist_Wgamma_stack_4->SetBinError(30,0.2245927);
   hist_Wgamma_stack_4->SetEntries(207);
   hist_Wgamma_stack_4->SetStats(0);
   hist_Wgamma_stack_4->SetFillColor(223);
   hist_Wgamma_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_Wgamma_stack_4->GetXaxis()->SetLabelFont(42);
   hist_Wgamma_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_4->GetXaxis()->SetTitleFont(42);
   hist_Wgamma_stack_4->GetYaxis()->SetLabelFont(42);
   hist_Wgamma_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_4->GetYaxis()->SetTitleFont(42);
   hist_Wgamma_stack_4->GetZaxis()->SetLabelFont(42);
   hist_Wgamma_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wgamma_stack_4,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3002[33] = {
   0,
   2.5,
   7.5,
   12.5,
   17.5,
   22.5,
   27.5,
   32.5,
   37.5,
   42.5,
   47.5,
   52.5,
   57.5,
   62.5,
   67.5,
   72.5,
   77.5,
   82.5,
   87.5,
   92.5,
   97.5,
   102.5,
   107.5,
   112.5,
   117.5,
   122.5,
   127.5,
   132.5,
   137.5,
   142.5,
   147.5,
   152.5,
   157.5};
   Double_t totalStackErr_fy3002[33] = {
   0,
   0,
   0,
   0,
   35.69648,
   19.5885,
   11.73954,
   9.010902,
   7.920466,
   2.462022,
   8.320354,
   1.995117,
   1.167554,
   1.776343,
   0.3642257,
   0.986562,
   1.86919,
   0.2800492,
   0,
   0.1429842,
   0,
   0,
   0,
   0.06678569,
   0,
   -3.379946,
   0.09739631,
   0,
   0.00693043,
   0,
   0.5913506,
   0,
   0};
   Double_t totalStackErr_felx3002[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fely3002[33] = {
   0,
   0,
   0,
   0,
   8.436051,
   6.907495,
   5.29307,
   3.996177,
   4.061653,
   0.9462504,
   4.962745,
   1.10055,
   0.9005258,
   2.244064,
   0.2761095,
   0.7529796,
   1.229182,
   0.1965608,
   0,
   0.1429842,
   0,
   0,
   0,
   0.06678569,
   0,
   3.381802,
   0.09739631,
   0,
   0.005514636,
   0,
   0.302014,
   0,
   0};
   Double_t totalStackErr_fehx3002[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fehy3002[33] = {
   0,
   0,
   0,
   0,
   8.436051,
   6.907495,
   5.29307,
   3.996177,
   4.061653,
   0.9462504,
   4.962745,
   1.10055,
   0.9005258,
   2.244064,
   0.2761095,
   0.7529796,
   1.229182,
   0.1965608,
   0,
   0.1429842,
   0,
   0,
   0,
   0.06678569,
   0,
   3.381802,
   0.09739631,
   0,
   0.005514636,
   0,
   0.302014,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(33,totalStackErr_fx3002,totalStackErr_fy3002,totalStackErr_felx3002,totalStackErr_fehx3002,totalStackErr_fely3002,totalStackErr_fehy3002);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3002 = new TH1F("Graph_totalStackErr3002","SM",100,0,176);
   Graph_totalStackErr3002->SetMinimum(0.04888377);
   Graph_totalStackErr3002->SetMaximum(48.88377);
   Graph_totalStackErr3002->SetDirectory(0);
   Graph_totalStackErr3002->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3002->SetLineColor(ci);
   Graph_totalStackErr3002->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3002);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.818,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wgamma_stack_4"," #it{W+#gamma}","f");
   entry->SetFillColor(223);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wjets_stack_1"," #it{W+}jets","f");

   ci = TColor::GetColor("#006666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tt_stack_3"," #it{t#bar{t}}","f");
   entry->SetFillColor(219);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_bkg___channel____campaign__ddFakes_eFakes_mc_diboson_stack_2"," Other fakes","f");
   entry->SetFillColor(222);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutMultiANN_SigANN_SR/lepPtCorr_antiID\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",30,0,150);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(1100);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->cd();
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->Modified();
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->cd();
   CutMultiANN_SigANN_SR_lepPtCorr_antiID->SetSelected(CutMultiANN_SigANN_SR_lepPtCorr_antiID);
}
