void run2-mm-CutWHG0BVetoMM-HWWWZVetoRNN_rebinned-lin()
{
//=========Macro generated from canvas: CutWHG0BVetoMM_HWWWZVetoRNN_rebinned/CutWHG0BVetoMM_HWWWZVetoRNN_rebinned
//=========  (Fri Mar  3 15:04:39 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoMM_HWWWZVetoRNN_rebinned = new TCanvas("CutWHG0BVetoMM_HWWWZVetoRNN_rebinned", "CutWHG0BVetoMM_HWWWZVetoRNN_rebinned",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetHighLightColor(2);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->Range(0,0,1,1);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetFillColor(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetBorderMode(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetBorderSize(2);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetLeftMargin(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetRightMargin(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetTopMargin(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetBottomMargin(0);
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-0.2025316,-6075.949,1.063291,31898.73);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis61[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",9, xAxis61);
   Graph_master->SetMinimum(0);
   Graph_master->SetMaximum(30000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis62[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *stack_stack_11 = new TH1F("stack_stack_11","Background Stack",9, xAxis62);
   stack_stack_11->SetMinimum(0);
   stack_stack_11->SetMaximum(450.0179);
   stack_stack_11->SetDirectory(0);
   stack_stack_11->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_11->SetLineColor(ci);
   stack_stack_11->GetXaxis()->SetLabelFont(42);
   stack_stack_11->GetXaxis()->SetLabelSize(0.035);
   stack_stack_11->GetXaxis()->SetTitleSize(0.035);
   stack_stack_11->GetXaxis()->SetTitleOffset(1);
   stack_stack_11->GetXaxis()->SetTitleFont(42);
   stack_stack_11->GetYaxis()->SetLabelFont(42);
   stack_stack_11->GetYaxis()->SetLabelSize(0.035);
   stack_stack_11->GetYaxis()->SetTitleSize(0.035);
   stack_stack_11->GetYaxis()->SetTitleFont(42);
   stack_stack_11->GetZaxis()->SetLabelFont(42);
   stack_stack_11->GetZaxis()->SetLabelSize(0.035);
   stack_stack_11->GetZaxis()->SetTitleSize(0.035);
   stack_stack_11->GetZaxis()->SetTitleOffset(1);
   stack_stack_11->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_11);
   
   Double_t xAxis63[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_WZ_stack_1 = new TH1F("hist_WZ_stack_1","$WZ$",9, xAxis63);
   hist_WZ_stack_1->SetBinContent(1,354.1734);
   hist_WZ_stack_1->SetBinContent(2,160.9923);
   hist_WZ_stack_1->SetBinContent(3,114.9574);
   hist_WZ_stack_1->SetBinContent(4,84.68375);
   hist_WZ_stack_1->SetBinContent(5,61.79699);
   hist_WZ_stack_1->SetBinContent(6,43.35463);
   hist_WZ_stack_1->SetBinContent(7,29.9708);
   hist_WZ_stack_1->SetBinContent(8,22.08454);
   hist_WZ_stack_1->SetBinContent(9,12.57917);
   hist_WZ_stack_1->SetBinError(1,5.935329);
   hist_WZ_stack_1->SetBinError(2,3.758581);
   hist_WZ_stack_1->SetBinError(3,2.737126);
   hist_WZ_stack_1->SetBinError(4,2.680113);
   hist_WZ_stack_1->SetBinError(5,1.818863);
   hist_WZ_stack_1->SetBinError(6,1.533558);
   hist_WZ_stack_1->SetBinError(7,1.347475);
   hist_WZ_stack_1->SetBinError(8,1.253747);
   hist_WZ_stack_1->SetBinError(9,1.384067);
   hist_WZ_stack_1->SetEntries(110263);
   hist_WZ_stack_1->SetStats(0);
   hist_WZ_stack_1->SetFillColor(222);
   hist_WZ_stack_1->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_WZ_stack_1->GetXaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_WZ_stack_1->GetXaxis()->SetTitleFont(42);
   hist_WZ_stack_1->GetYaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetYaxis()->SetTitleFont(42);
   hist_WZ_stack_1->GetZaxis()->SetLabelFont(42);
   hist_WZ_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_WZ_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_WZ_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_WZ_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WZ_stack_1,"");
   Double_t xAxis64[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_WW_stack_2 = new TH1F("hist_WW_stack_2","Same-sign $WW$",9, xAxis64);
   hist_WW_stack_2->SetBinContent(1,25.7308);
   hist_WW_stack_2->SetBinContent(2,9.276048);
   hist_WW_stack_2->SetBinContent(3,6.762466);
   hist_WW_stack_2->SetBinContent(4,4.887392);
   hist_WW_stack_2->SetBinContent(5,3.991191);
   hist_WW_stack_2->SetBinContent(6,2.895076);
   hist_WW_stack_2->SetBinContent(7,2.407271);
   hist_WW_stack_2->SetBinContent(8,1.921506);
   hist_WW_stack_2->SetBinContent(9,1.274319);
   hist_WW_stack_2->SetBinError(1,0.1626701);
   hist_WW_stack_2->SetBinError(2,0.09766036);
   hist_WW_stack_2->SetBinError(3,0.08338926);
   hist_WW_stack_2->SetBinError(4,0.07101861);
   hist_WW_stack_2->SetBinError(5,0.06516989);
   hist_WW_stack_2->SetBinError(6,0.05602345);
   hist_WW_stack_2->SetBinError(7,0.05117904);
   hist_WW_stack_2->SetBinError(8,0.0460943);
   hist_WW_stack_2->SetBinError(9,0.03860827);
   hist_WW_stack_2->SetEntries(93986);
   hist_WW_stack_2->SetStats(0);

   ci = TColor::GetColor("#6666cc");
   hist_WW_stack_2->SetFillColor(ci);
   hist_WW_stack_2->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_WW_stack_2->GetXaxis()->SetLabelFont(42);
   hist_WW_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_WW_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_WW_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_WW_stack_2->GetXaxis()->SetTitleFont(42);
   hist_WW_stack_2->GetYaxis()->SetLabelFont(42);
   hist_WW_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_WW_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_WW_stack_2->GetYaxis()->SetTitleFont(42);
   hist_WW_stack_2->GetZaxis()->SetLabelFont(42);
   hist_WW_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_WW_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_WW_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_WW_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WW_stack_2,"");
   Double_t xAxis65[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_ZZ_stack_3 = new TH1F("hist_ZZ_stack_3","$ZZ$",9, xAxis65);
   hist_ZZ_stack_3->SetBinContent(1,38.28891);
   hist_ZZ_stack_3->SetBinContent(2,24.23129);
   hist_ZZ_stack_3->SetBinContent(3,17.70253);
   hist_ZZ_stack_3->SetBinContent(4,9.001578);
   hist_ZZ_stack_3->SetBinContent(5,10.29379);
   hist_ZZ_stack_3->SetBinContent(6,9.213312);
   hist_ZZ_stack_3->SetBinContent(7,2.440086);
   hist_ZZ_stack_3->SetBinContent(8,0.9576761);
   hist_ZZ_stack_3->SetBinContent(9,0.4779797);
   hist_ZZ_stack_3->SetBinError(1,2.983048);
   hist_ZZ_stack_3->SetBinError(2,2.248727);
   hist_ZZ_stack_3->SetBinError(3,1.739209);
   hist_ZZ_stack_3->SetBinError(4,1.000962);
   hist_ZZ_stack_3->SetBinError(5,1.632319);
   hist_ZZ_stack_3->SetBinError(6,1.548804);
   hist_ZZ_stack_3->SetBinError(7,0.5127431);
   hist_ZZ_stack_3->SetBinError(8,0.2966749);
   hist_ZZ_stack_3->SetBinError(9,0.2086792);
   hist_ZZ_stack_3->SetEntries(38272);
   hist_ZZ_stack_3->SetStats(0);
   hist_ZZ_stack_3->SetFillColor(15);
   hist_ZZ_stack_3->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_ZZ_stack_3->GetXaxis()->SetLabelFont(42);
   hist_ZZ_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_ZZ_stack_3->GetXaxis()->SetTitleFont(42);
   hist_ZZ_stack_3->GetYaxis()->SetLabelFont(42);
   hist_ZZ_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_3->GetYaxis()->SetTitleFont(42);
   hist_ZZ_stack_3->GetZaxis()->SetLabelFont(42);
   hist_ZZ_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_ZZ_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ZZ_stack_3,"");
   Double_t xAxis66[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_VVV_stack_4 = new TH1F("hist_VVV_stack_4","$VVV$",9, xAxis66);
   hist_VVV_stack_4->SetBinContent(1,5.433506);
   hist_VVV_stack_4->SetBinContent(2,2.43758);
   hist_VVV_stack_4->SetBinContent(3,1.90316);
   hist_VVV_stack_4->SetBinContent(4,1.593011);
   hist_VVV_stack_4->SetBinContent(5,1.348294);
   hist_VVV_stack_4->SetBinContent(6,0.9936841);
   hist_VVV_stack_4->SetBinContent(7,0.8641436);
   hist_VVV_stack_4->SetBinContent(8,0.5696805);
   hist_VVV_stack_4->SetBinContent(9,0.3721087);
   hist_VVV_stack_4->SetBinError(1,0.1494548);
   hist_VVV_stack_4->SetBinError(2,0.09566783);
   hist_VVV_stack_4->SetBinError(3,0.08577358);
   hist_VVV_stack_4->SetBinError(4,0.07627377);
   hist_VVV_stack_4->SetBinError(5,0.06943404);
   hist_VVV_stack_4->SetBinError(6,0.06229866);
   hist_VVV_stack_4->SetBinError(7,0.05567523);
   hist_VVV_stack_4->SetBinError(8,0.04448007);
   hist_VVV_stack_4->SetBinError(9,0.03791474);
   hist_VVV_stack_4->SetEntries(13664);
   hist_VVV_stack_4->SetStats(0);

   ci = TColor::GetColor("#ff00ff");
   hist_VVV_stack_4->SetFillColor(ci);
   hist_VVV_stack_4->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_VVV_stack_4->GetXaxis()->SetLabelFont(42);
   hist_VVV_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_VVV_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_VVV_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_VVV_stack_4->GetXaxis()->SetTitleFont(42);
   hist_VVV_stack_4->GetYaxis()->SetLabelFont(42);
   hist_VVV_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_VVV_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_VVV_stack_4->GetYaxis()->SetTitleFont(42);
   hist_VVV_stack_4->GetZaxis()->SetLabelFont(42);
   hist_VVV_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_VVV_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_VVV_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_VVV_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_VVV_stack_4,"");
   Double_t xAxis67[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_ttV_stack_5 = new TH1F("hist_ttV_stack_5","$ttV$",9, xAxis67);
   hist_ttV_stack_5->SetBinContent(1,4.357318);
   hist_ttV_stack_5->SetBinContent(2,2.447646);
   hist_ttV_stack_5->SetBinContent(3,2.141376);
   hist_ttV_stack_5->SetBinContent(4,1.500538);
   hist_ttV_stack_5->SetBinContent(5,1.296102);
   hist_ttV_stack_5->SetBinContent(6,0.8038545);
   hist_ttV_stack_5->SetBinContent(7,0.691623);
   hist_ttV_stack_5->SetBinContent(8,0.5784821);
   hist_ttV_stack_5->SetBinContent(9,0.2154092);
   hist_ttV_stack_5->SetBinError(1,0.1812079);
   hist_ttV_stack_5->SetBinError(2,0.1429814);
   hist_ttV_stack_5->SetBinError(3,0.1311406);
   hist_ttV_stack_5->SetBinError(4,0.1158136);
   hist_ttV_stack_5->SetBinError(5,0.1018912);
   hist_ttV_stack_5->SetBinError(6,0.07834753);
   hist_ttV_stack_5->SetBinError(7,0.0668805);
   hist_ttV_stack_5->SetBinError(8,0.06290799);
   hist_ttV_stack_5->SetBinError(9,0.03694163);
   hist_ttV_stack_5->SetEntries(6048);
   hist_ttV_stack_5->SetStats(0);
   hist_ttV_stack_5->SetFillColor(220);
   hist_ttV_stack_5->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_ttV_stack_5->GetXaxis()->SetLabelFont(42);
   hist_ttV_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_ttV_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_ttV_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_ttV_stack_5->GetXaxis()->SetTitleFont(42);
   hist_ttV_stack_5->GetYaxis()->SetLabelFont(42);
   hist_ttV_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_ttV_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_ttV_stack_5->GetYaxis()->SetTitleFont(42);
   hist_ttV_stack_5->GetZaxis()->SetLabelFont(42);
   hist_ttV_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_ttV_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_ttV_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_ttV_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ttV_stack_5,"");
   Double_t xAxis68[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_tZ_stack_6 = new TH1F("hist_tZ_stack_6","$tZ$",9, xAxis68);
   hist_tZ_stack_6->SetBinContent(1,0.6045219);
   hist_tZ_stack_6->SetBinContent(2,0.206209);
   hist_tZ_stack_6->SetBinContent(3,0.1522827);
   hist_tZ_stack_6->SetBinContent(4,0.08922456);
   hist_tZ_stack_6->SetBinContent(5,0.0737793);
   hist_tZ_stack_6->SetBinContent(6,0.04363042);
   hist_tZ_stack_6->SetBinContent(7,0.02113678);
   hist_tZ_stack_6->SetBinContent(8,0.0258478);
   hist_tZ_stack_6->SetBinContent(9,0.007478651);
   hist_tZ_stack_6->SetBinError(1,0.02304639);
   hist_tZ_stack_6->SetBinError(2,0.01320134);
   hist_tZ_stack_6->SetBinError(3,0.01148327);
   hist_tZ_stack_6->SetBinError(4,0.008840562);
   hist_tZ_stack_6->SetBinError(5,0.008146905);
   hist_tZ_stack_6->SetBinError(6,0.006138302);
   hist_tZ_stack_6->SetBinError(7,0.004126428);
   hist_tZ_stack_6->SetBinError(8,0.004747133);
   hist_tZ_stack_6->SetBinError(9,0.002554104);
   hist_tZ_stack_6->SetEntries(1529);
   hist_tZ_stack_6->SetStats(0);
   hist_tZ_stack_6->SetFillColor(218);
   hist_tZ_stack_6->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_tZ_stack_6->GetXaxis()->SetLabelFont(42);
   hist_tZ_stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_tZ_stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_tZ_stack_6->GetXaxis()->SetTitleOffset(1);
   hist_tZ_stack_6->GetXaxis()->SetTitleFont(42);
   hist_tZ_stack_6->GetYaxis()->SetLabelFont(42);
   hist_tZ_stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_tZ_stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_tZ_stack_6->GetYaxis()->SetTitleFont(42);
   hist_tZ_stack_6->GetZaxis()->SetLabelFont(42);
   hist_tZ_stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_tZ_stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_tZ_stack_6->GetZaxis()->SetTitleOffset(1);
   hist_tZ_stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tZ_stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3011[12] = {
   0,
   0.105,
   0.275,
   0.405,
   0.525,
   0.63,
   0.72,
   0.8,
   0.885,
   0.965,
   1.055556,
   1.166667};
   Double_t totalStackErr_fy3011[12] = {
   0,
   428.5885,
   199.5911,
   143.6192,
   101.7555,
   78.80016,
   57.30418,
   36.39506,
   26.13773,
   14.92646,
   0,
   0};
   Double_t totalStackErr_felx3011[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fely3011[12] = {
   0,
   6.648975,
   4.384406,
   3.247822,
   2.865185,
   2.447906,
   2.182611,
   1.44527,
   1.291504,
   1.401245,
   0,
   0};
   Double_t totalStackErr_fehx3011[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fehy3011[12] = {
   0,
   6.648975,
   4.384406,
   3.247822,
   2.865185,
   2.447906,
   2.182611,
   1.44527,
   1.291504,
   1.401245,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(12,totalStackErr_fx3011,totalStackErr_fy3011,totalStackErr_felx3011,totalStackErr_fehx3011,totalStackErr_fely3011,totalStackErr_fehy3011);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3011 = new TH1F("Graph_totalStackErr3011","SM",100,0,1.321833);
   Graph_totalStackErr3011->SetMinimum(0);
   Graph_totalStackErr3011->SetMaximum(478.7612);
   Graph_totalStackErr3011->SetDirectory(0);
   Graph_totalStackErr3011->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3011->SetLineColor(ci);
   Graph_totalStackErr3011->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3011->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3011->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3011->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3011->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3011->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3011->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3011->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3011->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3011->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3011->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3011->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3011->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3011->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3011);
   
   grae->Draw("2");
   Double_t xAxis69[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_data__11 = new TH1F("hist_data__11","Data",9, xAxis69);
   hist_data__11->SetBinContent(1,13193);
   hist_data__11->SetBinContent(2,5967);
   hist_data__11->SetBinContent(3,4398);
   hist_data__11->SetBinContent(4,3201);
   hist_data__11->SetBinContent(5,2495);
   hist_data__11->SetBinContent(6,1446);
   hist_data__11->SetBinContent(7,897);
   hist_data__11->SetBinContent(8,667);
   hist_data__11->SetBinContent(9,276);
   hist_data__11->SetBinError(1,114.8608);
   hist_data__11->SetBinError(2,77.24636);
   hist_data__11->SetBinError(3,66.31742);
   hist_data__11->SetBinError(4,56.57738);
   hist_data__11->SetBinError(5,49.94997);
   hist_data__11->SetBinError(6,38.02631);
   hist_data__11->SetBinError(7,29.94996);
   hist_data__11->SetBinError(8,25.82634);
   hist_data__11->SetBinError(9,16.61325);
   hist_data__11->SetEntries(32540);
   hist_data__11->SetStats(0);
   hist_data__11->SetFillColor(15);
   hist_data__11->SetLineWidth(2);
   hist_data__11->SetMarkerStyle(20);
   hist_data__11->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_data__11->GetXaxis()->SetLabelFont(42);
   hist_data__11->GetXaxis()->SetLabelSize(0.035);
   hist_data__11->GetXaxis()->SetTitleSize(0.035);
   hist_data__11->GetXaxis()->SetTitleOffset(1);
   hist_data__11->GetXaxis()->SetTitleFont(42);
   hist_data__11->GetYaxis()->SetLabelFont(42);
   hist_data__11->GetYaxis()->SetLabelSize(0.035);
   hist_data__11->GetYaxis()->SetTitleSize(0.035);
   hist_data__11->GetYaxis()->SetTitleFont(42);
   hist_data__11->GetZaxis()->SetLabelFont(42);
   hist_data__11->GetZaxis()->SetLabelSize(0.035);
   hist_data__11->GetZaxis()->SetTitleSize(0.035);
   hist_data__11->GetZaxis()->SetTitleOffset(1);
   hist_data__11->GetZaxis()->SetTitleFont(42);
   hist_data__11->Draw("ep same");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("hist_data"," Data","lep");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WZ_stack_1"," #it{WZ}","f");
   entry->SetFillColor(222);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WW_stack_2"," Same-sign #it{WW}","f");

   ci = TColor::GetColor("#6666cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ZZ_stack_3"," #it{ZZ}","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_VVV_stack_4"," #it{VVV}","f");

   ci = TColor::GetColor("#ff00ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ttV_stack_5"," #it{ttV}","f");
   entry->SetFillColor(220);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tZ_stack_6"," #it{tZ}","f");
   entry->SetFillColor(218);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoMM/HWWWZVetoRNN_rebinned\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"#mu#mu channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis70[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",9, xAxis70);
   Graph_master_copy->SetMinimum(0);
   Graph_master_copy->SetMaximum(30000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->Modified();
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoMM_HWWWZVetoRNN_rebinned->SetSelected(CutWHG0BVetoMM_HWWWZVetoRNN_rebinned);
}
