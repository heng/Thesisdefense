void run2-emme-CutWHG0BVetoEMME-HWWWZVetoRNN_rebinned-log()
{
//=========Macro generated from canvas: CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned/CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned
//=========  (Fri Mar  3 15:04:37 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned = new TCanvas("CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned", "CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetHighLightColor(2);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->Range(0,0,1,1);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetFillColor(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetBorderMode(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetBorderSize(2);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetLeftMargin(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetRightMargin(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetTopMargin(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetBottomMargin(0);
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-0.2025316,-2.588881,1.063291,7.341623);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis11[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",9, xAxis11);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(7000000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis12[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *stack_stack_2 = new TH1F("stack_stack_2","Background Stack",9, xAxis12);
   stack_stack_2->SetMinimum(0.001354966);
   stack_stack_2->SetMaximum(794.904);
   stack_stack_2->SetDirectory(0);
   stack_stack_2->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_2->SetLineColor(ci);
   stack_stack_2->GetXaxis()->SetLabelFont(42);
   stack_stack_2->GetXaxis()->SetLabelSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleOffset(1);
   stack_stack_2->GetXaxis()->SetTitleFont(42);
   stack_stack_2->GetYaxis()->SetLabelFont(42);
   stack_stack_2->GetYaxis()->SetLabelSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleFont(42);
   stack_stack_2->GetZaxis()->SetLabelFont(42);
   stack_stack_2->GetZaxis()->SetLabelSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleOffset(1);
   stack_stack_2->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_2);
   
   Double_t xAxis13[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_tZ_stack_1 = new TH1F("hist_tZ_stack_1","$tZ$",9, xAxis13);
   hist_tZ_stack_1->SetBinContent(1,0.6351664);
   hist_tZ_stack_1->SetBinContent(2,0.2347953);
   hist_tZ_stack_1->SetBinContent(3,0.1710371);
   hist_tZ_stack_1->SetBinContent(4,0.1008511);
   hist_tZ_stack_1->SetBinContent(5,0.07247482);
   hist_tZ_stack_1->SetBinContent(6,0.03628717);
   hist_tZ_stack_1->SetBinContent(7,0.02920048);
   hist_tZ_stack_1->SetBinContent(8,0.01728511);
   hist_tZ_stack_1->SetBinContent(9,0.007928025);
   hist_tZ_stack_1->SetBinError(1,0.02350163);
   hist_tZ_stack_1->SetBinError(2,0.01437981);
   hist_tZ_stack_1->SetBinError(3,0.0123189);
   hist_tZ_stack_1->SetBinError(4,0.009477532);
   hist_tZ_stack_1->SetBinError(5,0.008175673);
   hist_tZ_stack_1->SetBinError(6,0.005699557);
   hist_tZ_stack_1->SetBinError(7,0.005032657);
   hist_tZ_stack_1->SetBinError(8,0.003894044);
   hist_tZ_stack_1->SetBinError(9,0.002710926);
   hist_tZ_stack_1->SetEntries(1618);
   hist_tZ_stack_1->SetStats(0);
   hist_tZ_stack_1->SetFillColor(218);
   hist_tZ_stack_1->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_tZ_stack_1->GetXaxis()->SetLabelFont(42);
   hist_tZ_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_tZ_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_tZ_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_tZ_stack_1->GetXaxis()->SetTitleFont(42);
   hist_tZ_stack_1->GetYaxis()->SetLabelFont(42);
   hist_tZ_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_tZ_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_tZ_stack_1->GetYaxis()->SetTitleFont(42);
   hist_tZ_stack_1->GetZaxis()->SetLabelFont(42);
   hist_tZ_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_tZ_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_tZ_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_tZ_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tZ_stack_1,"");
   Double_t xAxis14[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_VVV_stack_2 = new TH1F("hist_VVV_stack_2","$VVV$",9, xAxis14);
   hist_VVV_stack_2->SetBinContent(1,4.027386);
   hist_VVV_stack_2->SetBinContent(2,2.028398);
   hist_VVV_stack_2->SetBinContent(3,1.860947);
   hist_VVV_stack_2->SetBinContent(4,1.383993);
   hist_VVV_stack_2->SetBinContent(5,1.23183);
   hist_VVV_stack_2->SetBinContent(6,0.8018777);
   hist_VVV_stack_2->SetBinContent(7,0.7010037);
   hist_VVV_stack_2->SetBinContent(8,0.7071506);
   hist_VVV_stack_2->SetBinContent(9,0.3651067);
   hist_VVV_stack_2->SetBinError(1,0.1366047);
   hist_VVV_stack_2->SetBinError(2,0.09463202);
   hist_VVV_stack_2->SetBinError(3,0.08816997);
   hist_VVV_stack_2->SetBinError(4,0.07551705);
   hist_VVV_stack_2->SetBinError(5,0.0704289);
   hist_VVV_stack_2->SetBinError(6,0.05792371);
   hist_VVV_stack_2->SetBinError(7,0.0541448);
   hist_VVV_stack_2->SetBinError(8,0.05515057);
   hist_VVV_stack_2->SetBinError(9,0.03885991);
   hist_VVV_stack_2->SetEntries(4426);
   hist_VVV_stack_2->SetStats(0);

   ci = TColor::GetColor("#ff00ff");
   hist_VVV_stack_2->SetFillColor(ci);
   hist_VVV_stack_2->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_VVV_stack_2->GetXaxis()->SetLabelFont(42);
   hist_VVV_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_VVV_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_VVV_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_VVV_stack_2->GetXaxis()->SetTitleFont(42);
   hist_VVV_stack_2->GetYaxis()->SetLabelFont(42);
   hist_VVV_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_VVV_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_VVV_stack_2->GetYaxis()->SetTitleFont(42);
   hist_VVV_stack_2->GetZaxis()->SetLabelFont(42);
   hist_VVV_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_VVV_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_VVV_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_VVV_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_VVV_stack_2,"");
   Double_t xAxis15[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_ttV_stack_3 = new TH1F("hist_ttV_stack_3","$ttV$",9, xAxis15);
   hist_ttV_stack_3->SetBinContent(1,3.91261);
   hist_ttV_stack_3->SetBinContent(2,2.726501);
   hist_ttV_stack_3->SetBinContent(3,2.142529);
   hist_ttV_stack_3->SetBinContent(4,1.739421);
   hist_ttV_stack_3->SetBinContent(5,1.25922);
   hist_ttV_stack_3->SetBinContent(6,0.7297234);
   hist_ttV_stack_3->SetBinContent(7,0.5386312);
   hist_ttV_stack_3->SetBinContent(8,0.6089376);
   hist_ttV_stack_3->SetBinContent(9,0.2827494);
   hist_ttV_stack_3->SetBinError(1,0.1672313);
   hist_ttV_stack_3->SetBinError(2,0.1414447);
   hist_ttV_stack_3->SetBinError(3,0.1242547);
   hist_ttV_stack_3->SetBinError(4,0.1126251);
   hist_ttV_stack_3->SetBinError(5,0.09307455);
   hist_ttV_stack_3->SetBinError(6,0.07861425);
   hist_ttV_stack_3->SetBinError(7,0.07287731);
   hist_ttV_stack_3->SetBinError(8,0.06719759);
   hist_ttV_stack_3->SetBinError(9,0.04642182);
   hist_ttV_stack_3->SetEntries(5712);
   hist_ttV_stack_3->SetStats(0);
   hist_ttV_stack_3->SetFillColor(220);
   hist_ttV_stack_3->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_ttV_stack_3->GetXaxis()->SetLabelFont(42);
   hist_ttV_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_ttV_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_ttV_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_ttV_stack_3->GetXaxis()->SetTitleFont(42);
   hist_ttV_stack_3->GetYaxis()->SetLabelFont(42);
   hist_ttV_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_ttV_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_ttV_stack_3->GetYaxis()->SetTitleFont(42);
   hist_ttV_stack_3->GetZaxis()->SetLabelFont(42);
   hist_ttV_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_ttV_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_ttV_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_ttV_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ttV_stack_3,"");
   Double_t xAxis16[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_WW_stack_4 = new TH1F("hist_WW_stack_4","Same-sign $WW$",9, xAxis16);
   hist_WW_stack_4->SetBinContent(1,20.1479);
   hist_WW_stack_4->SetBinContent(2,7.903387);
   hist_WW_stack_4->SetBinContent(3,5.836727);
   hist_WW_stack_4->SetBinContent(4,4.241733);
   hist_WW_stack_4->SetBinContent(5,3.517105);
   hist_WW_stack_4->SetBinContent(6,2.532418);
   hist_WW_stack_4->SetBinContent(7,2.033018);
   hist_WW_stack_4->SetBinContent(8,1.780493);
   hist_WW_stack_4->SetBinContent(9,1.19089);
   hist_WW_stack_4->SetBinError(1,0.1440422);
   hist_WW_stack_4->SetBinError(2,0.09046343);
   hist_WW_stack_4->SetBinError(3,0.07821999);
   hist_WW_stack_4->SetBinError(4,0.06664973);
   hist_WW_stack_4->SetBinError(5,0.06112108);
   hist_WW_stack_4->SetBinError(6,0.05243513);
   hist_WW_stack_4->SetBinError(7,0.04758477);
   hist_WW_stack_4->SetBinError(8,0.04504005);
   hist_WW_stack_4->SetBinError(9,0.03760948);
   hist_WW_stack_4->SetEntries(77469);
   hist_WW_stack_4->SetStats(0);

   ci = TColor::GetColor("#6666cc");
   hist_WW_stack_4->SetFillColor(ci);
   hist_WW_stack_4->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_WW_stack_4->GetXaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_WW_stack_4->GetXaxis()->SetTitleFont(42);
   hist_WW_stack_4->GetYaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetYaxis()->SetTitleFont(42);
   hist_WW_stack_4->GetZaxis()->SetLabelFont(42);
   hist_WW_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_WW_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_WW_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_WW_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WW_stack_4,"");
   Double_t xAxis17[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_ZZ_stack_5 = new TH1F("hist_ZZ_stack_5","$ZZ$",9, xAxis17);
   hist_ZZ_stack_5->SetBinContent(1,63.89596);
   hist_ZZ_stack_5->SetBinContent(2,33.1311);
   hist_ZZ_stack_5->SetBinContent(3,26.76579);
   hist_ZZ_stack_5->SetBinContent(4,24.31774);
   hist_ZZ_stack_5->SetBinContent(5,19.95226);
   hist_ZZ_stack_5->SetBinContent(6,9.585606);
   hist_ZZ_stack_5->SetBinContent(7,3.231535);
   hist_ZZ_stack_5->SetBinContent(8,1.406576);
   hist_ZZ_stack_5->SetBinContent(9,0.6484429);
   hist_ZZ_stack_5->SetBinError(1,3.964704);
   hist_ZZ_stack_5->SetBinError(2,3.344121);
   hist_ZZ_stack_5->SetBinError(3,2.24136);
   hist_ZZ_stack_5->SetBinError(4,2.147321);
   hist_ZZ_stack_5->SetBinError(5,2.002808);
   hist_ZZ_stack_5->SetBinError(6,1.377432);
   hist_ZZ_stack_5->SetBinError(7,0.6617066);
   hist_ZZ_stack_5->SetBinError(8,0.430337);
   hist_ZZ_stack_5->SetBinError(9,0.3326605);
   hist_ZZ_stack_5->SetEntries(44887);
   hist_ZZ_stack_5->SetStats(0);
   hist_ZZ_stack_5->SetFillColor(15);
   hist_ZZ_stack_5->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_ZZ_stack_5->GetXaxis()->SetLabelFont(42);
   hist_ZZ_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_ZZ_stack_5->GetXaxis()->SetTitleFont(42);
   hist_ZZ_stack_5->GetYaxis()->SetLabelFont(42);
   hist_ZZ_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_5->GetYaxis()->SetTitleFont(42);
   hist_ZZ_stack_5->GetZaxis()->SetLabelFont(42);
   hist_ZZ_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_ZZ_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_ZZ_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_ZZ_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_ZZ_stack_5,"");
   Double_t xAxis18[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_WZ_stack_6 = new TH1F("hist_WZ_stack_6","$WZ$",9, xAxis18);
   hist_WZ_stack_6->SetBinContent(1,307.6091);
   hist_WZ_stack_6->SetBinContent(2,143.2833);
   hist_WZ_stack_6->SetBinContent(3,103.201);
   hist_WZ_stack_6->SetBinContent(4,72.95652);
   hist_WZ_stack_6->SetBinContent(5,55.76215);
   hist_WZ_stack_6->SetBinContent(6,34.87896);
   hist_WZ_stack_6->SetBinContent(7,26.89837);
   hist_WZ_stack_6->SetBinContent(8,20.06439);
   hist_WZ_stack_6->SetBinContent(9,11.14968);
   hist_WZ_stack_6->SetBinError(1,5.960899);
   hist_WZ_stack_6->SetBinError(2,3.211062);
   hist_WZ_stack_6->SetBinError(3,2.588235);
   hist_WZ_stack_6->SetBinError(4,2.291189);
   hist_WZ_stack_6->SetBinError(5,1.79992);
   hist_WZ_stack_6->SetBinError(6,1.450388);
   hist_WZ_stack_6->SetBinError(7,1.251343);
   hist_WZ_stack_6->SetBinError(8,1.338978);
   hist_WZ_stack_6->SetBinError(9,1.190142);
   hist_WZ_stack_6->SetEntries(101257);
   hist_WZ_stack_6->SetStats(0);
   hist_WZ_stack_6->SetFillColor(222);
   hist_WZ_stack_6->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_WZ_stack_6->GetXaxis()->SetLabelFont(42);
   hist_WZ_stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_WZ_stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_WZ_stack_6->GetXaxis()->SetTitleOffset(1);
   hist_WZ_stack_6->GetXaxis()->SetTitleFont(42);
   hist_WZ_stack_6->GetYaxis()->SetLabelFont(42);
   hist_WZ_stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_WZ_stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_WZ_stack_6->GetYaxis()->SetTitleFont(42);
   hist_WZ_stack_6->GetZaxis()->SetLabelFont(42);
   hist_WZ_stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_WZ_stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_WZ_stack_6->GetZaxis()->SetTitleOffset(1);
   hist_WZ_stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_WZ_stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3002[12] = {
   0,
   0.105,
   0.275,
   0.405,
   0.525,
   0.63,
   0.72,
   0.8,
   0.885,
   0.965,
   1.055556,
   1.166667};
   Double_t totalStackErr_fy3002[12] = {
   0,
   400.2281,
   189.3075,
   139.978,
   104.7403,
   81.79504,
   48.56487,
   33.43176,
   24.58484,
   13.64479,
   0,
   0};
   Double_t totalStackErr_felx3002[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fely3002[12] = {
   0,
   7.163736,
   4.640196,
   3.428139,
   3.143797,
   2.695993,
   2.003313,
   1.419242,
   1.409842,
   1.237816,
   0,
   0};
   Double_t totalStackErr_fehx3002[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fehy3002[12] = {
   0,
   7.163736,
   4.640196,
   3.428139,
   3.143797,
   2.695993,
   2.003313,
   1.419242,
   1.409842,
   1.237816,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(12,totalStackErr_fx3002,totalStackErr_fy3002,totalStackErr_felx3002,totalStackErr_fehx3002,totalStackErr_fely3002,totalStackErr_fehy3002);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3002 = new TH1F("Graph_totalStackErr3002","SM",100,0,1.321833);
   Graph_totalStackErr3002->SetMinimum(0.448131);
   Graph_totalStackErr3002->SetMaximum(448.131);
   Graph_totalStackErr3002->SetDirectory(0);
   Graph_totalStackErr3002->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3002->SetLineColor(ci);
   Graph_totalStackErr3002->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3002);
   
   grae->Draw("2");
   Double_t xAxis19[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_data__2 = new TH1F("hist_data__2","Data",9, xAxis19);
   hist_data__2->SetBinContent(1,14684);
   hist_data__2->SetBinContent(2,6814);
   hist_data__2->SetBinContent(3,5167);
   hist_data__2->SetBinContent(4,3853);
   hist_data__2->SetBinContent(5,2908);
   hist_data__2->SetBinContent(6,1611);
   hist_data__2->SetBinContent(7,1116);
   hist_data__2->SetBinContent(8,793);
   hist_data__2->SetBinContent(9,327);
   hist_data__2->SetBinError(1,121.1776);
   hist_data__2->SetBinError(2,82.54696);
   hist_data__2->SetBinError(3,71.88185);
   hist_data__2->SetBinError(4,62.07254);
   hist_data__2->SetBinError(5,53.92588);
   hist_data__2->SetBinError(6,40.13726);
   hist_data__2->SetBinError(7,33.40659);
   hist_data__2->SetBinError(8,28.16026);
   hist_data__2->SetBinError(9,18.08314);
   hist_data__2->SetEntries(37273);
   hist_data__2->SetStats(0);
   hist_data__2->SetFillColor(15);
   hist_data__2->SetLineWidth(2);
   hist_data__2->SetMarkerStyle(20);
   hist_data__2->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_data__2->GetXaxis()->SetLabelFont(42);
   hist_data__2->GetXaxis()->SetLabelSize(0.035);
   hist_data__2->GetXaxis()->SetTitleSize(0.035);
   hist_data__2->GetXaxis()->SetTitleOffset(1);
   hist_data__2->GetXaxis()->SetTitleFont(42);
   hist_data__2->GetYaxis()->SetLabelFont(42);
   hist_data__2->GetYaxis()->SetLabelSize(0.035);
   hist_data__2->GetYaxis()->SetTitleSize(0.035);
   hist_data__2->GetYaxis()->SetTitleFont(42);
   hist_data__2->GetZaxis()->SetLabelFont(42);
   hist_data__2->GetZaxis()->SetLabelSize(0.035);
   hist_data__2->GetZaxis()->SetTitleSize(0.035);
   hist_data__2->GetZaxis()->SetTitleOffset(1);
   hist_data__2->GetZaxis()->SetTitleFont(42);
   hist_data__2->Draw("ep same");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("hist_data"," Data","lep");
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(2);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(20);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WZ_stack_6"," #it{WZ}","f");
   entry->SetFillColor(222);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_WW_stack_4"," Same-sign #it{WW}","f");

   ci = TColor::GetColor("#6666cc");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ZZ_stack_5"," #it{ZZ}","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_VVV_stack_2"," #it{VVV}","f");

   ci = TColor::GetColor("#ff00ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_ttV_stack_3"," #it{ttV}","f");
   entry->SetFillColor(220);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tZ_stack_1"," #it{tZ}","f");
   entry->SetFillColor(218);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEMME/HWWWZVetoRNN_rebinned\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis20[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",9, xAxis20);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(7000000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->Modified();
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned->SetSelected(CutWHG0BVetoEMME_HWWWZVetoRNN_rebinned);
}
