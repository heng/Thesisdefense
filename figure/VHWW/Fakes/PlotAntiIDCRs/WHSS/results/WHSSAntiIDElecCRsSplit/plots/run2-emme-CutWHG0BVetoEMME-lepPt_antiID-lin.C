void run2-emme-CutWHG0BVetoEMME-lepPt_antiID-lin()
{
//=========Macro generated from canvas: CutWHG0BVetoEMME_lepPt_antiID/CutWHG0BVetoEMME_lepPt_antiID
//=========  (Fri Mar  3 15:30:51 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEMME_lepPt_antiID = new TCanvas("CutWHG0BVetoEMME_lepPt_antiID", "CutWHG0BVetoEMME_lepPt_antiID",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetHighLightColor(2);
   CutWHG0BVetoEMME_lepPt_antiID->Range(0,0,1,1);
   CutWHG0BVetoEMME_lepPt_antiID->SetFillColor(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetBorderMode(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetBorderSize(2);
   CutWHG0BVetoEMME_lepPt_antiID->SetLeftMargin(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetRightMargin(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetTopMargin(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetBottomMargin(0);
   CutWHG0BVetoEMME_lepPt_antiID->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-1622.648,159.4937,8506.455);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",30,0,150);
   Graph_master->SetMinimum(-1.991666);
   Graph_master->SetMaximum(8000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   
   TH1F *stack_stack_17 = new TH1F("stack_stack_17","Background Stack",30,0,150);
   stack_stack_17->SetMinimum(-1.991666);
   stack_stack_17->SetMaximum(5351.068);
   stack_stack_17->SetDirectory(0);
   stack_stack_17->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_17->SetLineColor(ci);
   stack_stack_17->GetXaxis()->SetLabelFont(42);
   stack_stack_17->GetXaxis()->SetLabelSize(0.035);
   stack_stack_17->GetXaxis()->SetTitleSize(0.035);
   stack_stack_17->GetXaxis()->SetTitleOffset(1);
   stack_stack_17->GetXaxis()->SetTitleFont(42);
   stack_stack_17->GetYaxis()->SetLabelFont(42);
   stack_stack_17->GetYaxis()->SetLabelSize(0.035);
   stack_stack_17->GetYaxis()->SetTitleSize(0.035);
   stack_stack_17->GetYaxis()->SetTitleFont(42);
   stack_stack_17->GetZaxis()->SetLabelFont(42);
   stack_stack_17->GetZaxis()->SetLabelSize(0.035);
   stack_stack_17->GetZaxis()->SetTitleSize(0.035);
   stack_stack_17->GetZaxis()->SetTitleOffset(1);
   stack_stack_17->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_17);
   
   
   TH1F *hist_gamma_conversion_stack_1 = new TH1F("hist_gamma_conversion_stack_1","$\\gamma$ conv.",30,0,150);
   hist_gamma_conversion_stack_1->SetBinContent(4,3629.659);
   hist_gamma_conversion_stack_1->SetBinContent(5,2322.105);
   hist_gamma_conversion_stack_1->SetBinContent(6,1592.006);
   hist_gamma_conversion_stack_1->SetBinContent(7,1060.226);
   hist_gamma_conversion_stack_1->SetBinContent(8,837.7411);
   hist_gamma_conversion_stack_1->SetBinContent(9,464.6159);
   hist_gamma_conversion_stack_1->SetBinContent(10,390.4655);
   hist_gamma_conversion_stack_1->SetBinContent(11,365.2754);
   hist_gamma_conversion_stack_1->SetBinContent(12,193.2219);
   hist_gamma_conversion_stack_1->SetBinContent(13,157.0213);
   hist_gamma_conversion_stack_1->SetBinContent(14,176.2643);
   hist_gamma_conversion_stack_1->SetBinContent(15,87.97205);
   hist_gamma_conversion_stack_1->SetBinContent(16,89.63942);
   hist_gamma_conversion_stack_1->SetBinContent(17,69.66294);
   hist_gamma_conversion_stack_1->SetBinContent(18,48.77723);
   hist_gamma_conversion_stack_1->SetBinContent(19,50.2185);
   hist_gamma_conversion_stack_1->SetBinContent(20,51.70369);
   hist_gamma_conversion_stack_1->SetBinContent(21,60.53034);
   hist_gamma_conversion_stack_1->SetBinContent(22,24.12162);
   hist_gamma_conversion_stack_1->SetBinContent(23,17.9389);
   hist_gamma_conversion_stack_1->SetBinContent(24,30.02334);
   hist_gamma_conversion_stack_1->SetBinContent(25,16.05503);
   hist_gamma_conversion_stack_1->SetBinContent(26,25.5504);
   hist_gamma_conversion_stack_1->SetBinContent(27,22.18008);
   hist_gamma_conversion_stack_1->SetBinContent(28,17.18391);
   hist_gamma_conversion_stack_1->SetBinContent(29,17.74244);
   hist_gamma_conversion_stack_1->SetBinContent(30,148.7327);
   hist_gamma_conversion_stack_1->SetBinError(4,138.6539);
   hist_gamma_conversion_stack_1->SetBinError(5,114.7637);
   hist_gamma_conversion_stack_1->SetBinError(6,89.81865);
   hist_gamma_conversion_stack_1->SetBinError(7,75.8285);
   hist_gamma_conversion_stack_1->SetBinError(8,62.7039);
   hist_gamma_conversion_stack_1->SetBinError(9,44.09906);
   hist_gamma_conversion_stack_1->SetBinError(10,43.93631);
   hist_gamma_conversion_stack_1->SetBinError(11,46.15463);
   hist_gamma_conversion_stack_1->SetBinError(12,31.58648);
   hist_gamma_conversion_stack_1->SetBinError(13,27.35626);
   hist_gamma_conversion_stack_1->SetBinError(14,30.24753);
   hist_gamma_conversion_stack_1->SetBinError(15,14.7275);
   hist_gamma_conversion_stack_1->SetBinError(16,19.33535);
   hist_gamma_conversion_stack_1->SetBinError(17,16.13714);
   hist_gamma_conversion_stack_1->SetBinError(18,12.16054);
   hist_gamma_conversion_stack_1->SetBinError(19,14.45171);
   hist_gamma_conversion_stack_1->SetBinError(20,13.53763);
   hist_gamma_conversion_stack_1->SetBinError(21,16.58444);
   hist_gamma_conversion_stack_1->SetBinError(22,5.342966);
   hist_gamma_conversion_stack_1->SetBinError(23,5.486837);
   hist_gamma_conversion_stack_1->SetBinError(24,10.6795);
   hist_gamma_conversion_stack_1->SetBinError(25,3.195646);
   hist_gamma_conversion_stack_1->SetBinError(26,13.5543);
   hist_gamma_conversion_stack_1->SetBinError(27,9.448192);
   hist_gamma_conversion_stack_1->SetBinError(28,3.290757);
   hist_gamma_conversion_stack_1->SetBinError(29,3.346088);
   hist_gamma_conversion_stack_1->SetBinError(30,9.095634);
   hist_gamma_conversion_stack_1->SetEntries(41807);
   hist_gamma_conversion_stack_1->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_1->SetFillColor(ci);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_gamma_conversion_stack_1->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_1->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_1->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_1->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_1,"");
   
   TH1F *hist_light_flavor_stack_2 = new TH1F("hist_light_flavor_stack_2","light flavor",30,0,150);
   hist_light_flavor_stack_2->SetBinContent(4,1197.322);
   hist_light_flavor_stack_2->SetBinContent(5,608.8028);
   hist_light_flavor_stack_2->SetBinContent(6,455.2357);
   hist_light_flavor_stack_2->SetBinContent(7,299.26);
   hist_light_flavor_stack_2->SetBinContent(8,226.1093);
   hist_light_flavor_stack_2->SetBinContent(9,158.7076);
   hist_light_flavor_stack_2->SetBinContent(10,141.0844);
   hist_light_flavor_stack_2->SetBinContent(11,82.80604);
   hist_light_flavor_stack_2->SetBinContent(12,96.14864);
   hist_light_flavor_stack_2->SetBinContent(13,55.02009);
   hist_light_flavor_stack_2->SetBinContent(14,34.825);
   hist_light_flavor_stack_2->SetBinContent(15,44.831);
   hist_light_flavor_stack_2->SetBinContent(16,56.15865);
   hist_light_flavor_stack_2->SetBinContent(17,34.639);
   hist_light_flavor_stack_2->SetBinContent(18,35.59779);
   hist_light_flavor_stack_2->SetBinContent(19,43.43128);
   hist_light_flavor_stack_2->SetBinContent(20,36.43007);
   hist_light_flavor_stack_2->SetBinContent(21,12.43502);
   hist_light_flavor_stack_2->SetBinContent(22,41.55927);
   hist_light_flavor_stack_2->SetBinContent(23,5.67087);
   hist_light_flavor_stack_2->SetBinContent(24,15.66107);
   hist_light_flavor_stack_2->SetBinContent(25,3.976206);
   hist_light_flavor_stack_2->SetBinContent(26,6.212347);
   hist_light_flavor_stack_2->SetBinContent(27,10.85857);
   hist_light_flavor_stack_2->SetBinContent(28,25.45488);
   hist_light_flavor_stack_2->SetBinContent(29,16.27791);
   hist_light_flavor_stack_2->SetBinContent(30,168.8425);
   hist_light_flavor_stack_2->SetBinError(4,98.66682);
   hist_light_flavor_stack_2->SetBinError(5,65.1947);
   hist_light_flavor_stack_2->SetBinError(6,59.31068);
   hist_light_flavor_stack_2->SetBinError(7,47.47955);
   hist_light_flavor_stack_2->SetBinError(8,39.84785);
   hist_light_flavor_stack_2->SetBinError(9,34.45125);
   hist_light_flavor_stack_2->SetBinError(10,33.37979);
   hist_light_flavor_stack_2->SetBinError(11,24.34673);
   hist_light_flavor_stack_2->SetBinError(12,25.8165);
   hist_light_flavor_stack_2->SetBinError(13,17.97425);
   hist_light_flavor_stack_2->SetBinError(14,17.76696);
   hist_light_flavor_stack_2->SetBinError(15,16.08119);
   hist_light_flavor_stack_2->SetBinError(16,21.07264);
   hist_light_flavor_stack_2->SetBinError(17,13.68382);
   hist_light_flavor_stack_2->SetBinError(18,13.43385);
   hist_light_flavor_stack_2->SetBinError(19,18.03462);
   hist_light_flavor_stack_2->SetBinError(20,15.50455);
   hist_light_flavor_stack_2->SetBinError(21,7.241578);
   hist_light_flavor_stack_2->SetBinError(22,18.20242);
   hist_light_flavor_stack_2->SetBinError(23,2.320528);
   hist_light_flavor_stack_2->SetBinError(24,11.08941);
   hist_light_flavor_stack_2->SetBinError(25,1.094034);
   hist_light_flavor_stack_2->SetBinError(26,3.222321);
   hist_light_flavor_stack_2->SetBinError(27,8.374986);
   hist_light_flavor_stack_2->SetBinError(28,14.79421);
   hist_light_flavor_stack_2->SetBinError(29,13.43853);
   hist_light_flavor_stack_2->SetBinError(30,36.56069);
   hist_light_flavor_stack_2->SetEntries(7879);
   hist_light_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_2->SetFillColor(ci);
   hist_light_flavor_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_light_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_2,"");
   
   TH1F *hist_heavy_flavor_stack_3 = new TH1F("hist_heavy_flavor_stack_3","heavy flavor",30,0,150);
   hist_heavy_flavor_stack_3->SetBinContent(4,269.2741);
   hist_heavy_flavor_stack_3->SetBinContent(5,78.03625);
   hist_heavy_flavor_stack_3->SetBinContent(6,89.59268);
   hist_heavy_flavor_stack_3->SetBinContent(7,44.92437);
   hist_heavy_flavor_stack_3->SetBinContent(8,36.38242);
   hist_heavy_flavor_stack_3->SetBinContent(9,26.13551);
   hist_heavy_flavor_stack_3->SetBinContent(10,13.35485);
   hist_heavy_flavor_stack_3->SetBinContent(11,13.9807);
   hist_heavy_flavor_stack_3->SetBinContent(12,7.586481);
   hist_heavy_flavor_stack_3->SetBinContent(13,6.160505);
   hist_heavy_flavor_stack_3->SetBinContent(14,4.797658);
   hist_heavy_flavor_stack_3->SetBinContent(15,6.057959);
   hist_heavy_flavor_stack_3->SetBinContent(16,3.518009);
   hist_heavy_flavor_stack_3->SetBinContent(17,3.591141);
   hist_heavy_flavor_stack_3->SetBinContent(18,0.4559374);
   hist_heavy_flavor_stack_3->SetBinContent(19,-0.836367);
   hist_heavy_flavor_stack_3->SetBinContent(20,1.206625);
   hist_heavy_flavor_stack_3->SetBinContent(21,0.2940345);
   hist_heavy_flavor_stack_3->SetBinContent(22,26.29066);
   hist_heavy_flavor_stack_3->SetBinContent(23,1.231995);
   hist_heavy_flavor_stack_3->SetBinContent(24,0.8631063);
   hist_heavy_flavor_stack_3->SetBinContent(25,0.3658165);
   hist_heavy_flavor_stack_3->SetBinContent(26,11.30531);
   hist_heavy_flavor_stack_3->SetBinContent(27,0.0361978);
   hist_heavy_flavor_stack_3->SetBinContent(28,0.2058325);
   hist_heavy_flavor_stack_3->SetBinContent(29,0.2061104);
   hist_heavy_flavor_stack_3->SetBinContent(30,3.346255);
   hist_heavy_flavor_stack_3->SetBinError(4,38.31132);
   hist_heavy_flavor_stack_3->SetBinError(5,14.8688);
   hist_heavy_flavor_stack_3->SetBinError(6,19.29479);
   hist_heavy_flavor_stack_3->SetBinError(7,12.48208);
   hist_heavy_flavor_stack_3->SetBinError(8,11.7777);
   hist_heavy_flavor_stack_3->SetBinError(9,5.68137);
   hist_heavy_flavor_stack_3->SetBinError(10,3.141037);
   hist_heavy_flavor_stack_3->SetBinError(11,5.824637);
   hist_heavy_flavor_stack_3->SetBinError(12,1.881412);
   hist_heavy_flavor_stack_3->SetBinError(13,1.899553);
   hist_heavy_flavor_stack_3->SetBinError(14,1.228749);
   hist_heavy_flavor_stack_3->SetBinError(15,2.019919);
   hist_heavy_flavor_stack_3->SetBinError(16,1.249816);
   hist_heavy_flavor_stack_3->SetBinError(17,1.293371);
   hist_heavy_flavor_stack_3->SetBinError(18,0.220391);
   hist_heavy_flavor_stack_3->SetBinError(19,1.155299);
   hist_heavy_flavor_stack_3->SetBinError(20,0.8571652);
   hist_heavy_flavor_stack_3->SetBinError(21,0.20794);
   hist_heavy_flavor_stack_3->SetBinError(22,16.73365);
   hist_heavy_flavor_stack_3->SetBinError(23,0.6135083);
   hist_heavy_flavor_stack_3->SetBinError(24,0.4223546);
   hist_heavy_flavor_stack_3->SetBinError(25,0.2472503);
   hist_heavy_flavor_stack_3->SetBinError(26,12.06533);
   hist_heavy_flavor_stack_3->SetBinError(27,0.212071);
   hist_heavy_flavor_stack_3->SetBinError(28,0.1624069);
   hist_heavy_flavor_stack_3->SetBinError(29,0.1595442);
   hist_heavy_flavor_stack_3->SetBinError(30,0.6750938);
   hist_heavy_flavor_stack_3->SetEntries(2871);
   hist_heavy_flavor_stack_3->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_3->SetFillColor(ci);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_heavy_flavor_stack_3->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_3->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_3->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_3->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3017[33] = {
   0,
   2.5,
   7.5,
   12.5,
   17.5,
   22.5,
   27.5,
   32.5,
   37.5,
   42.5,
   47.5,
   52.5,
   57.5,
   62.5,
   67.5,
   72.5,
   77.5,
   82.5,
   87.5,
   92.5,
   97.5,
   102.5,
   107.5,
   112.5,
   117.5,
   122.5,
   127.5,
   132.5,
   137.5,
   142.5,
   147.5,
   152.5,
   157.5};
   Double_t totalStackErr_fy3017[33] = {
   0,
   0,
   0,
   0,
   5096.255,
   3008.944,
   2136.834,
   1404.41,
   1100.233,
   649.4591,
   544.9048,
   462.0621,
   296.957,
   218.2019,
   215.8869,
   138.861,
   149.3161,
   107.8931,
   84.83096,
   92.81342,
   89.34039,
   73.2594,
   91.97155,
   24.84176,
   46.54752,
   20.39705,
   43.06805,
   33.07484,
   42.84462,
   34.22647,
   320.9215,
   0,
   0};
   Double_t totalStackErr_felx3017[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fely3017[33] = {
   0,
   0,
   0,
   0,
   174.4357,
   132.8237,
   109.3501,
   90.33311,
   75.22197,
   56.2485,
   55.26732,
   52.50656,
   40.83794,
   32.78791,
   35.10111,
   21.89941,
   28.62645,
   21.19734,
   18.12169,
   23.13945,
   20.6008,
   18.09772,
   25.29606,
   5.988875,
   15.40147,
   3.386768,
   18.43026,
   12.6275,
   15.15665,
   13.84976,
   37.68116,
   0,
   0};
   Double_t totalStackErr_fehx3017[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fehy3017[33] = {
   0,
   0,
   0,
   0,
   174.4357,
   132.8237,
   109.3501,
   90.33311,
   75.22197,
   56.2485,
   55.26732,
   52.50656,
   40.83794,
   32.78791,
   35.10111,
   21.89941,
   28.62645,
   21.19734,
   18.12169,
   23.13945,
   20.6008,
   18.09772,
   25.29606,
   5.988875,
   15.40147,
   3.386768,
   18.43026,
   12.6275,
   15.15665,
   13.84976,
   37.68116,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(33,totalStackErr_fx3017,totalStackErr_fy3017,totalStackErr_felx3017,totalStackErr_fehx3017,totalStackErr_fely3017,totalStackErr_fehy3017);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3017 = new TH1F("Graph_totalStackErr3017","SM",100,0,176);
   Graph_totalStackErr3017->SetMinimum(0);
   Graph_totalStackErr3017->SetMaximum(5797.76);
   Graph_totalStackErr3017->SetDirectory(0);
   Graph_totalStackErr3017->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3017->SetLineColor(ci);
   Graph_totalStackErr3017->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3017->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3017->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3017->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3017->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3017->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3017->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3017->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3017);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_1"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_2"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_3"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEMME/lepPt_antiID\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",30,0,150);
   Graph_master_copy->SetMinimum(-1.991666);
   Graph_master_copy->SetMaximum(8000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEMME_lepPt_antiID->cd();
   CutWHG0BVetoEMME_lepPt_antiID->Modified();
   CutWHG0BVetoEMME_lepPt_antiID->cd();
   CutWHG0BVetoEMME_lepPt_antiID->SetSelected(CutWHG0BVetoEMME_lepPt_antiID);
}
