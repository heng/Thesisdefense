void run2-ee-CutWHG0BVetoEE-HWWWZVetoRNN_rebinned-log()
{
//=========Macro generated from canvas: CutWHG0BVetoEE_HWWWZVetoRNN_rebinned/CutWHG0BVetoEE_HWWWZVetoRNN_rebinned
//=========  (Fri Mar  3 15:30:42 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEE_HWWWZVetoRNN_rebinned = new TCanvas("CutWHG0BVetoEE_HWWWZVetoRNN_rebinned", "CutWHG0BVetoEE_HWWWZVetoRNN_rebinned",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetHighLightColor(2);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->Range(0,0,1,1);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetFillColor(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetBorderMode(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetBorderSize(2);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetLeftMargin(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetRightMargin(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetTopMargin(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetBottomMargin(0);
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-0.2025316,-2.440799,1.063291,6.564193);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis7[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",9, xAxis7);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(1300000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis8[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *stack_stack_2 = new TH1F("stack_stack_2","Background Stack",9, xAxis8);
   stack_stack_2->SetMinimum(2.520087);
   stack_stack_2->SetMaximum(7471.504);
   stack_stack_2->SetDirectory(0);
   stack_stack_2->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_2->SetLineColor(ci);
   stack_stack_2->GetXaxis()->SetLabelFont(42);
   stack_stack_2->GetXaxis()->SetLabelSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleSize(0.035);
   stack_stack_2->GetXaxis()->SetTitleOffset(1);
   stack_stack_2->GetXaxis()->SetTitleFont(42);
   stack_stack_2->GetYaxis()->SetLabelFont(42);
   stack_stack_2->GetYaxis()->SetLabelSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleSize(0.035);
   stack_stack_2->GetYaxis()->SetTitleFont(42);
   stack_stack_2->GetZaxis()->SetLabelFont(42);
   stack_stack_2->GetZaxis()->SetLabelSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleSize(0.035);
   stack_stack_2->GetZaxis()->SetTitleOffset(1);
   stack_stack_2->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_2);
   
   Double_t xAxis9[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_heavy_flavor_stack_1 = new TH1F("hist_heavy_flavor_stack_1","heavy flavor",9, xAxis9);
   hist_heavy_flavor_stack_1->SetBinContent(1,127.807);
   hist_heavy_flavor_stack_1->SetBinContent(2,79.70477);
   hist_heavy_flavor_stack_1->SetBinContent(3,56.43322);
   hist_heavy_flavor_stack_1->SetBinContent(4,56.6457);
   hist_heavy_flavor_stack_1->SetBinContent(5,30.69435);
   hist_heavy_flavor_stack_1->SetBinContent(6,33.46527);
   hist_heavy_flavor_stack_1->SetBinContent(7,33.6854);
   hist_heavy_flavor_stack_1->SetBinContent(8,8.186784);
   hist_heavy_flavor_stack_1->SetBinContent(9,23.97047);
   hist_heavy_flavor_stack_1->SetBinError(1,21.78582);
   hist_heavy_flavor_stack_1->SetBinError(2,14.41465);
   hist_heavy_flavor_stack_1->SetBinError(3,13.8517);
   hist_heavy_flavor_stack_1->SetBinError(4,16.17788);
   hist_heavy_flavor_stack_1->SetBinError(5,9.756291);
   hist_heavy_flavor_stack_1->SetBinError(6,16.60162);
   hist_heavy_flavor_stack_1->SetBinError(7,15.47521);
   hist_heavy_flavor_stack_1->SetBinError(8,1.345924);
   hist_heavy_flavor_stack_1->SetBinError(9,13.97927);
   hist_heavy_flavor_stack_1->SetEntries(1746);
   hist_heavy_flavor_stack_1->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_1->SetFillColor(ci);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_1,"");
   Double_t xAxis10[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_light_flavor_stack_2 = new TH1F("hist_light_flavor_stack_2","light flavor",9, xAxis10);
   hist_light_flavor_stack_2->SetBinContent(1,920.4984);
   hist_light_flavor_stack_2->SetBinContent(2,419.4117);
   hist_light_flavor_stack_2->SetBinContent(3,278.2816);
   hist_light_flavor_stack_2->SetBinContent(4,186.0631);
   hist_light_flavor_stack_2->SetBinContent(5,234.5112);
   hist_light_flavor_stack_2->SetBinContent(6,121.7152);
   hist_light_flavor_stack_2->SetBinContent(7,70.4193);
   hist_light_flavor_stack_2->SetBinContent(8,64.3825);
   hist_light_flavor_stack_2->SetBinContent(9,61.80289);
   hist_light_flavor_stack_2->SetBinError(1,66.71992);
   hist_light_flavor_stack_2->SetBinError(2,49.92626);
   hist_light_flavor_stack_2->SetBinError(3,41.19917);
   hist_light_flavor_stack_2->SetBinError(4,30.03369);
   hist_light_flavor_stack_2->SetBinError(5,40.66976);
   hist_light_flavor_stack_2->SetBinError(6,29.33821);
   hist_light_flavor_stack_2->SetBinError(7,19.93322);
   hist_light_flavor_stack_2->SetBinError(8,17.08479);
   hist_light_flavor_stack_2->SetBinError(9,20.86073);
   hist_light_flavor_stack_2->SetEntries(7199);
   hist_light_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_2->SetFillColor(ci);
   hist_light_flavor_stack_2->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_light_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_2,"");
   Double_t xAxis11[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *hist_gamma_conversion_stack_3 = new TH1F("hist_gamma_conversion_stack_3","$\\gamma$ conv.",9, xAxis11);
   hist_gamma_conversion_stack_3->SetBinContent(1,3688.794);
   hist_gamma_conversion_stack_3->SetBinContent(2,1368.016);
   hist_gamma_conversion_stack_3->SetBinContent(3,911.2017);
   hist_gamma_conversion_stack_3->SetBinContent(4,669.4946);
   hist_gamma_conversion_stack_3->SetBinContent(5,551.5527);
   hist_gamma_conversion_stack_3->SetBinContent(6,233.3312);
   hist_gamma_conversion_stack_3->SetBinContent(7,153.0584);
   hist_gamma_conversion_stack_3->SetBinContent(8,138.46);
   hist_gamma_conversion_stack_3->SetBinContent(9,69.17596);
   hist_gamma_conversion_stack_3->SetBinError(1,132.0486);
   hist_gamma_conversion_stack_3->SetBinError(2,82.61654);
   hist_gamma_conversion_stack_3->SetBinError(3,64.0326);
   hist_gamma_conversion_stack_3->SetBinError(4,52.25083);
   hist_gamma_conversion_stack_3->SetBinError(5,51.29037);
   hist_gamma_conversion_stack_3->SetBinError(6,32.62912);
   hist_gamma_conversion_stack_3->SetBinError(7,28.30375);
   hist_gamma_conversion_stack_3->SetBinError(8,21.80085);
   hist_gamma_conversion_stack_3->SetBinError(9,17.21464);
   hist_gamma_conversion_stack_3->SetEntries(39735);
   hist_gamma_conversion_stack_3->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_3->SetFillColor(ci);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitle("RNN WH vs WZ");
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3002[12] = {
   0,
   0.105,
   0.275,
   0.405,
   0.525,
   0.63,
   0.72,
   0.8,
   0.885,
   0.965,
   1.055556,
   1.166667};
   Double_t totalStackErr_fy3002[12] = {
   0,
   4737.099,
   1867.133,
   1245.917,
   912.2034,
   816.7582,
   388.5117,
   257.1631,
   211.0293,
   154.9493,
   0,
   0};
   Double_t totalStackErr_felx3002[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fely3002[12] = {
   0,
   149.5426,
   97.60075,
   77.39131,
   62.40109,
   66.18094,
   46.91485,
   37.91989,
   27.73046,
   30.44559,
   0,
   0};
   Double_t totalStackErr_fehx3002[12] = {
   0,
   0.105,
   0.065,
   0.065,
   0.055,
   0.05,
   0.04,
   0.04,
   0.045,
   0.035,
   0.035,
   0.035};
   Double_t totalStackErr_fehy3002[12] = {
   0,
   149.5426,
   97.60075,
   77.39131,
   62.40109,
   66.18094,
   46.91485,
   37.91989,
   27.73046,
   30.44559,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(12,totalStackErr_fx3002,totalStackErr_fy3002,totalStackErr_felx3002,totalStackErr_fehx3002,totalStackErr_fely3002,totalStackErr_fehy3002);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3002 = new TH1F("Graph_totalStackErr3002","SM",100,0,1.321833);
   Graph_totalStackErr3002->SetMinimum(5.375306);
   Graph_totalStackErr3002->SetMaximum(5375.306);
   Graph_totalStackErr3002->SetDirectory(0);
   Graph_totalStackErr3002->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3002->SetLineColor(ci);
   Graph_totalStackErr3002->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3002->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3002->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3002->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3002);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_3"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_2"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_1"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEE/HWWWZVetoRNN_rebinned\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"ee channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis12[10] = {0, 0.21, 0.34, 0.47, 0.58, 0.68, 0.76, 0.84, 0.93, 1}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",9, xAxis12);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(1300000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("RNN WH vs WZ");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->Modified();
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->cd();
   CutWHG0BVetoEE_HWWWZVetoRNN_rebinned->SetSelected(CutWHG0BVetoEE_HWWWZVetoRNN_rebinned);
}
