void run2-ee-CutWHG0BVetoEE-lepPt_antiID-log()
{
//=========Macro generated from canvas: CutWHG0BVetoEE_lepPt_antiID/CutWHG0BVetoEE_lepPt_antiID
//=========  (Fri Mar  3 15:30:45 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEE_lepPt_antiID = new TCanvas("CutWHG0BVetoEE_lepPt_antiID", "CutWHG0BVetoEE_lepPt_antiID",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEE_lepPt_antiID->SetHighLightColor(2);
   CutWHG0BVetoEE_lepPt_antiID->Range(0,0,1,1);
   CutWHG0BVetoEE_lepPt_antiID->SetFillColor(0);
   CutWHG0BVetoEE_lepPt_antiID->SetBorderMode(0);
   CutWHG0BVetoEE_lepPt_antiID->SetBorderSize(2);
   CutWHG0BVetoEE_lepPt_antiID->SetLeftMargin(0);
   CutWHG0BVetoEE_lepPt_antiID->SetRightMargin(0);
   CutWHG0BVetoEE_lepPt_antiID->SetTopMargin(0);
   CutWHG0BVetoEE_lepPt_antiID->SetBottomMargin(0);
   CutWHG0BVetoEE_lepPt_antiID->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-2.408454,159.4937,6.394384);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",30,0,150);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(900000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   
   TH1F *stack_stack_8 = new TH1F("stack_stack_8","Background Stack",30,0,150);
   stack_stack_8->SetMinimum(1.455779);
   stack_stack_8->SetMaximum(5823.114);
   stack_stack_8->SetDirectory(0);
   stack_stack_8->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_8->SetLineColor(ci);
   stack_stack_8->GetXaxis()->SetLabelFont(42);
   stack_stack_8->GetXaxis()->SetLabelSize(0.035);
   stack_stack_8->GetXaxis()->SetTitleSize(0.035);
   stack_stack_8->GetXaxis()->SetTitleOffset(1);
   stack_stack_8->GetXaxis()->SetTitleFont(42);
   stack_stack_8->GetYaxis()->SetLabelFont(42);
   stack_stack_8->GetYaxis()->SetLabelSize(0.035);
   stack_stack_8->GetYaxis()->SetTitleSize(0.035);
   stack_stack_8->GetYaxis()->SetTitleFont(42);
   stack_stack_8->GetZaxis()->SetLabelFont(42);
   stack_stack_8->GetZaxis()->SetLabelSize(0.035);
   stack_stack_8->GetZaxis()->SetTitleSize(0.035);
   stack_stack_8->GetZaxis()->SetTitleOffset(1);
   stack_stack_8->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_8);
   
   
   TH1F *hist_heavy_flavor_stack_1 = new TH1F("hist_heavy_flavor_stack_1","heavy flavor",30,0,150);
   hist_heavy_flavor_stack_1->SetBinContent(4,187.2334);
   hist_heavy_flavor_stack_1->SetBinContent(5,71.23119);
   hist_heavy_flavor_stack_1->SetBinContent(6,45.73618);
   hist_heavy_flavor_stack_1->SetBinContent(7,19.909);
   hist_heavy_flavor_stack_1->SetBinContent(8,19.40265);
   hist_heavy_flavor_stack_1->SetBinContent(9,9.098393);
   hist_heavy_flavor_stack_1->SetBinContent(10,6.828803);
   hist_heavy_flavor_stack_1->SetBinContent(11,5.201521);
   hist_heavy_flavor_stack_1->SetBinContent(12,1.909709);
   hist_heavy_flavor_stack_1->SetBinContent(13,1.740006);
   hist_heavy_flavor_stack_1->SetBinContent(14,2.515142);
   hist_heavy_flavor_stack_1->SetBinContent(15,9.576229);
   hist_heavy_flavor_stack_1->SetBinContent(16,1.26731);
   hist_heavy_flavor_stack_1->SetBinContent(17,6.016017);
   hist_heavy_flavor_stack_1->SetBinContent(18,9.637779);
   hist_heavy_flavor_stack_1->SetBinContent(19,0.9679713);
   hist_heavy_flavor_stack_1->SetBinContent(20,19.40777);
   hist_heavy_flavor_stack_1->SetBinContent(21,0.9795704);
   hist_heavy_flavor_stack_1->SetBinContent(22,0.1601139);
   hist_heavy_flavor_stack_1->SetBinContent(23,0.2667179);
   hist_heavy_flavor_stack_1->SetBinContent(24,0.4157815);
   hist_heavy_flavor_stack_1->SetBinContent(25,0.7678964);
   hist_heavy_flavor_stack_1->SetBinContent(26,0.473782);
   hist_heavy_flavor_stack_1->SetBinContent(27,0.8352373);
   hist_heavy_flavor_stack_1->SetBinContent(28,0.5488803);
   hist_heavy_flavor_stack_1->SetBinContent(29,1.644303);
   hist_heavy_flavor_stack_1->SetBinContent(30,26.82161);
   hist_heavy_flavor_stack_1->SetBinError(4,28.48887);
   hist_heavy_flavor_stack_1->SetBinError(5,16.16488);
   hist_heavy_flavor_stack_1->SetBinError(6,14.50799);
   hist_heavy_flavor_stack_1->SetBinError(7,3.777325);
   hist_heavy_flavor_stack_1->SetBinError(8,7.221018);
   hist_heavy_flavor_stack_1->SetBinError(9,1.84145);
   hist_heavy_flavor_stack_1->SetBinError(10,1.442854);
   hist_heavy_flavor_stack_1->SetBinError(11,1.863568);
   hist_heavy_flavor_stack_1->SetBinError(12,1.135798);
   hist_heavy_flavor_stack_1->SetBinError(13,0.5517301);
   hist_heavy_flavor_stack_1->SetBinError(14,0.7010995);
   hist_heavy_flavor_stack_1->SetBinError(15,6.966349);
   hist_heavy_flavor_stack_1->SetBinError(16,0.5229778);
   hist_heavy_flavor_stack_1->SetBinError(17,3.347259);
   hist_heavy_flavor_stack_1->SetBinError(18,7.553984);
   hist_heavy_flavor_stack_1->SetBinError(19,0.5030294);
   hist_heavy_flavor_stack_1->SetBinError(20,13.69787);
   hist_heavy_flavor_stack_1->SetBinError(21,0.6692701);
   hist_heavy_flavor_stack_1->SetBinError(22,0.1563228);
   hist_heavy_flavor_stack_1->SetBinError(23,0.1754822);
   hist_heavy_flavor_stack_1->SetBinError(24,0.2824097);
   hist_heavy_flavor_stack_1->SetBinError(25,0.3608757);
   hist_heavy_flavor_stack_1->SetBinError(26,0.3483934);
   hist_heavy_flavor_stack_1->SetBinError(27,0.4900271);
   hist_heavy_flavor_stack_1->SetBinError(28,0.4230678);
   hist_heavy_flavor_stack_1->SetBinError(29,0.6851532);
   hist_heavy_flavor_stack_1->SetBinError(30,16.61289);
   hist_heavy_flavor_stack_1->SetEntries(1746);
   hist_heavy_flavor_stack_1->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_1->SetFillColor(ci);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_1,"");
   
   TH1F *hist_light_flavor_stack_2 = new TH1F("hist_light_flavor_stack_2","light flavor",30,0,150);
   hist_light_flavor_stack_2->SetBinContent(4,720.5197);
   hist_light_flavor_stack_2->SetBinContent(5,401.0945);
   hist_light_flavor_stack_2->SetBinContent(6,206.3807);
   hist_light_flavor_stack_2->SetBinContent(7,80.3521);
   hist_light_flavor_stack_2->SetBinContent(8,120.1068);
   hist_light_flavor_stack_2->SetBinContent(9,103.8824);
   hist_light_flavor_stack_2->SetBinContent(10,112.4414);
   hist_light_flavor_stack_2->SetBinContent(11,67.3182);
   hist_light_flavor_stack_2->SetBinContent(12,37.30265);
   hist_light_flavor_stack_2->SetBinContent(13,84.54559);
   hist_light_flavor_stack_2->SetBinContent(14,44.60847);
   hist_light_flavor_stack_2->SetBinContent(15,39.79761);
   hist_light_flavor_stack_2->SetBinContent(16,40.39814);
   hist_light_flavor_stack_2->SetBinContent(17,34.1863);
   hist_light_flavor_stack_2->SetBinContent(18,29.26766);
   hist_light_flavor_stack_2->SetBinContent(19,27.36215);
   hist_light_flavor_stack_2->SetBinContent(20,15.76594);
   hist_light_flavor_stack_2->SetBinContent(21,7.682444);
   hist_light_flavor_stack_2->SetBinContent(22,6.533193);
   hist_light_flavor_stack_2->SetBinContent(23,18.19343);
   hist_light_flavor_stack_2->SetBinContent(24,11.40766);
   hist_light_flavor_stack_2->SetBinContent(25,6.28392);
   hist_light_flavor_stack_2->SetBinContent(26,12.30828);
   hist_light_flavor_stack_2->SetBinContent(27,5.397186);
   hist_light_flavor_stack_2->SetBinContent(28,5.193487);
   hist_light_flavor_stack_2->SetBinContent(29,16.95545);
   hist_light_flavor_stack_2->SetBinContent(30,101.801);
   hist_light_flavor_stack_2->SetBinError(4,64.19888);
   hist_light_flavor_stack_2->SetBinError(5,50.41194);
   hist_light_flavor_stack_2->SetBinError(6,35.96477);
   hist_light_flavor_stack_2->SetBinError(7,15.907);
   hist_light_flavor_stack_2->SetBinError(8,27.56692);
   hist_light_flavor_stack_2->SetBinError(9,22.11495);
   hist_light_flavor_stack_2->SetBinError(10,25.61419);
   hist_light_flavor_stack_2->SetBinError(11,19.94827);
   hist_light_flavor_stack_2->SetBinError(12,10.43827);
   hist_light_flavor_stack_2->SetBinError(13,23.50997);
   hist_light_flavor_stack_2->SetBinError(14,15.65562);
   hist_light_flavor_stack_2->SetBinError(15,14.80239);
   hist_light_flavor_stack_2->SetBinError(16,14.35762);
   hist_light_flavor_stack_2->SetBinError(17,14.4609);
   hist_light_flavor_stack_2->SetBinError(18,13.46975);
   hist_light_flavor_stack_2->SetBinError(19,12.43975);
   hist_light_flavor_stack_2->SetBinError(20,7.458056);
   hist_light_flavor_stack_2->SetBinError(21,1.893736);
   hist_light_flavor_stack_2->SetBinError(22,1.691622);
   hist_light_flavor_stack_2->SetBinError(23,11.18789);
   hist_light_flavor_stack_2->SetBinError(24,6.147175);
   hist_light_flavor_stack_2->SetBinError(25,1.941015);
   hist_light_flavor_stack_2->SetBinError(26,5.398876);
   hist_light_flavor_stack_2->SetBinError(27,1.479707);
   hist_light_flavor_stack_2->SetBinError(28,1.778978);
   hist_light_flavor_stack_2->SetBinError(29,12.18078);
   hist_light_flavor_stack_2->SetBinError(30,19.52818);
   hist_light_flavor_stack_2->SetEntries(7199);
   hist_light_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_2->SetFillColor(ci);
   hist_light_flavor_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_light_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_2,"");
   
   TH1F *hist_gamma_conversion_stack_3 = new TH1F("hist_gamma_conversion_stack_3","$\\gamma$ conv.",30,0,150);
   hist_gamma_conversion_stack_3->SetBinContent(4,2731.693);
   hist_gamma_conversion_stack_3->SetBinContent(5,1477.547);
   hist_gamma_conversion_stack_3->SetBinContent(6,911.5435);
   hist_gamma_conversion_stack_3->SetBinContent(7,698.1912);
   hist_gamma_conversion_stack_3->SetBinContent(8,380.9547);
   hist_gamma_conversion_stack_3->SetBinContent(9,313.5477);
   hist_gamma_conversion_stack_3->SetBinContent(10,255.3338);
   hist_gamma_conversion_stack_3->SetBinContent(11,151.656);
   hist_gamma_conversion_stack_3->SetBinContent(12,136.8768);
   hist_gamma_conversion_stack_3->SetBinContent(13,84.8962);
   hist_gamma_conversion_stack_3->SetBinContent(14,60.13377);
   hist_gamma_conversion_stack_3->SetBinContent(15,77.01597);
   hist_gamma_conversion_stack_3->SetBinContent(16,66.7381);
   hist_gamma_conversion_stack_3->SetBinContent(17,76.09689);
   hist_gamma_conversion_stack_3->SetBinContent(18,25.8984);
   hist_gamma_conversion_stack_3->SetBinContent(19,34.37153);
   hist_gamma_conversion_stack_3->SetBinContent(20,34.06396);
   hist_gamma_conversion_stack_3->SetBinContent(21,18.33499);
   hist_gamma_conversion_stack_3->SetBinContent(22,28.52141);
   hist_gamma_conversion_stack_3->SetBinContent(23,24.99801);
   hist_gamma_conversion_stack_3->SetBinContent(24,16.75197);
   hist_gamma_conversion_stack_3->SetBinContent(25,20.86481);
   hist_gamma_conversion_stack_3->SetBinContent(26,13.53632);
   hist_gamma_conversion_stack_3->SetBinContent(27,19.85841);
   hist_gamma_conversion_stack_3->SetBinContent(28,11.86245);
   hist_gamma_conversion_stack_3->SetBinContent(29,8.095451);
   hist_gamma_conversion_stack_3->SetBinContent(30,103.7028);
   hist_gamma_conversion_stack_3->SetBinError(4,113.822);
   hist_gamma_conversion_stack_3->SetBinError(5,85.69532);
   hist_gamma_conversion_stack_3->SetBinError(6,66.73278);
   hist_gamma_conversion_stack_3->SetBinError(7,63.2601);
   hist_gamma_conversion_stack_3->SetBinError(8,41.38987);
   hist_gamma_conversion_stack_3->SetBinError(9,38.39499);
   hist_gamma_conversion_stack_3->SetBinError(10,37.83074);
   hist_gamma_conversion_stack_3->SetBinError(11,25.90487);
   hist_gamma_conversion_stack_3->SetBinError(12,21.90294);
   hist_gamma_conversion_stack_3->SetBinError(13,18.09016);
   hist_gamma_conversion_stack_3->SetBinError(14,8.780308);
   hist_gamma_conversion_stack_3->SetBinError(15,14.86217);
   hist_gamma_conversion_stack_3->SetBinError(16,17.03046);
   hist_gamma_conversion_stack_3->SetBinError(17,20.18922);
   hist_gamma_conversion_stack_3->SetBinError(18,6.717133);
   hist_gamma_conversion_stack_3->SetBinError(19,8.313024);
   hist_gamma_conversion_stack_3->SetBinError(20,7.15567);
   hist_gamma_conversion_stack_3->SetBinError(21,7.490787);
   hist_gamma_conversion_stack_3->SetBinError(22,7.082299);
   hist_gamma_conversion_stack_3->SetBinError(23,4.779329);
   hist_gamma_conversion_stack_3->SetBinError(24,2.816636);
   hist_gamma_conversion_stack_3->SetBinError(25,7.500697);
   hist_gamma_conversion_stack_3->SetBinError(26,2.421211);
   hist_gamma_conversion_stack_3->SetBinError(27,7.344306);
   hist_gamma_conversion_stack_3->SetBinError(28,2.81318);
   hist_gamma_conversion_stack_3->SetBinError(29,3.53074);
   hist_gamma_conversion_stack_3->SetBinError(30,8.685467);
   hist_gamma_conversion_stack_3->SetEntries(39735);
   hist_gamma_conversion_stack_3->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_3->SetFillColor(ci);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3008[33] = {
   0,
   2.5,
   7.5,
   12.5,
   17.5,
   22.5,
   27.5,
   32.5,
   37.5,
   42.5,
   47.5,
   52.5,
   57.5,
   62.5,
   67.5,
   72.5,
   77.5,
   82.5,
   87.5,
   92.5,
   97.5,
   102.5,
   107.5,
   112.5,
   117.5,
   122.5,
   127.5,
   132.5,
   137.5,
   142.5,
   147.5,
   152.5,
   157.5};
   Double_t totalStackErr_fy3008[33] = {
   0,
   0,
   0,
   0,
   3639.446,
   1949.873,
   1163.66,
   798.4523,
   520.4642,
   426.5284,
   374.6039,
   224.1757,
   176.0891,
   171.1818,
   107.2574,
   126.3898,
   108.4036,
   116.2992,
   64.80383,
   62.70166,
   69.23767,
   26.997,
   35.21472,
   43.45816,
   28.57541,
   27.91663,
   26.31839,
   26.09083,
   17.60481,
   26.69521,
   232.3253,
   0,
   0};
   Double_t totalStackErr_felx3008[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fely3008[33] = {
   0,
   0,
   0,
   0,
   133.7481,
   100.7291,
   77.18296,
   65.33867,
   50.25137,
   44.34679,
   45.70923,
   32.74857,
   24.28964,
   29.66946,
   17.9634,
   22.1026,
   22.28119,
   25.05846,
   16.84092,
   14.97019,
   17.15977,
   7.755388,
   7.283199,
   12.16724,
   6.767641,
   7.756174,
   5.927184,
   7.507896,
   3.355254,
   12.70067,
   27.06983,
   0,
   0};
   Double_t totalStackErr_fehx3008[33] = {
   0,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5,
   2.5};
   Double_t totalStackErr_fehy3008[33] = {
   0,
   0,
   0,
   0,
   133.7481,
   100.7291,
   77.18296,
   65.33867,
   50.25137,
   44.34679,
   45.70923,
   32.74857,
   24.28964,
   29.66946,
   17.9634,
   22.1026,
   22.28119,
   25.05846,
   16.84092,
   14.97019,
   17.15977,
   7.755388,
   7.283199,
   12.16724,
   6.767641,
   7.756174,
   5.927184,
   7.507896,
   3.355254,
   12.70067,
   27.06983,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(33,totalStackErr_fx3008,totalStackErr_fy3008,totalStackErr_felx3008,totalStackErr_fehx3008,totalStackErr_fely3008,totalStackErr_fehy3008);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3008 = new TH1F("Graph_totalStackErr3008","SM",100,0,176);
   Graph_totalStackErr3008->SetMinimum(4.150514);
   Graph_totalStackErr3008->SetMaximum(4150.514);
   Graph_totalStackErr3008->SetDirectory(0);
   Graph_totalStackErr3008->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3008->SetLineColor(ci);
   Graph_totalStackErr3008->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3008->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3008->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3008->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3008->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3008->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3008->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3008->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3008->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3008->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3008->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3008->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3008->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3008->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3008);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_3"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_2"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_1"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEE/lepPt_antiID\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"ee channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",30,0,150);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(900000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events / 5 GeV");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEE_lepPt_antiID->cd();
   CutWHG0BVetoEE_lepPt_antiID->Modified();
   CutWHG0BVetoEE_lepPt_antiID->cd();
   CutWHG0BVetoEE_lepPt_antiID->SetSelected(CutWHG0BVetoEE_lepPt_antiID);
}
