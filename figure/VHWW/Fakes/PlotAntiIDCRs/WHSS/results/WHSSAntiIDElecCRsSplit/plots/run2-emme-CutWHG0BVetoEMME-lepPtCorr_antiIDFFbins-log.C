void run2-emme-CutWHG0BVetoEMME-lepPtCorr_antiIDFFbins-log()
{
//=========Macro generated from canvas: CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins/CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins
//=========  (Fri Mar  3 15:30:50 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins = new TCanvas("CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins", "CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetHighLightColor(2);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->Range(0,0,1,1);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetFillColor(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetBorderMode(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetBorderSize(2);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetLeftMargin(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetRightMargin(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetTopMargin(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetBottomMargin(0);
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-2.559285,159.4937,7.186247);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis55[4] = {0, 15, 30, 150}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",3, xAxis55);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(5000000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis56[4] = {0, 15, 30, 150}; 
   
   TH1F *stack_stack_16 = new TH1F("stack_stack_16","Background Stack",3, xAxis56);
   stack_stack_16->SetMinimum(4.096814);
   stack_stack_16->SetMaximum(16387.25);
   stack_stack_16->SetDirectory(0);
   stack_stack_16->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_16->SetLineColor(ci);
   stack_stack_16->GetXaxis()->SetLabelFont(42);
   stack_stack_16->GetXaxis()->SetLabelSize(0.035);
   stack_stack_16->GetXaxis()->SetTitleSize(0.035);
   stack_stack_16->GetXaxis()->SetTitleOffset(1);
   stack_stack_16->GetXaxis()->SetTitleFont(42);
   stack_stack_16->GetYaxis()->SetLabelFont(42);
   stack_stack_16->GetYaxis()->SetLabelSize(0.035);
   stack_stack_16->GetYaxis()->SetTitleSize(0.035);
   stack_stack_16->GetYaxis()->SetTitleFont(42);
   stack_stack_16->GetZaxis()->SetLabelFont(42);
   stack_stack_16->GetZaxis()->SetLabelSize(0.035);
   stack_stack_16->GetZaxis()->SetTitleSize(0.035);
   stack_stack_16->GetZaxis()->SetTitleOffset(1);
   stack_stack_16->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_16);
   
   Double_t xAxis57[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_heavy_flavor_stack_1 = new TH1F("hist_heavy_flavor_stack_1","heavy flavor",3, xAxis57);
   hist_heavy_flavor_stack_1->SetBinContent(2,436.903);
   hist_heavy_flavor_stack_1->SetBinContent(3,211.4611);
   hist_heavy_flavor_stack_1->SetBinError(2,45.39964);
   hist_heavy_flavor_stack_1->SetBinError(3,28.55655);
   hist_heavy_flavor_stack_1->SetEntries(2871);
   hist_heavy_flavor_stack_1->SetStats(0);

   ci = TColor::GetColor("#ff0000");
   hist_heavy_flavor_stack_1->SetFillColor(ci);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetXaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetYaxis()->SetTitleFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelFont(42);
   hist_heavy_flavor_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_heavy_flavor_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_heavy_flavor_stack_1,"");
   Double_t xAxis58[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_light_flavor_stack_2 = new TH1F("hist_light_flavor_stack_2","light flavor",3, xAxis58);
   hist_light_flavor_stack_2->SetBinContent(2,2261.362);
   hist_light_flavor_stack_2->SetBinContent(3,1651.997);
   hist_light_flavor_stack_2->SetBinError(2,132.2998);
   hist_light_flavor_stack_2->SetBinError(3,109.5777);
   hist_light_flavor_stack_2->SetEntries(7879);
   hist_light_flavor_stack_2->SetStats(0);

   ci = TColor::GetColor("#0000ff");
   hist_light_flavor_stack_2->SetFillColor(ci);
   hist_light_flavor_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_light_flavor_stack_2->GetXaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetXaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetYaxis()->SetTitleFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelFont(42);
   hist_light_flavor_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_light_flavor_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_light_flavor_stack_2,"");
   Double_t xAxis59[4] = {0, 15, 30, 150}; 
   
   TH1F *hist_gamma_conversion_stack_3 = new TH1F("hist_gamma_conversion_stack_3","$\\gamma$ conv.",3, xAxis59);
   hist_gamma_conversion_stack_3->SetBinContent(2,7543.769);
   hist_gamma_conversion_stack_3->SetBinContent(3,4422.863);
   hist_gamma_conversion_stack_3->SetBinError(2,201.1542);
   hist_gamma_conversion_stack_3->SetBinError(3,143.4398);
   hist_gamma_conversion_stack_3->SetEntries(41807);
   hist_gamma_conversion_stack_3->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_gamma_conversion_stack_3->SetFillColor(ci);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetXaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetYaxis()->SetTitleFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelFont(42);
   hist_gamma_conversion_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_gamma_conversion_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_gamma_conversion_stack_3,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3016[6] = {
   0,
   7.5,
   22.5,
   90,
   175,
   225};
   Double_t totalStackErr_fy3016[6] = {
   0,
   0,
   10242.03,
   6286.321,
   0,
   0};
   Double_t totalStackErr_felx3016[6] = {
   0,
   7.5,
   7.5,
   60,
   60,
   60};
   Double_t totalStackErr_fely3016[6] = {
   0,
   0,
   245.0048,
   182.7504,
   0,
   0};
   Double_t totalStackErr_fehx3016[6] = {
   0,
   7.5,
   7.5,
   60,
   60,
   60};
   Double_t totalStackErr_fehy3016[6] = {
   0,
   0,
   245.0048,
   182.7504,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(6,totalStackErr_fx3016,totalStackErr_fy3016,totalStackErr_felx3016,totalStackErr_fehx3016,totalStackErr_fely3016,totalStackErr_fehy3016);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3016 = new TH1F("Graph_totalStackErr3016","SM",100,0,313.5);
   Graph_totalStackErr3016->SetMinimum(11.53574);
   Graph_totalStackErr3016->SetMaximum(11535.74);
   Graph_totalStackErr3016->SetDirectory(0);
   Graph_totalStackErr3016->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3016->SetLineColor(ci);
   Graph_totalStackErr3016->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3016->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3016->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3016->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3016->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3016->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3016->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3016->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3016->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3016->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3016->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3016->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3016->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3016->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3016);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.852,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_gamma_conversion_stack_3"," #it{#gamma} conv.","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_light_flavor_stack_2"," light flavor","f");

   ci = TColor::GetColor("#0000ff");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_heavy_flavor_stack_1"," heavy flavor","f");

   ci = TColor::GetColor("#ff0000");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEMME/lepPtCorr_antiIDFFbins\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis60[4] = {0, 15, 30, 150}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",3, xAxis60);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(5000000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid #mu}+p_{T,varcone}^{30 antiid #mu} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->cd();
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->Modified();
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->cd();
   CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins->SetSelected(CutWHG0BVetoEMME_lepPtCorr_antiIDFFbins);
}
