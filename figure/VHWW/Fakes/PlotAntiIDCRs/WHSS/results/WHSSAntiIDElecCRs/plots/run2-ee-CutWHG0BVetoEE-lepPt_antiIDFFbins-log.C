void run2-ee-CutWHG0BVetoEE-lepPt_antiIDFFbins-log()
{
//=========Macro generated from canvas: CutWHG0BVetoEE_lepPt_antiIDFFbins/CutWHG0BVetoEE_lepPt_antiIDFFbins
//=========  (Fri Mar  3 15:27:55 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEE_lepPt_antiIDFFbins = new TCanvas("CutWHG0BVetoEE_lepPt_antiIDFFbins", "CutWHG0BVetoEE_lepPt_antiIDFFbins",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetHighLightColor(2);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->Range(0,0,1,1);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetFillColor(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetBorderMode(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetBorderSize(2);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetLeftMargin(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetRightMargin(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetTopMargin(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetBottomMargin(0);
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-2.433758,159.4937,6.527231);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetLogy();
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis46[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",4, xAxis46);
   Graph_master->SetMinimum(0.1);
   Graph_master->SetMaximum(1200000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis47[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *stack_stack_10 = new TH1F("stack_stack_10","Background Stack",4, xAxis47);
   stack_stack_10->SetMinimum(1.535114);
   stack_stack_10->SetMaximum(6140.455);
   stack_stack_10->SetDirectory(0);
   stack_stack_10->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_10->SetLineColor(ci);
   stack_stack_10->GetXaxis()->SetLabelFont(42);
   stack_stack_10->GetXaxis()->SetLabelSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleSize(0.035);
   stack_stack_10->GetXaxis()->SetTitleOffset(1);
   stack_stack_10->GetXaxis()->SetTitleFont(42);
   stack_stack_10->GetYaxis()->SetLabelFont(42);
   stack_stack_10->GetYaxis()->SetLabelSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleSize(0.035);
   stack_stack_10->GetYaxis()->SetTitleFont(42);
   stack_stack_10->GetZaxis()->SetLabelFont(42);
   stack_stack_10->GetZaxis()->SetLabelSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleSize(0.035);
   stack_stack_10->GetZaxis()->SetTitleOffset(1);
   stack_stack_10->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_10);
   
   Double_t xAxis48[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1 = new TH1F("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1","Other fakes",4, xAxis48);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinContent(2,21.29815);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinContent(3,17.53621);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinContent(4,34.88229);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinError(2,2.326883);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinError(3,1.531236);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetBinError(4,2.380038);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetEntries(2102);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetStats(0);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->SetFillColor(15);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetXaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetYaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetYaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetZaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetZaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1,"");
   Double_t xAxis49[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_tt_stack_2 = new TH1F("hist_tt_stack_2","$t\\bar{t}$",4, xAxis49);
   hist_tt_stack_2->SetBinContent(2,68.14661);
   hist_tt_stack_2->SetBinContent(3,59.81488);
   hist_tt_stack_2->SetBinContent(4,87.96771);
   hist_tt_stack_2->SetBinError(2,3.202018);
   hist_tt_stack_2->SetBinError(3,3.017721);
   hist_tt_stack_2->SetBinError(4,3.652672);
   hist_tt_stack_2->SetEntries(1561);
   hist_tt_stack_2->SetStats(0);
   hist_tt_stack_2->SetFillColor(219);
   hist_tt_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_tt_stack_2->GetXaxis()->SetLabelFont(42);
   hist_tt_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_tt_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_tt_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_tt_stack_2->GetXaxis()->SetTitleFont(42);
   hist_tt_stack_2->GetYaxis()->SetLabelFont(42);
   hist_tt_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_tt_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_tt_stack_2->GetYaxis()->SetTitleFont(42);
   hist_tt_stack_2->GetZaxis()->SetLabelFont(42);
   hist_tt_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_tt_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_tt_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_tt_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tt_stack_2,"");
   Double_t xAxis50[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Zjets_stack_3 = new TH1F("hist_Zjets_stack_3","$Z+$jets",4, xAxis50);
   hist_Zjets_stack_3->SetBinContent(2,164.1662);
   hist_Zjets_stack_3->SetBinContent(3,100.5176);
   hist_Zjets_stack_3->SetBinContent(4,130.6703);
   hist_Zjets_stack_3->SetBinError(2,9.74154);
   hist_Zjets_stack_3->SetBinError(3,9.115003);
   hist_Zjets_stack_3->SetBinError(4,8.560432);
   hist_Zjets_stack_3->SetEntries(1444);
   hist_Zjets_stack_3->SetStats(0);
   hist_Zjets_stack_3->SetFillColor(210);
   hist_Zjets_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Zjets_stack_3->GetXaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_Zjets_stack_3->GetXaxis()->SetTitleFont(42);
   hist_Zjets_stack_3->GetYaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetYaxis()->SetTitleFont(42);
   hist_Zjets_stack_3->GetZaxis()->SetLabelFont(42);
   hist_Zjets_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_Zjets_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zjets_stack_3,"");
   Double_t xAxis51[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Zgamma_stack_4 = new TH1F("hist_Zgamma_stack_4","$Z+\\gamma$",4, xAxis51);
   hist_Zgamma_stack_4->SetBinContent(2,589.0063);
   hist_Zgamma_stack_4->SetBinContent(3,448.254);
   hist_Zgamma_stack_4->SetBinContent(4,361.9795);
   hist_Zgamma_stack_4->SetBinError(2,31.42263);
   hist_Zgamma_stack_4->SetBinError(3,31.96889);
   hist_Zgamma_stack_4->SetBinError(4,22.5294);
   hist_Zgamma_stack_4->SetEntries(12515);
   hist_Zgamma_stack_4->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_Zgamma_stack_4->SetFillColor(ci);
   hist_Zgamma_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Zgamma_stack_4->GetXaxis()->SetLabelFont(42);
   hist_Zgamma_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_4->GetXaxis()->SetTitleFont(42);
   hist_Zgamma_stack_4->GetYaxis()->SetLabelFont(42);
   hist_Zgamma_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_4->GetYaxis()->SetTitleFont(42);
   hist_Zgamma_stack_4->GetZaxis()->SetLabelFont(42);
   hist_Zgamma_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zgamma_stack_4,"");
   Double_t xAxis52[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Wgamma_stack_5 = new TH1F("hist_Wgamma_stack_5","$W+\\gamma$",4, xAxis52);
   hist_Wgamma_stack_5->SetBinContent(2,1225.346);
   hist_Wgamma_stack_5->SetBinContent(3,1148.694);
   hist_Wgamma_stack_5->SetBinContent(4,1757.083);
   hist_Wgamma_stack_5->SetBinError(2,52.93327);
   hist_Wgamma_stack_5->SetBinError(3,50.0157);
   hist_Wgamma_stack_5->SetBinError(4,56.84781);
   hist_Wgamma_stack_5->SetEntries(30500);
   hist_Wgamma_stack_5->SetStats(0);
   hist_Wgamma_stack_5->SetFillColor(223);
   hist_Wgamma_stack_5->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Wgamma_stack_5->GetXaxis()->SetLabelFont(42);
   hist_Wgamma_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_5->GetXaxis()->SetTitleFont(42);
   hist_Wgamma_stack_5->GetYaxis()->SetLabelFont(42);
   hist_Wgamma_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_5->GetYaxis()->SetTitleFont(42);
   hist_Wgamma_stack_5->GetZaxis()->SetLabelFont(42);
   hist_Wgamma_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wgamma_stack_5,"");
   Double_t xAxis53[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Wjets_stack_6 = new TH1F("hist_Wjets_stack_6","$W+$jets",4, xAxis53);
   hist_Wjets_stack_6->SetBinContent(2,1571.483);
   hist_Wjets_stack_6->SetBinContent(3,1338.716);
   hist_Wjets_stack_6->SetBinContent(4,1465.202);
   hist_Wjets_stack_6->SetBinError(2,118.2737);
   hist_Wjets_stack_6->SetBinError(3,111.7383);
   hist_Wjets_stack_6->SetBinError(4,116.8902);
   hist_Wjets_stack_6->SetEntries(558);
   hist_Wjets_stack_6->SetStats(0);

   ci = TColor::GetColor("#006666");
   hist_Wjets_stack_6->SetFillColor(ci);
   hist_Wjets_stack_6->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Wjets_stack_6->GetXaxis()->SetLabelFont(42);
   hist_Wjets_stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_6->GetXaxis()->SetTitleOffset(1);
   hist_Wjets_stack_6->GetXaxis()->SetTitleFont(42);
   hist_Wjets_stack_6->GetYaxis()->SetLabelFont(42);
   hist_Wjets_stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_6->GetYaxis()->SetTitleFont(42);
   hist_Wjets_stack_6->GetZaxis()->SetLabelFont(42);
   hist_Wjets_stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_6->GetZaxis()->SetTitleOffset(1);
   hist_Wjets_stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wjets_stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3010[7] = {
   0,
   7.5,
   17.5,
   25,
   90,
   168.75,
   206.25};
   Double_t totalStackErr_fy3010[7] = {
   0,
   0,
   3639.447,
   3113.533,
   3837.785,
   0,
   0};
   Double_t totalStackErr_felx3010[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fely3010[7] = {
   0,
   0,
   133.7481,
   126.8998,
   132.2681,
   0,
   0};
   Double_t totalStackErr_fehx3010[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fehy3010[7] = {
   0,
   0,
   133.7481,
   126.8998,
   132.2681,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(7,totalStackErr_fx3010,totalStackErr_fy3010,totalStackErr_felx3010,totalStackErr_fehx3010,totalStackErr_fely3010,totalStackErr_fehy3010);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3010 = new TH1F("Graph_totalStackErr3010","SM",100,0,292.875);
   Graph_totalStackErr3010->SetMinimum(4.367058);
   Graph_totalStackErr3010->SetMaximum(4367.058);
   Graph_totalStackErr3010->SetDirectory(0);
   Graph_totalStackErr3010->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3010->SetLineColor(ci);
   Graph_totalStackErr3010->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3010->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3010->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3010->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3010);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wgamma_stack_5"," #it{W+#gamma}","f");
   entry->SetFillColor(223);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zgamma_stack_4"," #it{Z+#gamma}","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wjets_stack_6"," #it{W+}jets","f");

   ci = TColor::GetColor("#006666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zjets_stack_3"," #it{Z+}jets","f");
   entry->SetFillColor(210);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tt_stack_2"," #it{t#bar{t}}","f");
   entry->SetFillColor(219);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_1"," Other fakes","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEE/lepPt_antiIDFFbins\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"ee channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis54[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",4, xAxis54);
   Graph_master_copy->SetMinimum(0.1);
   Graph_master_copy->SetMaximum(1200000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEE_lepPt_antiIDFFbins->cd();
   CutWHG0BVetoEE_lepPt_antiIDFFbins->Modified();
   CutWHG0BVetoEE_lepPt_antiIDFFbins->cd();
   CutWHG0BVetoEE_lepPt_antiIDFFbins->SetSelected(CutWHG0BVetoEE_lepPt_antiIDFFbins);
}
