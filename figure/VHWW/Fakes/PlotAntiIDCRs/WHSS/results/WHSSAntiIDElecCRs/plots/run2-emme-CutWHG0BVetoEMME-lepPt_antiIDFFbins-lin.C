void run2-emme-CutWHG0BVetoEMME-lepPt_antiIDFFbins-lin()
{
//=========Macro generated from canvas: CutWHG0BVetoEMME_lepPt_antiIDFFbins/CutWHG0BVetoEMME_lepPt_antiIDFFbins
//=========  (Fri Mar  3 15:27:58 2023) by ROOT version 6.18/04
   TCanvas *CutWHG0BVetoEMME_lepPt_antiIDFFbins = new TCanvas("CutWHG0BVetoEMME_lepPt_antiIDFFbins", "CutWHG0BVetoEMME_lepPt_antiIDFFbins",0,0,800,600);
   gStyle->SetOptStat(0);
   gStyle->SetOptTitle(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetHighLightColor(2);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->Range(0,0,1,1);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetFillColor(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetBorderMode(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetBorderSize(2);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetLeftMargin(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetRightMargin(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetTopMargin(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetBottomMargin(0);
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetFrameBorderMode(0);
  
// ------------>Primitives in pad: main
   TPad *main = new TPad("main", "main",0,0,1,1);
   main->Draw();
   main->cd();
   main->Range(-30.37975,-2025.316,159.4937,10632.91);
   main->SetFillColor(0);
   main->SetFillStyle(4000);
   main->SetBorderMode(0);
   main->SetBorderSize(0);
   main->SetTickx(1);
   main->SetTicky(1);
   main->SetLeftMargin(0.16);
   main->SetRightMargin(0.05);
   main->SetTopMargin(0.05);
   main->SetBottomMargin(0.16);
   main->SetFrameBorderMode(0);
   main->SetFrameBorderMode(0);
   Double_t xAxis91[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master = new TH1F("Graph_master","Main Coordinate System",4, xAxis91);
   Graph_master->SetMinimum(0);
   Graph_master->SetMaximum(10000);
   Graph_master->SetStats(0);
   Graph_master->SetFillStyle(0);
   Graph_master->SetLineColor(0);
   Graph_master->SetMarkerColor(0);
   Graph_master->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master->GetXaxis()->SetLabelFont(42);
   Graph_master->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetXaxis()->SetLabelSize(0.0375);
   Graph_master->GetXaxis()->SetTitleSize(0.0375);
   Graph_master->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master->GetXaxis()->SetTitleFont(42);
   Graph_master->GetYaxis()->SetTitle("Events");
   Graph_master->GetYaxis()->SetLabelFont(42);
   Graph_master->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master->GetYaxis()->SetLabelSize(0.0375);
   Graph_master->GetYaxis()->SetTitleSize(0.0375);
   Graph_master->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master->GetYaxis()->SetTitleFont(42);
   Graph_master->GetZaxis()->SetLabelFont(42);
   Graph_master->GetZaxis()->SetLabelSize(0.035);
   Graph_master->GetZaxis()->SetTitleSize(0.035);
   Graph_master->GetZaxis()->SetTitleOffset(1);
   Graph_master->GetZaxis()->SetTitleFont(42);
   Graph_master->Draw("hist");
   
   THStack *stack = new THStack();
   stack->SetName("stack");
   stack->SetTitle("Background Stack");
   Double_t xAxis92[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *stack_stack_19 = new TH1F("stack_stack_19","Background Stack",4, xAxis92);
   stack_stack_19->SetMinimum(0);
   stack_stack_19->SetMaximum(6600.638);
   stack_stack_19->SetDirectory(0);
   stack_stack_19->SetStats(0);

   Int_t ci;      // for color index setting
   TColor *color; // for color definition with alpha
   ci = TColor::GetColor("#000099");
   stack_stack_19->SetLineColor(ci);
   stack_stack_19->GetXaxis()->SetLabelFont(42);
   stack_stack_19->GetXaxis()->SetLabelSize(0.035);
   stack_stack_19->GetXaxis()->SetTitleSize(0.035);
   stack_stack_19->GetXaxis()->SetTitleOffset(1);
   stack_stack_19->GetXaxis()->SetTitleFont(42);
   stack_stack_19->GetYaxis()->SetLabelFont(42);
   stack_stack_19->GetYaxis()->SetLabelSize(0.035);
   stack_stack_19->GetYaxis()->SetTitleSize(0.035);
   stack_stack_19->GetYaxis()->SetTitleFont(42);
   stack_stack_19->GetZaxis()->SetLabelFont(42);
   stack_stack_19->GetZaxis()->SetLabelSize(0.035);
   stack_stack_19->GetZaxis()->SetTitleSize(0.035);
   stack_stack_19->GetZaxis()->SetTitleOffset(1);
   stack_stack_19->GetZaxis()->SetTitleFont(42);
   stack->SetHistogram(stack_stack_19);
   
   Double_t xAxis93[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Wgamma_stack_1 = new TH1F("hist_Wgamma_stack_1","$W+\\gamma$",4, xAxis93);
   hist_Wgamma_stack_1->SetBinContent(2,1924.998);
   hist_Wgamma_stack_1->SetBinContent(3,1949.105);
   hist_Wgamma_stack_1->SetBinContent(4,2872.886);
   hist_Wgamma_stack_1->SetBinError(2,65.04946);
   hist_Wgamma_stack_1->SetBinError(3,69.40611);
   hist_Wgamma_stack_1->SetBinError(4,81.65903);
   hist_Wgamma_stack_1->SetEntries(35751);
   hist_Wgamma_stack_1->SetStats(0);
   hist_Wgamma_stack_1->SetFillColor(223);
   hist_Wgamma_stack_1->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Wgamma_stack_1->GetXaxis()->SetLabelFont(42);
   hist_Wgamma_stack_1->GetXaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_1->GetXaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_1->GetXaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_1->GetXaxis()->SetTitleFont(42);
   hist_Wgamma_stack_1->GetYaxis()->SetLabelFont(42);
   hist_Wgamma_stack_1->GetYaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_1->GetYaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_1->GetYaxis()->SetTitleFont(42);
   hist_Wgamma_stack_1->GetZaxis()->SetLabelFont(42);
   hist_Wgamma_stack_1->GetZaxis()->SetLabelSize(0.035);
   hist_Wgamma_stack_1->GetZaxis()->SetTitleSize(0.035);
   hist_Wgamma_stack_1->GetZaxis()->SetTitleOffset(1);
   hist_Wgamma_stack_1->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wgamma_stack_1,"");
   Double_t xAxis94[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Zgamma_stack_2 = new TH1F("hist_Zgamma_stack_2","$Z+\\gamma$",4, xAxis94);
   hist_Zgamma_stack_2->SetBinContent(2,452.9525);
   hist_Zgamma_stack_2->SetBinContent(3,625.6466);
   hist_Zgamma_stack_2->SetBinContent(4,687.4131);
   hist_Zgamma_stack_2->SetBinError(2,28.07412);
   hist_Zgamma_stack_2->SetBinError(3,32.67468);
   hist_Zgamma_stack_2->SetBinError(4,37.84391);
   hist_Zgamma_stack_2->SetEntries(8257);
   hist_Zgamma_stack_2->SetStats(0);

   ci = TColor::GetColor("#cc6600");
   hist_Zgamma_stack_2->SetFillColor(ci);
   hist_Zgamma_stack_2->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Zgamma_stack_2->GetXaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetXaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_2->GetXaxis()->SetTitleFont(42);
   hist_Zgamma_stack_2->GetYaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetYaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetYaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetYaxis()->SetTitleFont(42);
   hist_Zgamma_stack_2->GetZaxis()->SetLabelFont(42);
   hist_Zgamma_stack_2->GetZaxis()->SetLabelSize(0.035);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleSize(0.035);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleOffset(1);
   hist_Zgamma_stack_2->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zgamma_stack_2,"");
   Double_t xAxis95[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Wjets_stack_3 = new TH1F("hist_Wjets_stack_3","$W+$jets",4, xAxis95);
   hist_Wjets_stack_3->SetBinContent(2,2436.656);
   hist_Wjets_stack_3->SetBinContent(3,2327.314);
   hist_Wjets_stack_3->SetBinContent(4,2289.669);
   hist_Wjets_stack_3->SetBinError(2,158.762);
   hist_Wjets_stack_3->SetBinError(3,153.5215);
   hist_Wjets_stack_3->SetBinError(4,158.3328);
   hist_Wjets_stack_3->SetEntries(751);
   hist_Wjets_stack_3->SetStats(0);

   ci = TColor::GetColor("#006666");
   hist_Wjets_stack_3->SetFillColor(ci);
   hist_Wjets_stack_3->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Wjets_stack_3->GetXaxis()->SetLabelFont(42);
   hist_Wjets_stack_3->GetXaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_3->GetXaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_3->GetXaxis()->SetTitleOffset(1);
   hist_Wjets_stack_3->GetXaxis()->SetTitleFont(42);
   hist_Wjets_stack_3->GetYaxis()->SetLabelFont(42);
   hist_Wjets_stack_3->GetYaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_3->GetYaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_3->GetYaxis()->SetTitleFont(42);
   hist_Wjets_stack_3->GetZaxis()->SetLabelFont(42);
   hist_Wjets_stack_3->GetZaxis()->SetLabelSize(0.035);
   hist_Wjets_stack_3->GetZaxis()->SetTitleSize(0.035);
   hist_Wjets_stack_3->GetZaxis()->SetTitleOffset(1);
   hist_Wjets_stack_3->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Wjets_stack_3,"");
   Double_t xAxis96[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_Zjets_stack_4 = new TH1F("hist_Zjets_stack_4","$Z+$jets",4, xAxis96);
   hist_Zjets_stack_4->SetBinContent(2,141.1228);
   hist_Zjets_stack_4->SetBinContent(3,113.4601);
   hist_Zjets_stack_4->SetBinContent(4,240.9342);
   hist_Zjets_stack_4->SetBinError(2,13.57076);
   hist_Zjets_stack_4->SetBinError(3,11.33299);
   hist_Zjets_stack_4->SetBinError(4,14.14608);
   hist_Zjets_stack_4->SetEntries(1632);
   hist_Zjets_stack_4->SetStats(0);
   hist_Zjets_stack_4->SetFillColor(210);
   hist_Zjets_stack_4->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_Zjets_stack_4->GetXaxis()->SetLabelFont(42);
   hist_Zjets_stack_4->GetXaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_4->GetXaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_4->GetXaxis()->SetTitleOffset(1);
   hist_Zjets_stack_4->GetXaxis()->SetTitleFont(42);
   hist_Zjets_stack_4->GetYaxis()->SetLabelFont(42);
   hist_Zjets_stack_4->GetYaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_4->GetYaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_4->GetYaxis()->SetTitleFont(42);
   hist_Zjets_stack_4->GetZaxis()->SetLabelFont(42);
   hist_Zjets_stack_4->GetZaxis()->SetLabelSize(0.035);
   hist_Zjets_stack_4->GetZaxis()->SetTitleSize(0.035);
   hist_Zjets_stack_4->GetZaxis()->SetTitleOffset(1);
   hist_Zjets_stack_4->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_Zjets_stack_4,"");
   Double_t xAxis97[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_tt_stack_5 = new TH1F("hist_tt_stack_5","$t\\bar{t}$",4, xAxis97);
   hist_tt_stack_5->SetBinContent(2,106.2098);
   hist_tt_stack_5->SetBinContent(3,101.6826);
   hist_tt_stack_5->SetBinContent(4,142.9818);
   hist_tt_stack_5->SetBinError(2,3.861706);
   hist_tt_stack_5->SetBinError(3,3.781561);
   hist_tt_stack_5->SetBinError(4,4.573387);
   hist_tt_stack_5->SetEntries(2670);
   hist_tt_stack_5->SetStats(0);
   hist_tt_stack_5->SetFillColor(219);
   hist_tt_stack_5->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_tt_stack_5->GetXaxis()->SetLabelFont(42);
   hist_tt_stack_5->GetXaxis()->SetLabelSize(0.035);
   hist_tt_stack_5->GetXaxis()->SetTitleSize(0.035);
   hist_tt_stack_5->GetXaxis()->SetTitleOffset(1);
   hist_tt_stack_5->GetXaxis()->SetTitleFont(42);
   hist_tt_stack_5->GetYaxis()->SetLabelFont(42);
   hist_tt_stack_5->GetYaxis()->SetLabelSize(0.035);
   hist_tt_stack_5->GetYaxis()->SetTitleSize(0.035);
   hist_tt_stack_5->GetYaxis()->SetTitleFont(42);
   hist_tt_stack_5->GetZaxis()->SetLabelFont(42);
   hist_tt_stack_5->GetZaxis()->SetLabelSize(0.035);
   hist_tt_stack_5->GetZaxis()->SetTitleSize(0.035);
   hist_tt_stack_5->GetZaxis()->SetTitleOffset(1);
   hist_tt_stack_5->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_tt_stack_5,"");
   Double_t xAxis98[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6 = new TH1F("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6","Other fakes",4, xAxis98);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinContent(2,34.31598);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinContent(3,28.57032);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinContent(4,52.43833);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinError(2,1.942582);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinError(3,1.777303);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetBinError(4,2.655238);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetEntries(3496);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetStats(0);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->SetFillColor(15);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetXaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetYaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetYaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetYaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetYaxis()->SetTitleFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetZaxis()->SetLabelFont(42);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetZaxis()->SetLabelSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetZaxis()->SetTitleSize(0.035);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetZaxis()->SetTitleOffset(1);
   hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6->GetZaxis()->SetTitleFont(42);
   stack->Add(hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6,"");
   stack->Draw("hist same");
   
   Double_t totalStackErr_fx3019[7] = {
   0,
   7.5,
   17.5,
   25,
   90,
   168.75,
   206.25};
   Double_t totalStackErr_fy3019[7] = {
   0,
   0,
   5096.255,
   5145.779,
   6286.322,
   0,
   0};
   Double_t totalStackErr_felx3019[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fely3019[7] = {
   0,
   0,
   174.4357,
   172.0453,
   182.7504,
   0,
   0};
   Double_t totalStackErr_fehx3019[7] = {
   0,
   7.5,
   2.5,
   5,
   60,
   60,
   60};
   Double_t totalStackErr_fehy3019[7] = {
   0,
   0,
   174.4357,
   172.0453,
   182.7504,
   0,
   0};
   TGraphAsymmErrors *grae = new TGraphAsymmErrors(7,totalStackErr_fx3019,totalStackErr_fy3019,totalStackErr_felx3019,totalStackErr_fehx3019,totalStackErr_fely3019,totalStackErr_fehy3019);
   grae->SetName("totalStackErr");
   grae->SetTitle("SM");
   grae->SetFillColor(14);
   grae->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   grae->SetLineColor(ci);
   grae->SetLineStyle(0);
   
   TH1F *Graph_totalStackErr3019 = new TH1F("Graph_totalStackErr3019","SM",100,0,292.875);
   Graph_totalStackErr3019->SetMinimum(0);
   Graph_totalStackErr3019->SetMaximum(7115.98);
   Graph_totalStackErr3019->SetDirectory(0);
   Graph_totalStackErr3019->SetStats(0);

   ci = TColor::GetColor("#000099");
   Graph_totalStackErr3019->SetLineColor(ci);
   Graph_totalStackErr3019->GetXaxis()->SetLabelFont(42);
   Graph_totalStackErr3019->GetXaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3019->GetXaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3019->GetXaxis()->SetTitleOffset(1);
   Graph_totalStackErr3019->GetXaxis()->SetTitleFont(42);
   Graph_totalStackErr3019->GetYaxis()->SetLabelFont(42);
   Graph_totalStackErr3019->GetYaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3019->GetYaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3019->GetYaxis()->SetTitleFont(42);
   Graph_totalStackErr3019->GetZaxis()->SetLabelFont(42);
   Graph_totalStackErr3019->GetZaxis()->SetLabelSize(0.035);
   Graph_totalStackErr3019->GetZaxis()->SetTitleSize(0.035);
   Graph_totalStackErr3019->GetZaxis()->SetTitleOffset(1);
   Graph_totalStackErr3019->GetZaxis()->SetTitleFont(42);
   grae->SetHistogram(Graph_totalStackErr3019);
   
   grae->Draw("2");
   
   TLegend *leg = new TLegend(0.59,0.784,0.9,0.92,NULL,"brNDC");
   leg->SetBorderSize(0);
   leg->SetTextSize(0.025);
   leg->SetLineColor(1);
   leg->SetLineStyle(1);
   leg->SetLineWidth(1);
   leg->SetFillColor(0);
   leg->SetFillStyle(0);
   TLegendEntry *entry=leg->AddEntry("totalStackError"," SM (stat)","lf");
   entry->SetFillColor(14);
   entry->SetFillStyle(3254);

   ci = TColor::GetColor("#0000ff");
   entry->SetLineColor(ci);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wgamma_stack_1"," #it{W+#gamma}","f");
   entry->SetFillColor(223);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zgamma_stack_2"," #it{Z+#gamma}","f");

   ci = TColor::GetColor("#cc6600");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Wjets_stack_3"," #it{W+}jets","f");

   ci = TColor::GetColor("#006666");
   entry->SetFillColor(ci);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_Zjets_stack_4"," #it{Z+}jets","f");
   entry->SetFillColor(210);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_tt_stack_5"," #it{t#bar{t}}","f");
   entry->SetFillColor(219);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   entry=leg->AddEntry("hist_bkg___channel____campaign__ddFakes_eFakes_mc__diboson_triboson_top_singletop__stack_6"," Other fakes","f");
   entry->SetFillColor(15);
   entry->SetFillStyle(1001);
   entry->SetLineColor(1);
   entry->SetLineStyle(1);
   entry->SetLineWidth(1);
   entry->SetMarkerColor(1);
   entry->SetMarkerStyle(21);
   entry->SetMarkerSize(1);
   entry->SetTextFont(42);
   leg->Draw();
   TLatex *   tex = new TLatex(0.2,0.86,"ATLAS");
tex->SetNDC();
   tex->SetTextFont(72);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.36,0.86,"Internal");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.046875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.95,0.96,"Plot: \"CutWHG0BVetoEMME/lepPt_antiIDFFbins\"");
tex->SetNDC();
   tex->SetTextAlign(31);
   tex->SetTextFont(42);
   tex->SetTextSize(0.0225);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.815,"#sqrt{s} = 13 TeV, #lower[-0.2]{#scale[0.6]{#int}} Ldt = 139 fb^{-1}");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
      tex = new TLatex(0.2,0.77,"e#mu+#mue channel");
tex->SetNDC();
   tex->SetTextFont(42);
   tex->SetTextSize(0.031875);
   tex->SetLineWidth(2);
   tex->Draw();
   Double_t xAxis99[5] = {0, 15, 20, 30, 150}; 
   
   TH1F *Graph_master_copy = new TH1F("Graph_master_copy","Main Coordinate System",4, xAxis99);
   Graph_master_copy->SetMinimum(0);
   Graph_master_copy->SetMaximum(10000);
   Graph_master_copy->SetDirectory(0);
   Graph_master_copy->SetStats(0);
   Graph_master_copy->SetFillStyle(0);
   Graph_master_copy->SetLineColor(0);
   Graph_master_copy->SetMarkerColor(0);
   Graph_master_copy->GetXaxis()->SetTitle("p_{T}^{antiid e} [GeV]");
   Graph_master_copy->GetXaxis()->SetLabelFont(42);
   Graph_master_copy->GetXaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetXaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetXaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetXaxis()->SetTitleFont(42);
   Graph_master_copy->GetYaxis()->SetTitle("Events");
   Graph_master_copy->GetYaxis()->SetLabelFont(42);
   Graph_master_copy->GetYaxis()->SetLabelOffset(0.006666667);
   Graph_master_copy->GetYaxis()->SetLabelSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleSize(0.0375);
   Graph_master_copy->GetYaxis()->SetTitleOffset(1.866667);
   Graph_master_copy->GetYaxis()->SetTitleFont(42);
   Graph_master_copy->GetZaxis()->SetLabelFont(42);
   Graph_master_copy->GetZaxis()->SetLabelSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleSize(0.035);
   Graph_master_copy->GetZaxis()->SetTitleOffset(1);
   Graph_master_copy->GetZaxis()->SetTitleFont(42);
   Graph_master_copy->Draw("sameaxis");
   main->Modified();
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->cd();
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->Modified();
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->cd();
   CutWHG0BVetoEMME_lepPt_antiIDFFbins->SetSelected(CutWHG0BVetoEMME_lepPt_antiIDFFbins);
}
